/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// only include once
#pragma once

// includes
#include <string>
#include <vector>
#include <stdexcept>
#include "definitions.hpp"
#include "DecDatetime.hpp"
#include "Directory.hpp"

// put code behind library namespace
namespace isochop
{
    /**
     * @brief  This class represents a basic volume descriptor
     * @note   do not use this directly
     */
    class VolumeDescriptor
    {
    protected:
        /*
         * The type of the volume descriptor (1 byte)
         *   0: Boot record volume descriptor
         *   1: Primary volume descriptor
         *   2: Supplementary volume descriptor, or enhanced volume descriptor
         *   3: Volume partition descriptor
         *   255: Volume descriptor set terminator
         */

        /*
         * This is the volume identifier (5 bytes)
         * Always "CD001"
         */

        /*
         * This is the version number (1 byte)
         * Always 0x01
         */

        // This is the entire volume descriptor, but as binary
        std::vector<char> data;

        // The address of the first byte
        unsigned long address;

    public:
        /*
         *  ____                      _     __  __      _   _               _
         * |  _ \ __ _ _ __ ___ _ __ | |_  |  \/  | ___| |_| |__   ___   __| |___
         * | |_) / _` | '__/ _ \ '_ \| __| | |\/| |/ _ \ __| '_ \ / _ \ / _` / __|
         * |  __/ (_| | | |  __/ | | | |_  | |  | |  __/ |_| | | | (_) | (_| \__ \
         * |_|   \__,_|_|  \___|_| |_|\__| |_|  |_|\___|\__|_| |_|\___/ \__,_|___/
         */

        /**
         * @brief  Initialize the Volume Descriptor with dummy values.
         * @note   dummy values may not be legitimate
         * @retval None
         */
        VolumeDescriptor();

        /**
         * @brief  Initialize the Volume Descriptor with parsed data.
         * @note   char array must be same size as VD_TOTAL_SIZE
         * @param  raw_data byte array to parse
         * @param  address address of the first byte
         * @retval None
         */
        VolumeDescriptor(const std::vector<char> &raw_data, unsigned long address);

        /**
         * @brief  Initialize the Volume Descriptor from a directory of exported data
         * @param  dirpath: the directory path where all exported files are stored
         * @param  manifesthash: the hash of the volume descriptor's manifest file
         * @retval None
         */
        VolumeDescriptor(const std::string &dirpath, const std::string &manifesthash);

        /**
         * @brief  Check if volume descriptor contains valid data
         * @note   only checks type, identifier, and version
         * @retval true if volume descriptor is valid
         */
        virtual bool isValid();

        /**
         * @brief  Generates a string with volume descriptor information
         * @note   format: "TYPE IDENTIFIER VERSION"
         * @retval a string with volume descriptor information
         */
        virtual std::string toString();

        /**
         * @brief  This clones the volume descriptor
         * @note   depending on the type, you may want to dynamic_cast the clone
         * @retval a clone of the current volume descriptor
         */
        virtual VolumeDescriptor *clone();

        /**
         * @brief  Fetch the address of the first byte
         * @retval address of the first byte
         */
        unsigned long getAddress();

        /**
         * @brief  Set the address of the first byte
         * @param  address: the new address of the first byte
         */
        void setAddress(unsigned long address);

        /**
         * @brief  Fetch the volume descriptor type
         * @retval the type attribute
         */
        unsigned char getType();

        /**
         * @brief  Set the volume descriptor type
         * @param  type volume descriptor type
         * @retval None
         */
        void setType(unsigned char type);

        /**
         * @brief  Fetch the volume descriptor identifier
         * @note   exactly 5 bytes long (standard is "CD001")
         * @retval copy of the identifier array
         */
        std::string getIdentifier();

        /**
         * @brief  Set the volume descriptor identifier
         * @note   must be 5 bytes long (standard is "CD001")
         * @param  identifier volume descriptor identifier
         * @retval None
         */
        void setIdentifier(const std::string &identifier);

        /**
         * @brief  Fetch the volume descriptor version
         * @note   standard is 1
         * @retval the version attribute
         */
        char getVersion();

        /**
         * @brief  Set the volume descriptor version
         * @note   standard is 1
         * @param  version: volume descriptor version
         * @retval None
         */
        void setVersion(char version);

        /**
         * @brief  Fetch the volume descriptor data section
         * @note   size is VD_DATA_SIZE
         * @retval copy of the data array
         */
        virtual std::vector<char> getData();

        /**
         * @brief  Set the volume descriptor data section
         * @note   size must be VD_DATA_SIZE
         * @param  data volume descriptor data section
         * @retval None
         */
        virtual void setData(const std::vector<char> &data);

        /**
         * @brief  Export the volume descriptor information to a file
         * @param  dirpath: the directory path where all exported files will be stored
         * @retval the SHA256 checksum of this VolumeDescriptor's manifest
         */
        std::string export_data(const std::string &dirpath);

        /**
         * @brief  Import the volume descriptor from a file
         * @param  dirpath: the directory path where all exported files are stored
         * @param  manifesthash: the hash of the volume descriptor's manifest file
         * @retval None
         */
        void import_data(const std::string &dirpath, const std::string &manifesthash);

        /**
         * @brief  Take the bit vector and mark the byte addresses covered by this object
         * @note   must be big enough to contain the address space
         * @param  bits: the vector of bits
         * @retval None
         */
        void markAddresses(std::vector<bool> *bits);

        /*
         *   ____ _     _ _     _   __  __      _   _               _
         *  / ___| |__ (_) | __| | |  \/  | ___| |_| |__   ___   __| |___
         * | |   | '_ \| | |/ _` | | |\/| |/ _ \ __| '_ \ / _ \ / _` / __|
         * | |___| | | | | | (_| | | |  | |  __/ |_| | | | (_) | (_| \__ \
         *  \____|_| |_|_|_|\__,_| |_|  |_|\___|\__|_| |_|\___/ \__,_|___/
         */

        /**
         * @brief  Fetch the system identifier
         * @retval the system identifier
         */
        virtual std::string getSystemIdentifier() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the system identifier
         * @note   id must be of size PMVD_SIZE_SYSTEM_IDENTIFIER
         * @param  id: new system identifier
         */
        virtual void setSystemIdentifier(const std::string &id) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the volume identifier
         * @retval the volume identifier
         */
        virtual std::string getVolumeIdentifier() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the volume identifier
         * @note   id must be of size PMVD_SIZE_VOLUME_IDENTIFIER
         * @param  id: new volume identifier
         */
        virtual void setVolumeIdentifier(const std::string &id) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the volume space size
         * @retval the volume space size
         */
        virtual uint32_t getVolumeSpaceSize() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the volume space size
         * @param  size: new volume space size
         */
        virtual void setVolumeSpaceSize(uint32_t size) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the volume set size
         * @retval the volume set size
         */
        virtual uint16_t getVolumeSetSize() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the volume set size
         * @param  size: new volume set size
         */
        virtual void setVolumeSetSize(uint16_t size) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the volume space size
         * @retval the volume space size
         */
        virtual uint16_t getVolumeSequenceNumber() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the volume sequence number
         * @param  size: new volume sequence number
         */
        virtual void setVolumeSequenceNumber(uint16_t size) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the logical block size
         * @retval the logical block size
         */
        virtual uint16_t getLogicalBlockSize() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the logical block size
         * @param  size: new logical block size
         */
        virtual void setLogicalBlockSize(uint16_t size) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the path table size
         * @retval the path table size
         */
        virtual uint32_t getPathTableSize() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the path table size
         * @param  size: new path table size
         */
        virtual void setPathTableSize(uint32_t size) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the location of the type L path table
         * @retval the location of the type L path table
         */
        virtual uint32_t getLocationOfTypeLPathTable() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the new location of the type L path table
         * @param  location: new location of the type L path table
         */
        virtual void setLocationOfTypeLPathTable(uint32_t location) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the location of the optional type L path table
         * @retval the location of the optional type L path table
         */
        virtual uint32_t getLocationOfOptionalTypeLPathTable() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the new location of the optional type L path table
         * @param  location: new location of the optional type L path table
         */
        virtual void setLocationOfOptionalTypeLPathTable(uint32_t location) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the location of the type M path table
         * @retval the location of the type M path table
         */
        virtual uint32_t getLocationOfTypeMPathTable() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the new location of the type M path table
         * @param  location: new location of the type M path table
         */
        virtual void setLocationOfTypeMPathTable(uint32_t location) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the location of the optional type M path table
         * @retval the location of the optional type M path table
         */
        virtual uint32_t getLocationOfOptionalTypeMPathTable() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the new location of the optional type M path table
         * @param  location: new location of the optional type M path table
         */
        virtual void setLocationOfOptionalTypeMPathTable(uint32_t location) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Get the root directory entry
         * @retval the root directory entry
         */
        virtual Directory getRootDirectoryEntry() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the root directory entry
         * @param  entry: new root directory entry
         */
        virtual void setRootDirectoryEntry(Directory entry) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Get the volume set identifier
         * @retval the volume set identifier
         */
        virtual std::string getVolumeSetIdentifier() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the volume set identifier
         * @note   id must be of size PMVD_SIZE_VOLUME_SET_IDENTIFIER
         * @param  id: new volume set identifier
         */
        virtual void setVolumeSetIdentifier(const std::string &id) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Get the publisher identifier
         * @retval the publisher identifier
         */
        virtual std::string getPublisherIdentifier() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the publisher identifier
         * @note   id must be of size PMVD_SIZE_PUBLISHER_IDENTIFIER
         * @param  id: new publisher identifier
         */
        virtual void setPublisherIdentifier(const std::string &id) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Get the data preparer identifier
         * @retval the data preparer identifier
         */
        virtual std::string getDataPreparerIdentifier() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the data preparer identifier
         * @note   id must be of size PMVD_SIZE_DATA_PREPARER_IDENTIFIER
         * @param  id: new data preparer identifier
         */
        virtual void setDataPreparerIdentifier(const std::string &id) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Get the application identifier
         * @retval the application identifier
         */
        virtual std::string getApplicationIdentifier() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the application identifier
         * @note   id must be of size PMVD_SIZE_APPLICATION_IDENTIFIER
         * @param  id: new application identifier
         */
        virtual void setApplicationIdentifier(const std::string &id) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Get the copyright file identifier
         * @retval the copyright file identifier
         */
        virtual std::string getCopyrightFileIdentifier() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the copyright file identifier
         * @note   id must be of size PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER
         * @param  id: new copyright file identifier
         */
        virtual void setCopyrightFileIdentifier(const std::string &id) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Get the abstract file identifier
         * @retval the abstract file identifier
         */
        virtual std::string getAbstractFileIdentifier() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the abstract file identifier
         * @note   id must be of size PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER
         * @param  id: new abstract file identifier
         */
        virtual void setAbstractFileIdentifier(const std::string &id) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Get the biblographic file identifier
         * @retval the biblographic file identifier
         */
        virtual std::string getBiblographicFileIdentifier() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the biblographic file identifier
         * @note   id must be of size PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER
         * @param  id: new biblographic file identifier
         */
        virtual void setBiblographicFileIdentifier(const std::string &id) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the volume creation date and time
         * @retval the volume creation date and time
         */
        virtual DecDatetime getVolumeCreationDateAndTime() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the volume creation date and time
         * @param  time: new volume creation date and time
         */
        virtual void setVolumeCreationDateAndTime(DecDatetime time) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the volume modification date and time
         * @retval the volume modification date and time
         */
        virtual DecDatetime getVolumeModificationDateAndTime() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the volume modification date and time
         * @param  time: new volume modification date and time
         */
        virtual void setVolumeModificationDateAndTime(DecDatetime time) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the volume expiration date and time
         * @retval the volume expiration date and time
         */
        virtual DecDatetime getVolumeExpirationDateAndTime() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the volume expiration date and time
         * @param  time: new volume expiration date and time
         */
        virtual void setVolumeExpirationDateAndTime(DecDatetime time) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the volume effective date and time
         * @retval the volume effective date and time
         */
        virtual DecDatetime getVolumeEffectiveDateAndTime() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the volume effective date and time
         * @param  time: new volume effective date and time
         */
        virtual void setVolumeEffectiveDateAndTime(DecDatetime time) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the file structure version
         * @retval the file structure version
         */
        virtual uint8_t getFileStructureVersion() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the file structure version
         * @param  version: new file structure version
         */
        virtual void setFileStructureVersion(uint8_t version) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the application used area
         * @retval the application used area
         */
        virtual std::vector<char> getApplicationUsed() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the application used area
         * @note   data must be of size PMVD_SIZE_APPLICATION_USED
         * @param  data: new application used area
         */
        virtual void setApplicationUsed(std::vector<char> data) { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Fetch the reserved area
         * @retval the reserved area
         */
        virtual std::vector<char> getReserved() { throw std::runtime_error("Parent does not define child-specific methods."); }

        /**
         * @brief  Set the reserved area
         * @note   data must be of size PMVD_OFFSET_RESERVED
         * @param  data: new reserved area
         */
        virtual void setReserved(std::vector<char> data) { throw std::runtime_error("Parent does not define child-specific methods."); }
    };
}

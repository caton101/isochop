/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <vector>
#include <string>
#include "isochop.hpp"
#include "definitions.hpp"
#include "ISO.hpp"
#include "VolumeDescriptor.hpp"
#include "ExtentTree.hpp"
#include "Orphan.hpp"

isochop::ISOChop::ISOChop()
{
    // do not take this constructor call seriously
    this->doFunStuff = false;
    this->populated = false;
    this->how = 0;
    this->iso = ISO();
}

isochop::ISOChop::ISOChop(std::string path)
{
    this->loadISO(path, nullptr);
}

isochop::ISOChop::ISOChop(std::string path, void status_callback(int code), int *result)
{
    int res = this->loadISO(path, status_callback);
    if (result != nullptr)
    {
        *result = res;
    }
}

isochop::ISOChop::ISOChop(std::string path, void status_callback_file(int code), void status_callback_directory(int code), int *result)
{
    int res = this->loadISO(path, status_callback_file, status_callback_directory);
    if (result != nullptr)
    {
        *result = res;
    }
}

bool isochop::ISOChop::directoryValid(std::string dirpath)
{
    return ISO::isExportValid(dirpath);
}

int isochop::ISOChop::whichLoader(std::string path)
{
    return ISO::checkLoader(path);
}

int isochop::ISOChop::loadISO(std::string path, void status_callback(int code))
{
    // call the other loader with the same callback for both cases
    return this->loadISO(path, status_callback, status_callback);
}

int isochop::ISOChop::loadISO(std::string path, void status_callback_file(int code), void status_callback_directory(int code))
{
    // load the ISO
    this->doFunStuff = false;
    this->how = this->whichLoader(path);
    int result = 0;
    if (how == ISOCHOP_ISO_LOADER_FILE)
    {
        // load ISO with file callback
        this->iso = ISO(path, status_callback_file, &result);
        // ok can mean success or a warning
        this->populated = result == ISOCHOP_ISO_PARSE_RET_SUCCESS || result == ISOCHOP_ISO_PARSE_RET_WARNING_VD_INVALID_CHILD;
    }
    else if (how == ISOCHOP_ISO_LOADER_DIRECTORY)
    {
        // load ISO with directory callback
        this->iso = ISO(path, status_callback_directory, &result);
        // ok can only mean success
        this->populated = result == ISOCHOP_ISO_IMPORT_RET_SUCCESS;
    }
    else
    {
        // this should not happen, but say it didn't populate
        this->populated = false;
    }
    // return the result code
    return result;
}

std::string isochop::ISOChop::explainLoadResult(int status)
{
    if (this->how == ISOCHOP_ISO_LOADER_DIRECTORY)
    {
        // explain the ISO::import_data status
        switch (status)
        {
        case ISOCHOP_ISO_IMPORT_RET_ISO_FSTREAM_NOT_GOOD:
            return "The fstream for reading the ISO manifest was not good.";
        case ISOCHOP_ISO_IMPORT_RET_ROOT_FSTREAM_NOT_GOOD:
            return "The fstream for reading the STRUCTURE_ROOT was not good.";
        case ISOCHOP_ISO_IMPORT_RET_SUCCESS:
            return "The ISO was imported successfully.";
        default:
            return "The status code is unknown.";
        }
    }
    else if (this->how == ISOCHOP_ISO_LOADER_FILE)
    {
        // explain the ISO::parseISO status
        switch (status)
        {
        case ISOCHOP_ISO_PARSE_RET_ERROR_FILE_IS_DIRECTORY:
            return "The ISO file path points to a directory.";
        case ISOCHOP_ISO_PARSE_RET_ERROR_FILE_NOT_FOUND:
            return "The ISO file does not exist.";
        case ISOCHOP_ISO_PARSE_RET_ERROR_FILE_NOT_GOOD:
            return "The fstream for reading the ISO file was not good.";
        case ISOCHOP_ISO_PARSE_RET_ERROR_NO_PRIMARY_VD:
            return "The ISO file does not have a primary volume descriptor.";
        case ISOCHOP_ISO_PARSE_RET_ERROR_VD_NOT_VALID:
            return "The ISO has a bad volume descriptor.";
        case ISOCHOP_ISO_PARSE_RET_ERROR_VD_TYPE_UNKNOWN:
            return "The ISO file has a volume descriptor with an unknown type.";
        case ISOCHOP_ISO_PARSE_RET_WARNING_VD_INVALID_CHILD:
            return "A volume descriptor has invalid data, but the ISO is still fine.";
        case ISOCHOP_ISO_PARSE_RET_SUCCESS:
            return "The ISO file was parsed correctly.";
        default:
            return "The status code is unknown.";
        }
    }
    else
    {
        return "Could not interpret how the ISO was loaded.";
    }
}

std::string isochop::ISOChop::explainMakeFileResult(int status)
{
    switch (status)
    {
    case ISOCHOP_ISO_WRITE_RET_FILE_NOT_GOOD:
        return "The fstream for writing was not good.";
    case ISOCHOP_ISO_WRITE_RET_PATH_EXISTS:
        return "The ISO file path already exists.";
    case ISOCHOP_ISO_WRITE_RET_SUCCESS:
        return "The ISO file was written successfully";
    default:
        return "The status code is unknown.";
    }
}

int isochop::ISOChop::makeFile(std::string filepath, void status_callback(int code))
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    return this->iso.writeISO(filepath, status_callback);
}

std::string isochop::ISOChop::makeDirectory(std::string dirpath, void status_callback(int code))
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    return this->iso.export_data(dirpath, status_callback);
}

std::string isochop::ISOChop::tree(size_t indent)
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    return this->iso.showTree(indent);
}

std::string isochop::ISOChop::showVolumeDescriptors()
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    return this->iso.showVolumeDescriptors();
}

std::string isochop::ISOChop::showExtentInfo()
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    return this->iso.showExtentTreeToString();
}

bool isochop::ISOChop::good()
{
    return this->populated;
}

bool isochop::ISOChop::fun()
{
    return this->doFunStuff;
}

void isochop::ISOChop::setFun(bool enabled)
{
    this->doFunStuff = enabled;
}

isochop::ISO isochop::ISOChop::getISO()
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    if (!this->doFunStuff)
    {
        throw std::logic_error("Fun must be enabled before calling this method.");
    }
    return this->iso.clone();
}

void isochop::ISOChop::setISO(ISO iso)
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    if (!this->doFunStuff)
    {
        throw std::logic_error("Fun must be enabled before calling this method.");
    }
    this->iso = iso.clone();
}

std::vector<char> isochop::ISOChop::getSystemArea()
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    if (!this->doFunStuff)
    {
        throw std::logic_error("Fun must be enabled before calling this method.");
    }
    return this->iso.getSystemArea();
}

void isochop::ISOChop::setSystemArea(std::vector<char> data)
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    if (!this->doFunStuff)
    {
        throw std::logic_error("Fun must be enabled before calling this method.");
    }
    this->iso.setSystemArea(data);
}

std::vector<isochop::VolumeDescriptor *> *isochop::ISOChop::getVolumeDescriptors()
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    if (!this->doFunStuff)
    {
        throw std::logic_error("Fun must be enabled before calling this method.");
    }
    return this->iso.getVolumeDescriptors();
}

void isochop::ISOChop::setVolumeDescriptors(std::vector<VolumeDescriptor *> *vds)
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    if (!this->doFunStuff)
    {
        throw std::logic_error("Fun must be enabled before calling this method.");
    }
    this->iso.setVolumeDescriptors(vds);
}

isochop::ExtentTree isochop::ISOChop::getTree()
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    if (!this->doFunStuff)
    {
        throw std::logic_error("Fun must be enabled before calling this method.");
    }
    return this->iso.getTree();
}

void isochop::ISOChop::setTree(ExtentTree tree)
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    if (!this->doFunStuff)
    {
        throw std::logic_error("Fun must be enabled before calling this method.");
    }
    this->iso.setTree(tree);
}

std::vector<isochop::Orphan> isochop::ISOChop::getOrphans()
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    if (!this->doFunStuff)
    {
        throw std::logic_error("Fun must be enabled before calling this method.");
    }
    return this->iso.getOrphans();
}

void isochop::ISOChop::setOrphans(std::vector<Orphan> orphans)
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    if (!this->doFunStuff)
    {
        throw std::logic_error("Fun must be enabled before calling this method.");
    }
    return this->iso.setOrphans(orphans);
}

unsigned long isochop::ISOChop::getSize()
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    if (!this->doFunStuff)
    {
        throw std::logic_error("Fun must be enabled before calling this method.");
    }
    return this->iso.getSize();
}

void isochop::ISOChop::setSize(unsigned long size)
{
    if (!this->populated)
    {
        throw std::logic_error("Called method before ISO was loaded.");
    }
    if (!this->doFunStuff)
    {
        throw std::logic_error("Fun must be enabled before calling this method.");
    }
    this->iso.setSize(size);
}
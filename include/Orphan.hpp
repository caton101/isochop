/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// only include once
#pragma once

// includes
#include <string>
#include <vector>

// put code behind library namespace
namespace isochop
{
    /**
     * @brief  This class implements a storage container for orphaned data
     */
    class Orphan
    {
    protected:
        // this stores internal data
        std::vector<char> data;
        // this stores the address of the orphaned data
        unsigned long address;

    public:
        /**
         * @brief  The no-args constructor for an Orphan object
         * @note   default values may not be valid
         * @retval a new extent object
         */
        Orphan();

        /**
         * @brief  The normal constructor for an Orphan object
         * @param  data: internal data
         * @retval a new extent object
         */
        Orphan(const std::vector<char> &data, unsigned long address);

        /**
         * @brief  A special constructor which loads data from a file
         * @param  dirpath: the directory path where all exported files are stored
         * @param  manifesthash: the hash of the orphan's manifest file
         * @retval None
         */
        Orphan(const std::string &dirpath, const std::string &manifesthash);

        /**
         * @brief  The deconstructor for an Orphan object
         */
        ~Orphan();

        /**
         * @brief  Generates a string summarizing the orphan
         * @note   string output may change format in future versions
         * @retval string representing the orphan
         */
        std::string toString();

        /**
         * @brief  This creates a clone of the orphan
         * @retval a copy of the orphan
         */
        Orphan clone();

        /**
         * @brief  Fetch the internal data
         * @retval the internal data
         */
        std::vector<char> getData();

        /**
         * @brief  Set the internal data
         * @param  data: the internal data
         */
        void setData(const std::vector<char> &data);

        /**
         * @brief  Fetch the address of the data
         * @retval the address of the data
         */
        unsigned long getAddress();

        /**
         * @brief  Set the address of the data
         * @param  address: the address of the data
         * @retval None
         */
        void setAddress(unsigned long address);

        /**
         * @brief  Export orphan data
         * @param  dirpath: the directory path where all exported files will be stored
         * @retval the SHA256 checksum of this Orphan's manifest
         */
        std::string export_data(const std::string &dirpath);

        /**
         * @brief  Import orphan data from a file
         * @param  dirpath: the directory path where all exported files are stored
         * @param  manifesthash: the hash of the orphan's manifest file
         * @retval None
         */
        void import_data(const std::string &dirpath, const std::string &manifesthash);

        /**
         * @brief  Take the bit vector and mark the byte addresses covered by this object
         * @note   must be big enough to contain the address space
         * @param  bits: the vector of bits
         * @retval None
         */
        void markAddresses(std::vector<bool> *bits);
    };
}

/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// only include once
#pragma once

// includes
#include <string>
#include <vector>

// put code behind library namespace
namespace isochop
{
    /**
     * @brief  This class implements a storage container for extent data
     */
    class ExtentData
    {
    protected:
        // this stores internal data
        std::vector<char> data;

    public:
        /**
         * @brief  The no-args constructor for an extentdata object
         * @note   default values may not be valid
         * @retval a new extent object
         */
        ExtentData();

        /**
         * @brief  The normal constructor for an extentdata object
         * @param  data: internal data
         * @retval a new extent object
         */
        ExtentData(const std::vector<char> &data);

        /**
         * @brief  The deconstructor for an extentdata object
         */
        ~ExtentData();

        /**
         * @brief  Generates a string summarizing the extent
         * @note   string output may change format in future versions
         * @retval string representing the extent
         */
        std::string toString();

        /**
         * @brief  This creates a clone of the ExtentData
         * @retval a copy of the ExtentData
         */
        ExtentData clone();

        /**
         * @brief  Fetch the internal data
         * @retval the internal data
         */
        std::vector<char> getData();

        /**
         * @brief  Set the internal data
         * @param  data: the internal data
         */
        void setData(const std::vector<char> &data);
    };
}

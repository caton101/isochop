/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <vector>
#include <cinttypes>
#include <algorithm>
#include <fstream>
#include "json/single_include/nlohmann/json.hpp"
#include "ExtentNode.hpp"
#include "ExtentData.hpp"
#include "Directory.hpp"
#include "utils.hpp"
#include "definitions.hpp"

isochop::ExtentNode::ExtentNode()
{
    // initialize empty fields
    this->directory = Directory();
    this->extent_data = ExtentData();
    this->children.clear();
}

isochop::ExtentNode::ExtentNode(const Directory &directory, unsigned long address)
{
    // set up known fields, initialize others
    this->setDirectoryEntry(directory);
    this->extent_data = ExtentData();
    this->setAddress(address);
    this->children.clear();
}

isochop::ExtentNode::ExtentNode(const Directory &directory, const ExtentData &extent_data, unsigned long address, const std::vector<ExtentNode> &children)
{
    // set up containing data
    this->setDirectoryEntry(directory);
    this->setExtentData(extent_data);
    this->setAddress(address);
    this->setChildren(children);
}

isochop::ExtentNode::ExtentNode(const std::string &dirpath, const std::string &manifesthash)
{
    // initialize from directory structure
    this->import_data(dirpath, manifesthash);
}

isochop::ExtentNode::~ExtentNode()
{
    // valgrind does not report any leaks at this time
}

std::string isochop::ExtentNode::toString()
{
    std::string str;
    // add directory entries
    str += "{Directory: ";
    str += this->directory.toString();
    str += "]\n";
    // add extent information
    str += "ExtentData: ";
    str += this->getExtentData().toString();
    str += "\n";
    // add child information
    str += "Children: [";
    for (size_t i = 0; i < this->children.size(); i++)
    {
        str += this->children.at(i).toString();
        if (i < this->children.size() - 1)
        {
            str += ",";
        }
    }
    str += "]}";
    // return the string
    return str;
}

isochop::ExtentNode isochop::ExtentNode::clone()
{
    ExtentNode fn;
    fn.directory = this->directory;
    fn.address = this->address;
    fn.extent_data = this->extent_data.clone();
    fn.children = this->children;
    return fn;
}

unsigned long isochop::ExtentNode::getAddress()
{
    return this->address;
}

void isochop::ExtentNode::setAddress(unsigned long address)
{
    this->address = address;
}

isochop::ExtentData isochop::ExtentNode::getExtentData()
{
    return this->extent_data;
}

void isochop::ExtentNode::setExtentData(const ExtentData &extent_data)
{
    this->extent_data = extent_data;
}

isochop::Directory isochop::ExtentNode::getDirectoryEntry()
{
    return this->directory;
}

void isochop::ExtentNode::setDirectoryEntry(const Directory &directory)
{
    this->directory = directory;
}

std::vector<isochop::ExtentNode> isochop::ExtentNode::getChildren()
{
    return this->children;
}

void isochop::ExtentNode::setChildren(const std::vector<ExtentNode> &children)
{
    this->children = children;
}

void isochop::ExtentNode::parseData(std::fstream *file, std::vector<uint64_t> *parsedAddresses)
{
    // ensure the file pointer is not null
    if (file == nullptr)
    {
        throw std::invalid_argument("file == nullptr");
    }
    // ensure the file is still good
    if (!file->good())
    {
        throw std::invalid_argument("!file->good()");
    }
    // ensure the directory is valid
    if (!this->directory.isValid())
    {
        throw std::runtime_error("!this->directory.isValid()");
    }
    // get extent information
    uint32_t sector = this->directory.getLocationOfExtent();
    uint32_t address = utils::convertSectorToByteAddress(sector);
    uint32_t size = this->directory.getDataLength();
    // ensure director is not parsed again
    parsedAddresses->push_back(address);
    // get extent data
    std::vector<char> data;
    // read file from disc
    file->seekg(address);
    for (size_t i = 0; i < size; i++)
    {
        char c;
        file->read(&c, 1);
        data.push_back(c);
    }
    // set the new extent
    this->extent_data.setData(data);
    // If this is a file, stop reading
    if (!this->directory.getFileFlags().getIsDirectory())
    {
        return;
    }
    // NOTE: only directories should execute beyond this point
    this->children.clear();
    std::vector<char> entries = this->extent_data.getData();
    size_t pos = 0;
    while (pos < entries.size())
    {
        // read the entry
        std::vector<char> dirData;
        uint8_t dirSize = entries.at(pos);
        if (dirSize == 0)
        {
            // calculate the next pos
            uint32_t posSector = utils::convertByteAddressToSector(address + pos);
            uint32_t posNewSector = posSector + 1;
            uint32_t posNewAddress = utils::convertSectorToByteAddress(posNewSector);
            size_t newPos = posNewAddress - address;
            if (newPos < size)
            {
                // skip to next sector
                pos = newPos;
                dirSize = entries.at(pos);
            }
            else
            {
                // if it can't be sector aligned, there are no more directory entries
                break;
            }
        }
        for (int i = 0; i < dirSize; i++)
        {
            dirData.push_back(entries.at(pos + i));
        }
        Directory dir = Directory(dirData);
        // generate the ExtentNode
        uint64_t addr = utils::convertSectorToByteAddress(dir.getLocationOfExtent());
        if (std::find(parsedAddresses->begin(), parsedAddresses->end(), addr) == parsedAddresses->end())
        {
            ExtentNode fn = ExtentNode(dir, addr);
            this->children.push_back(fn);
        }
        // move cursor
        pos += dirSize;
    }
    // destroy all unnecessary data (this helps reduce stack size)
    entries.clear();
    data.clear();
    // allow children to initialize themselves
    for (std::vector<ExtentNode>::iterator i = this->children.begin(); i != this->children.end(); i++)
    {
        i->parseData(file, parsedAddresses);
    }
}

void isochop::ExtentNode::writeData(std::fstream *file)
{
    // ensure the file pointer is not null
    if (file == nullptr)
    {
        throw std::invalid_argument("file == nullptr");
    }
    // ensure the file is still good
    if (!file->good())
    {
        throw std::invalid_argument("!file->good()");
    }
    // ensure the directory is valid
    if (!this->directory.isValid())
    {
        throw std::runtime_error("!this->directory.isValid()");
    }
    // seek to this address
    file->seekp(this->address);
    // write this ExtentNode to the file
    // NOTE: Directory is stored somewhere else so it doesn't need to be written again
    for (char c : this->extent_data.getData())
    {
        file->write(&c, 1);
    }
    // write the child ExtentNodes
    for (ExtentNode child : this->children)
    {
        child.writeData(file);
    }
}

std::string isochop::ExtentNode::tree(size_t indentLevel, size_t indentSpaces)
{
    // generate indent string
    std::string indent;
    for (size_t i = 0; i < indentSpaces; i++)
    {
        indent += " ";
    }
    // generate indent prefix
    std::string prefix;
    for (size_t i = 0; i < indentLevel; i++)
    {
        prefix += indent;
    }
    // write this directory out
    std::string str;
    // add this node
    str += prefix;
    std::string identifier = this->directory.getFileIdentifier();
    if (utils::isStrA(identifier, false))
    {
        str += identifier;
    }
    else
    {
        str += "(no name)";
    }
    str += "\n";
    // add children if needed
    for (ExtentNode en : this->children)
    {
        str += en.tree(indentLevel + 1, indentSpaces);
    }
    // return the tree string
    return str;
}

std::string isochop::ExtentNode::export_data(const std::string &dirpath)
{
    // get the SHA256 hash of the Directory and store it
    std::vector<char> dir = this->directory.getData();
    std::string sha256dir = utils::sha256sum(&dir);
    utils::dump_vector(&dir, dirpath + "/" + sha256dir, nullptr);
    // get the SHA256 hash of the extent and store it
    std::vector<char> extent = this->extent_data.getData();
    std::string sha256extent = utils::sha256sum(&extent);
    utils::dump_vector(&extent, dirpath + "/" + sha256extent, nullptr);
    // clear vectors to free ram
    dir.clear();
    extent.clear();
    // export children
    std::vector<std::string> sha256children;
    for (ExtentNode en : this->children)
    {
        std::string hash = en.export_data(dirpath);
        sha256children.push_back(hash);
    }
    // generate manifest JSON
    nlohmann::json manifestJSON;
    manifestJSON["manifest_class"] = "ExtentNode";
    manifestJSON["directory_hash"] = sha256dir;
    manifestJSON["extent_hash"] = sha256extent;
    manifestJSON["byte_address"] = this->address;
    manifestJSON["children_hashes"] = sha256children;
    // generate manifest string
    std::string manifestString = manifestJSON.dump(4, ' ', true);
    // calculate manifest hash
    std::vector<char> tmp;
    for (char c : manifestString)
    {
        tmp.push_back(c);
    }
    std::string manifestSHA256 = utils::sha256sum(&tmp);
    tmp.clear();
    // write JSON to file
    std::fstream file;
    file.open(dirpath + "/" + manifestSHA256, std::fstream::out);
    file << manifestString;
    file.close();
    // return this ExtentNode's SHA256 hash
    return manifestSHA256;
}

void isochop::ExtentNode::import_data(const std::string &dirpath, const std::string &manifesthash)
{
    // parse manifest file
    std::fstream manifestFile;
    manifestFile.open(dirpath + "/" + manifesthash, std::fstream::in);
    nlohmann::json manifestJSON;
    manifestFile >> manifestJSON;
    manifestFile.close();
    // load data from file
    std::string sha256dir = manifestJSON["directory_hash"];
    std::string sha256extent = manifestJSON["extent_hash"];
    unsigned long addr = manifestJSON["byte_address"];
    std::vector<std::string> sha256children = manifestJSON["children_hashes"];
    // read needed files
    std::vector<char> dirVec = utils::load_vector(dirpath + "/" + sha256dir, nullptr);
    std::vector<char> extVec = utils::load_vector(dirpath + "/" + sha256extent, nullptr);
    // set fields
    this->directory = Directory(dirVec);
    this->extent_data = ExtentData(extVec);
    this->address = addr;
    // clear temporary vectors (this helps reduce the stack size)
    dirVec.clear();
    extVec.clear();
    // import children
    this->children.clear();
    for (std::string sha256child : sha256children)
    {
        ExtentNode child = ExtentNode(dirpath, sha256child);
        this->children.push_back(child);
    }
}

void isochop::ExtentNode::markAddresses(std::vector<bool> *bits)
{
    if (bits == nullptr)
    {
        throw std::invalid_argument("bits == nullptr");
    }
    size_t end = this->address + this->extent_data.getData().size();
    for (unsigned long i = this->address; i < end; i++)
    {
        bits->at(i) = true;
    }
    for (ExtentNode en : this->children)
    {
        en.markAddresses(bits);
    }
}

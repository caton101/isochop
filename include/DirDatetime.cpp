/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <vector>
#include <cstdint>
#include <stdexcept>
#include "DirDatetime.hpp"
#include "definitions.hpp"
#include "utils.hpp"

isochop::DirDatetime::DirDatetime()
{
    // make data dector
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    this->data = data;
    // populate with placeholder values
    this->setYear(1);
    this->setMonth(1);
    this->setDay(1);
    this->setHour(1);
    this->setMinute(1);
    this->setSecond(1);
    this->setTimezone(0);
}

isochop::DirDatetime::DirDatetime(const std::vector<char> &data)
{
    // check size of data vector
    if (data.size() != ISOCHOP_DIR_DATETIME_SIZE_TOTAL)
    {
        throw std::invalid_argument("data.size() != ISOCHOP_DIR_DATETIME_SIZE_TOTAL");
    }
    // set the data vector
    this->setData(data);
}

isochop::DirDatetime::~DirDatetime()
{
    // valgrind does not report any leaks at this time
}

bool isochop::DirDatetime::isValid()
{
    // make flags
    bool valid = true;
    // check value ranges
    uint8_t month = this->getMonth();
    uint8_t day = this->getDay();
    uint8_t hour = this->getHour();
    uint8_t minute = this->getMinute();
    uint8_t second = this->getSecond();
    int8_t timezone = this->getTimezone();
    valid &= month >= 1 && month <= 12;
    valid &= day >= 1 && day <= 31;
    valid &= hour >= 0 && hour <= 23;
    valid &= minute >= 0 && minute <= 59;
    valid &= second >= 0 && second <= 59;
    valid &= timezone >= -48 && timezone <= 52;
    // return flags
    return valid;
}

std::string isochop::DirDatetime::toString()
{
    std::string str;
    str += "{";
    str += "Year: " + std::to_string(this->getYear()) + ", ";
    str += "Month: " + std::to_string(this->getMonth()) + ", ";
    str += "Day: " + std::to_string(this->getDay()) + ", ";
    str += "Hour: " + std::to_string(this->getHour()) + ", ";
    str += "Minutes: " + std::to_string(this->getMinute()) + ", ";
    str += "Seconds: " + std::to_string(this->getSecond()) + ", ";
    str += "Timezone: " + std::to_string(this->getTimezone());
    str += "}";
    return str;
}

std::vector<char> isochop::DirDatetime::getData()
{
    return this->data;
}

void isochop::DirDatetime::setData(const std::vector<char> &data)
{
    if (data.size() != ISOCHOP_DIR_DATETIME_SIZE_TOTAL)
    {
        throw new std::invalid_argument("data.size() != ISOCHOP_DIR_DATETIME_SIZE_TOTAL");
    }
    this->data = data;
}

uint8_t isochop::DirDatetime::getYear()
{
    return this->data.at(ISOCHOP_DIR_DATETIME_OFFSET_YEAR);
}

void isochop::DirDatetime::setYear(uint8_t year)
{
    this->data.at(ISOCHOP_DIR_DATETIME_OFFSET_YEAR) = year;
}

uint8_t isochop::DirDatetime::getMonth()
{
    return this->data.at(ISOCHOP_DIR_DATETIME_OFFSET_MONTH);
}

void isochop::DirDatetime::setMonth(uint8_t month)
{
    this->data.at(ISOCHOP_DIR_DATETIME_OFFSET_MONTH) = month;
}

uint8_t isochop::DirDatetime::getDay()
{
    return this->data.at(ISOCHOP_DIR_DATETIME_OFFSET_DAY);
}

void isochop::DirDatetime::setDay(uint8_t day)
{
    this->data.at(ISOCHOP_DIR_DATETIME_OFFSET_DAY) = day;
}

uint8_t isochop::DirDatetime::getHour()
{
    return this->data.at(ISOCHOP_DIR_DATETIME_OFFSET_HOUR);
}

void isochop::DirDatetime::setHour(uint8_t hour)
{
    this->data.at(ISOCHOP_DIR_DATETIME_OFFSET_HOUR) = hour;
}

uint8_t isochop::DirDatetime::getMinute()
{
    return this->data.at(ISOCHOP_DIR_DATETIME_OFFSET_MINUTE);
}

void isochop::DirDatetime::setMinute(uint8_t minute)
{
    this->data.at(ISOCHOP_DIR_DATETIME_OFFSET_MINUTE) = minute;
}

uint8_t isochop::DirDatetime::getSecond()
{
    return this->data.at(ISOCHOP_DIR_DATETIME_OFFSET_SECOND);
}

void isochop::DirDatetime::setSecond(uint8_t second)
{
    this->data.at(ISOCHOP_DIR_DATETIME_OFFSET_SECOND) = second;
}

int8_t isochop::DirDatetime::getTimezone()
{
    return this->data.at(ISOCHOP_DIR_DATETIME_OFFSET_TIMEZONE);
}

void isochop::DirDatetime::setTimezone(int8_t timezone)
{
    this->data.at(ISOCHOP_DIR_DATETIME_OFFSET_TIMEZONE) = timezone;
}

/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// only include once
#pragma once

// includes
#include <vector>
#include <string>
#include "definitions.hpp"
#include "VolumeDescriptor.hpp"
#include "ExtentTree.hpp"
#include "Orphan.hpp"

// put code behind library namespace
namespace isochop
{
    /**
     * @brief  This class represents an ISO 9660 image
     */
    class ISO
    {
    private:
        // this is the system reserved area
        std::vector<char> system_area;

        // this is a collection of volume descriptors
        std::vector<VolumeDescriptor *> volume_descriptors;

        // this is the extent-based tree
        ExtentTree extent_tree;

        // this stores all data not covered by the implemented data structures
        std::vector<Orphan> orphans;

        // this stores the size of the ISO file
        unsigned long size;

    public:
        /**
         * @brief  Initialize an ISO with dummy values.
         * @note   dummy values may not be legitimate
         * @retval a new ISO object
         */
        ISO();

        /**
         * @brief  Initialize an ISO with an ISO file or exported structure.
         * @param  path: file or directory to read
         * @param  status_callback: a function to handle status updates while loading
         * @param  result: the result code from the loader
         * @retval a new ISO object
         */
        ISO(const std::string &path, void status_callback(int code), int *result);

        /**
         * @brief  This frees all memory from an ISO object
         * @note   relies on the parent to free inherited fields
         * @retval None
         */
        ~ISO();

        /**
         * @brief  This checks which loader will be used to load the ISO
         * @param  path: file or directory to read
         * @retval the result code from checking (see ISO_LOADER_* in definitions.hpp)
         */
        static int checkLoader(const std::string &path);

        /**
         * @brief  This intelligently loads the ISO object using an ISO file or an exported directory
         * @param  path: file or directory to read
         * @param  status_callback: a function to call with loading updates
         * @retval the result code from parsing (see ISO_PARSE_RET_* or ISO_IMPORT_RET_* in definitions.hpp)
         */
        int smartLoader(const std::string &path, void status_callback(int code));

        /**
         * @brief  Parse byte data and populate the ISO object
         * @param  filepath: file to read
         * @param  status_callback: a function to call with parsing updates
         * @retval the result code from parsing (see ISO_PARSE_RET_* in definitions.hpp)
         */
        int parseISO(const std::string &filepath, void status_callback(int code));

        /**
         * @brief  Write ISO data to a file
         * @param  filepath: file to write
         * @param  status_callback: a function to call with writing updates
         * @retval the result code from writing (see ISO_WRITE_RET_* in definitions.hpp)
         */
        int writeISO(const std::string &filepath, void status_callback(int code));

        /**
         * @brief  This checks if the ISO image is valid
         * @retval true if the ISO image is valid
         */
        bool isValid();

        /**
         * @brief  Get the system area
         * @note   vector is of length SYSTEM_AREA_SIZE
         * @retval the system area vector
         */
        std::vector<char> getSystemArea();

        /**
         * @brief  Set the system area
         * @note   vector must be of length SYSTEM_AREA_SIZE
         * @param  data: the system area data
         * @retval None
         */
        void setSystemArea(const std::vector<char> &data);

        /**
         * @brief  This returns a pointer to the ISO's volume descriptors
         * @note   these are in-order as on disc
         * @retval a pointer to the ISO's volume descriptors
         */
        std::vector<VolumeDescriptor *> *getVolumeDescriptors();

        /**
         * @brief  This sets ISO's volume descriptors from a pointer
         * @note   these should be in-order as on disc
         * @param  vds: a pointer to volume descriptors
         * @retval None
         */
        void setVolumeDescriptors(std::vector<VolumeDescriptor *> *vds);

        /**
         * @brief  This returns the extent tree
         * @note   this represents the entire filesystem
         * @retval the ExtentTree used by the ISO object
         */
        ExtentTree getTree();

        /**
         * @brief  This sets the extent tree
         * @note   this represents the entire filesystem
         * @param  tree: the ExtentTree to be used by the ISO object
         * @retval None
         */
        void setTree(const ExtentTree &tree);

        /**
         * @brief  This returns all orphaned data in the ISO object
         * @retval all orphaned data in the ISO object
         */
        std::vector<Orphan> getOrphans();

        /**
         * @brief  This sets the orphaned data in the ISO object
         * @param  orphans: the new orphaned data in the ISO object
         * @retval None
         */
        void setOrphans(const std::vector<Orphan> &orphans);

        /**
         * @brief  This fetches the size of the ISO image
         * @retval the size of the ISO image
         */
        unsigned long getSize();

        /**
         * @brief  This sets the size of the ISO image
         * @param  size: the new size of the ISO image
         * @retval None
         */
        void setSize(unsigned long size);

        /**
         * @brief  Get a string representing the ISO file object
         * @note   string contains formatting operators like \n and \t
         * @retval string representing an iso file
         */
        std::string toString();

        /**
         * @brief  This clones the iso
         * @note   this is a deep copy
         * @retval a clone of the current iso
         */
        ISO clone();

        /**
         * @brief  Get a listing of all volume descriptors and their fields
         * @note   do not rely on the output format
         * @retval a listing of all volume descriptors and their fields
         */
        std::string showVolumeDescriptors();

        /**
         * @brief  Get the output of ExtentTree's toString method
         * @note   do not rely on the output format
         * @retval the output of ExtentTree's toString method
         */
        std::string showExtentTreeToString();

        /**
         * @brief  Get a graphical tree of the entire ExtentNode structure
         * @note   do not rely on the output format for parsing
         * @param  indentSpaces the number of spaces per indentation level
         * @retval a graphical tree of the entire ExtentNode structure
         */
        std::string showTree(size_t indentSpaces);

        /**
         * @brief  Export all ISO data into a directory structure
         * @param  dirpath: the directory path where all exported files will be stored
         * @param  status_callback: a function to call with exporting updates
         * @retval the SHA256 checksum of this ISO's manifest
         */
        std::string export_data(const std::string &dirpath, void status_callback(int code));

        /**
         * @brief  Import all ISO data from a directory structure
         * @param  dirpath: the directory path where all exported files are stored
         * @param  status_callback: a function to call with loading updates
         * @retval the result code from parsing (see ISO_IMPORT_RET_* in definitions.hpp)
         */
        int import_data(const std::string &dirpath, void status_callback(int code));

        /**
         * @brief  Check an exported directory structure for issues
         * @param  dirpath: the directory path where all exported files are stored
         * @retval true of the exported directory structure is valid
         */
        static bool isExportValid(const std::string &dirpath);

    private:
        /**
         * @brief  A helper for isExportValid. Recursively validates the file tree.
         * @param  dirpath: the directory path where all exported files are stored
         * @param  hash: the hash of the file to check
         * @param  manifest: true if the file to check is supposed to be a manifest
         * @retval true if the file (and possibly onward) is valid
         */
        static bool isFileValid(const std::string &dirpath, const std::string &hash, bool manifest);
    };
}

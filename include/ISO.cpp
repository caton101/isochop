/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <vector>
#include <string>
#include <fstream>
#include <filesystem>
#include <bitset>
#include "json/single_include/nlohmann/json.hpp"
#include "ISO.hpp"
#include "definitions.hpp"
#include "utils.hpp"
#include "VolumeDescriptor.hpp"
#include "PrimaryVolumeDescriptor.hpp"
#include "ExtentTree.hpp"
#include "Orphan.hpp"

isochop::ISO::ISO()
{
    // clear the system area
    std::vector<char> system_area(ISOCHOP_SYSTEM_AREA_SIZE, 0);
    this->system_area = system_area;
    // clear the volume descriptors
    this->volume_descriptors.clear();
    // clear the orphans
    this->orphans.clear();
    // add a single volume descriptor terminator
    VolumeDescriptor *vd = new VolumeDescriptor();
    vd->setType(ISOCHOP_VD_TYPE_TERMINATOR);
    this->volume_descriptors.push_back(vd);
}

isochop::ISO::ISO(const std::string &path, void status_callback(int code), int *result)
{
    // determine if filepath exists
    if (!std::filesystem::exists(path))
    {
        throw std::invalid_argument("path does not exist");
    }
    // load iso intelligently
    int ret = smartLoader(path, status_callback);
    if (result != nullptr)
    {
        *result = ret;
    }
}

isochop::ISO::~ISO()
{
    // Valgrind does not report any leaks
}

int isochop::ISO::checkLoader(const std::string &path)
{
    // load ISO depending on if the path is a directory
    if (std::filesystem::is_directory(path))
    {
        // load as directory structure
        return ISOCHOP_ISO_LOADER_DIRECTORY;
    }
    else
    {
        // load as ISO file
        return ISOCHOP_ISO_LOADER_FILE;
    }
}

int isochop::ISO::smartLoader(const std::string &path, void status_callback(int code))
{
    switch (this->checkLoader(path))
    {
    case ISOCHOP_ISO_LOADER_DIRECTORY:
        return this->import_data(path, status_callback);
    case ISOCHOP_ISO_LOADER_FILE:
        return this->parseISO(path, status_callback);
    default:
        throw std::invalid_argument("Path can not be parsed");
    }
}

int isochop::ISO::parseISO(const std::string &filepath, void status_callback(int code))
{
    // set callback if one does not exist
    if (status_callback == nullptr)
    {
        status_callback = [](int code) -> void
        {
            return;
        };
    }
    // make return code (in case of warnings)
    int return_code = ISOCHOP_ISO_PARSE_RET_SUCCESS;
    // open the iso file
    if (!std::filesystem::exists(filepath))
    {
        return ISOCHOP_ISO_PARSE_RET_ERROR_FILE_NOT_FOUND;
    }
    if (std::filesystem::is_directory(filepath))
    {
        return ISOCHOP_ISO_PARSE_RET_ERROR_FILE_IS_DIRECTORY;
    }
    status_callback(ISOCHOP_ISO_PARSE_STATUS_OPENING_DISC);
    std::fstream isoFile;
    isoFile.open(filepath, std::fstream::binary | std::fstream::in);
    if (!isoFile.good())
    {
        return ISOCHOP_ISO_PARSE_RET_ERROR_FILE_NOT_GOOD;
    }
    isoFile.seekg(0, isoFile.beg);

    // record the file size
    this->size = std::filesystem::file_size(filepath);

    // read the system area
    status_callback(ISOCHOP_ISO_PARSE_STATUS_READING_SYSTEM_AREA);
    char system_area[ISOCHOP_SYSTEM_AREA_SIZE];
    isoFile.read(system_area, ISOCHOP_SYSTEM_AREA_SIZE);
    this->system_area.clear();
    for (int i = 0; i < ISOCHOP_SYSTEM_AREA_SIZE; i++)
    {
        this->system_area.push_back(system_area[i]);
    }

    // clear volume descriptors
    status_callback(ISOCHOP_ISO_PARSE_STATUS_READING_VOLUME_DESCRIPTORS);
    this->volume_descriptors.clear();
    PrimaryVolumeDescriptor *pmvd = nullptr;

    // keep reading the volume descriptors
    while (true)
    {
        // read a volume descriptor
        char data_arr[ISOCHOP_VD_SIZE_TOTAL];
        unsigned long address = isoFile.tellg();
        isoFile.read(data_arr, ISOCHOP_VD_SIZE_TOTAL);
        std::vector<char> data_vec;
        for (int i = 0; i < ISOCHOP_VD_SIZE_TOTAL; i++)
        {
            data_vec.push_back(data_arr[i]);
        }
        // convert to a volume descriptor object
        VolumeDescriptor *vd = new VolumeDescriptor(data_vec, address);
        // check if volume descriptor is invalid
        if (!vd->isValid())
        {
            // stop reading and return
            return ISOCHOP_ISO_PARSE_RET_ERROR_VD_NOT_VALID;
        }
        // cast to PrimaryVolumeDescriptor if type matches
        if (vd->getType() == ISOCHOP_VD_TYPE_PRIMARY)
        {
            // handle as primary
            vd = new PrimaryVolumeDescriptor(data_vec, address);
            pmvd = (PrimaryVolumeDescriptor *)vd;
            // check that it is still valid
            if (!vd->isValid())
            {
                return_code = ISOCHOP_ISO_PARSE_RET_WARNING_VD_INVALID_CHILD;
            }
        }
        // add to volume descriptors
        this->volume_descriptors.push_back(vd);
        // check if this was the last one
        if (vd->getType() == ISOCHOP_VD_TYPE_TERMINATOR)
        {
            break;
        }
    }

    // load the extent tree
    status_callback(ISOCHOP_ISO_PARSE_STATUS_READING_EXTENT_TREE);
    if (pmvd == nullptr)
    {
        // there was no primary volume descriptor
        return ISOCHOP_ISO_PARSE_RET_ERROR_NO_PRIMARY_VD;
    }
    this->extent_tree = ExtentTree(*pmvd);
    this->extent_tree.parseData(&isoFile);

    // determine if there are any orphans to load
    uintmax_t size = std::filesystem::file_size(filepath);
    std::vector<bool> bits(size, false);
    status_callback(ISOCHOP_ISO_PARSE_STATUS_MARKING_SYSTEM_AREA);
    for (int i = 0; i < ISOCHOP_SYSTEM_AREA_SIZE; i++)
    {
        bits.at(i) = true;
    }
    status_callback(ISOCHOP_ISO_PARSE_STATUS_MARKING_VOLUME_DESCRIPTORS);
    for (VolumeDescriptor *vd : this->volume_descriptors)
    {
        vd->markAddresses(&bits);
    }
    status_callback(ISOCHOP_ISO_PARSE_STATUS_MARKING_EXTENT_TREE);
    this->extent_tree.markAddresses(&bits);

    // load orphans if needed
    status_callback(ISOCHOP_ISO_PARSE_STATUS_READING_ORPHANS);
    std::vector<char> buffer;
    uintmax_t addr = 0;
    for (uintmax_t i = 0; i < size; i++)
    {
        if (!bits.at(i))
        {
            if (buffer.empty())
            {
                addr = i;
            }
            char c;
            isoFile.seekg(i);
            isoFile.read(&c, 1);
            buffer.push_back(c);
        }
        else if (!buffer.empty())
        {
            this->orphans.emplace_back(buffer, addr);
            addr = 0;
            buffer.clear();
        }
    }
    if (!buffer.empty())
    {
        this->orphans.emplace_back(buffer, addr);
    }

    // close the file
    isoFile.close();

    // ISO was read successfully
    status_callback(ISOCHOP_ISO_PARSE_STATUS_DONE);
    return return_code;
}

int isochop::ISO::writeISO(const std::string &filepath, void status_callback(int code))
{
    // set callback if one does not exist
    if (status_callback == nullptr)
    {
        status_callback = [](int code) -> void
        {
            return;
        };
    }
    // make return code (in case of warnings)
    int return_code = ISOCHOP_ISO_WRITE_RET_SUCCESS;
    // open the iso file
    status_callback(ISOCHOP_ISO_WRITE_STATUS_OPENING_DISC);
    if (std::filesystem::exists(filepath))
    {
        return ISOCHOP_ISO_WRITE_RET_PATH_EXISTS;
    }
    // open file for writing
    std::fstream file;
    file.open(filepath, std::fstream::out | std::fstream::binary);
    if (!file.good())
    {
        return ISOCHOP_ISO_WRITE_RET_FILE_NOT_GOOD;
    }
    file.seekp(0);
    // preallocate file
    status_callback(ISOCHOP_ISO_WRITE_STATUS_PREALLOCATING_DISC);
    for (unsigned long i = 0; i < this->size; i++)
    {
        file.write("\0", 1);
    }
    // write system area
    status_callback(ISOCHOP_ISO_WRITE_STATUS_WRITING_SYSTEM_AREA);
    file.seekp(0);
    for (char c : this->system_area)
    {
        file.write(&c, 1);
    }
    // write volume descriptors
    status_callback(ISOCHOP_ISO_WRITE_STATUS_WRITING_VOLUME_DESCRIPTORS);
    for (VolumeDescriptor *vd : this->volume_descriptors)
    {
        file.seekp(vd->getAddress());
        for (char c : vd->getData())
        {
            file.write(&c, 1);
        }
    }
    // write extent tree
    status_callback(ISOCHOP_ISO_WRITE_STATUS_WRITING_EXTENT_TREE);
    this->extent_tree.writeData(&file);
    // write orphans
    status_callback(ISOCHOP_ISO_WRITE_STATUS_WRITING_ORPHANS);
    for (Orphan orphan : this->orphans)
    {
        file.seekp(orphan.getAddress());
        for (char c : orphan.getData())
        {
            file.write(&c, 1);
        }
    }
    // exit writer
    status_callback(ISOCHOP_ISO_WRITE_STATUS_DONE);
    return return_code;
}

bool isochop::ISO::isValid()
{
    bool valid = true;
    for (VolumeDescriptor *vd : this->volume_descriptors)
    {
        valid &= vd->isValid();
    }
    return valid;
}

std::vector<char> isochop::ISO::getSystemArea()
{
    // return a copy of the system area
    return this->system_area;
}

void isochop::ISO::setSystemArea(const std::vector<char> &data)
{
    // check data size
    if (data.size() != ISOCHOP_SYSTEM_AREA_SIZE)
    {
        throw std::invalid_argument("data.size() != ISOCHOP_SYSTEM_AREA_SIZE");
    }
    // copy to internal vector
    this->system_area = data;
}

std::vector<isochop::VolumeDescriptor *> *isochop::ISO::getVolumeDescriptors()
{
    // return the volume descriptors vector
    return &this->volume_descriptors;
}

void isochop::ISO::setVolumeDescriptors(std::vector<VolumeDescriptor *> *vds)
{
    // ensure vector pointer is not null
    if (vds == nullptr)
    {
        throw std::invalid_argument("vds == nullptr");
    }
    // set the vector pointer
    this->volume_descriptors = *vds;
}

isochop::ExtentTree isochop::ISO::getTree()
{
    return this->extent_tree;
}

void isochop::ISO::setTree(const isochop::ExtentTree &tree)
{
    this->extent_tree = tree;
}

std::vector<isochop::Orphan> isochop::ISO::getOrphans()
{
    return this->orphans;
}

void isochop::ISO::setOrphans(const std::vector<Orphan> &orphans)
{
    this->orphans = orphans;
}

unsigned long isochop::ISO::getSize()
{
    return this->size;
}

void isochop::ISO::setSize(unsigned long size)
{
    this->size = size;
}

std::string isochop::ISO::toString()
{
    std::string str;
    str += "Size: " + std::to_string(this->size) + "\n";
    str += "===== Volume Descriptors =====\n";
    str += this->showVolumeDescriptors();
    str += "===== Extent Tree Info =====\n";
    str += this->showExtentTreeToString() + "\n";
    str += "===== Extent Tree Structure =====\n";
    str += this->showTree(4);
    return str;
}

isochop::ISO isochop::ISO::clone()
{
    // make container
    ISO iso;
    // deep copy all fields
    iso.setSystemArea(this->system_area);
    for (VolumeDescriptor *vd : this->volume_descriptors)
    {
        VolumeDescriptor *neo = vd->clone();
        iso.volume_descriptors.push_back(neo);
    }
    iso.extent_tree = this->extent_tree.clone();
    iso.setOrphans(this->orphans);
    // return the new iso object
    return iso;
}

std::string isochop::ISO::showVolumeDescriptors()
{
    std::string str;
    for (VolumeDescriptor *vd : this->volume_descriptors)
    {
        str += vd->toString() + "\n\n";
    }
    return str;
}

std::string isochop::ISO::showExtentTreeToString()
{
    return this->extent_tree.toString();
}

std::string isochop::ISO::showTree(size_t indentSpaces)
{
    return this->extent_tree.tree(indentSpaces);
}

std::string isochop::ISO::export_data(const std::string &dirpath, void status_callback(int code))
{
    // set callback if one does not exist
    if (status_callback == nullptr)
    {
        status_callback = [](int code) -> void
        {
            return;
        };
    }
    // ensure export directory exists
    status_callback(ISOCHOP_ISO_EXPORT_PREPARING_DIRECTORY);
    if (std::filesystem::exists(dirpath))
    {
        // destroy the path to ensure it is a clean directory
        std::filesystem::remove_all(dirpath);
    }
    std::filesystem::create_directory(dirpath);
    // get the SHA256 hash of the system area and store it
    status_callback(ISOCHOP_ISO_EXPORT_WRITING_SYSTEM_AREA);
    std::string sha256system = utils::sha256sum(&this->system_area);
    utils::dump_vector(&this->system_area, dirpath + "/" + sha256system, nullptr);
    // export and store the SHA256 hash of the ExtentTree
    status_callback(ISOCHOP_ISO_EXPORT_WRITING_EXTENT_TREE);
    std::string sha256extent = this->extent_tree.export_data(dirpath);
    // export volume descriptors
    status_callback(ISOCHOP_ISO_EXPORT_WRITING_VOLUME_DESCRIPTORS);
    std::vector<std::string> sha256descriptors;
    for (VolumeDescriptor *vd : this->volume_descriptors)
    {
        std::string hash = vd->export_data(dirpath);
        sha256descriptors.push_back(hash);
    }
    // export orphans
    status_callback(ISOCHOP_ISO_EXPORT_WRITING_ORPHANS);
    std::vector<std::string> sha256orphans;
    for (Orphan o : this->orphans)
    {
        std::string hash = o.export_data(dirpath);
        sha256orphans.push_back(hash);
    }
    // generate manifest JSON
    status_callback(ISOCHOP_ISO_EXPORT_GENERATING_MANIFEST);
    nlohmann::json manifestJSON;
    manifestJSON["manifest_class"] = "ISO";
    manifestJSON["file_size"] = this->size;
    manifestJSON["system_hash"] = sha256system;
    manifestJSON["extenttree_hash"] = sha256extent;
    manifestJSON["volumedescriptor_hashes"] = sha256descriptors;
    manifestJSON["orphan_hashes"] = sha256orphans;
    // generate manifest string
    std::string manifestString = manifestJSON.dump(4, ' ', true);
    // calculate manifest hash
    std::vector<char> tmp;
    for (char c : manifestString)
    {
        tmp.push_back(c);
    }
    std::string manifestSHA256 = utils::sha256sum(&tmp);
    tmp.clear();
    // write JSON to file
    status_callback(ISOCHOP_ISO_EXPORT_WRITING_MANIFEST);
    std::fstream file;
    file.open(dirpath + "/" + manifestSHA256, std::fstream::out);
    file << manifestString;
    file.close();
    // write the ISO's hash to a file
    status_callback(ISOCHOP_ISO_EXPORT_WRITING_STRUCTURE_ROOT);
    file.open(dirpath + "/" + "STRUCTURE_ROOT", std::fstream::out);
    file << manifestSHA256;
    file.close();
    // return this ISO's SHA256 hash
    status_callback(ISOCHOP_ISO_EXPORT_DONE);
    return manifestSHA256;
}

int isochop::ISO::import_data(const std::string &dirpath, void status_callback(int code))
{
    // set callback if one does not exist
    if (status_callback == nullptr)
    {
        status_callback = [](int code) -> void
        {
            return;
        };
    }
    // check if dirpath is valid
    status_callback(ISOCHOP_ISO_IMPORT_VALIDATE_DIRECTORY);
    if (!isochop::ISO::isExportValid(dirpath))
    {
        throw std::invalid_argument("The ISO export directory is not valid.");
    }
    // load STRUCTURE_ROOT
    status_callback(ISOCHOP_ISO_IMPORT_STATUS_OPENING_ROOT);
    std::fstream sRootFile;
    sRootFile.open(dirpath + "/STRUCTURE_ROOT", std::fstream::in);
    if (!sRootFile.good())
    {
        return ISOCHOP_ISO_IMPORT_RET_ROOT_FSTREAM_NOT_GOOD;
    }
    status_callback(ISOCHOP_ISO_IMPORT_STATUS_READING_ROOT);
    std::string sha256ISO;
    sRootFile >> sha256ISO;
    sRootFile.close();
    // load the ISO manifest
    status_callback(ISOCHOP_ISO_IMPORT_STATUS_OPENING_MANIFEST);
    std::fstream manifestFile;
    manifestFile.open(dirpath + "/" + sha256ISO, std::fstream::in);
    if (!manifestFile.good())
    {
        return ISOCHOP_ISO_IMPORT_RET_ISO_FSTREAM_NOT_GOOD;
    }
    status_callback(ISOCHOP_ISO_IMPORT_STATUS_READING_MANIFEST);
    nlohmann::json manifestJSON;
    manifestFile >> manifestJSON;
    manifestFile.close();
    // load JSON fields
    std::string sha256system = manifestJSON["system_hash"];
    std::string sha256extent = manifestJSON["extenttree_hash"];
    std::vector<std::string> sha256descriptors = manifestJSON["volumedescriptor_hashes"];
    std::vector<std::string> sha256orphans = manifestJSON["orphan_hashes"];
    unsigned long file_size = manifestJSON["file_size"];
    // load the file size
    this->size = file_size;
    // load the system area
    status_callback(ISOCHOP_ISO_IMPORT_STATUS_READING_SYSTEM_AREA);
    this->system_area = utils::load_vector(dirpath + "/" + sha256system, nullptr);
    // load volume descriptors
    status_callback(ISOCHOP_ISO_IMPORT_STATUS_READING_VOLUME_DESCRIPTORS);
    this->volume_descriptors.clear();
    for (const std::string &sha256descriptor : sha256descriptors)
    {
        // make a volume descriptor
        VolumeDescriptor *vd = new VolumeDescriptor(dirpath, sha256descriptor);
        // convert the primary
        if (vd->getType() == ISOCHOP_VD_TYPE_PRIMARY)
        {
            vd = new PrimaryVolumeDescriptor(vd->getData(), vd->getAddress());
        }
        // store the volume descriptor
        this->volume_descriptors.push_back(vd);
    }
    // load the extent tree
    status_callback(ISOCHOP_ISO_IMPORT_STATUS_READING_EXTENT_TREE);
    this->extent_tree = ExtentTree(dirpath, sha256extent);
    // load orphans
    status_callback(ISOCHOP_ISO_IMPORT_STATUS_READING_ORPHANS);
    this->orphans.clear();
    for (const std::string &sha256orphan : sha256orphans)
    {
        Orphan orphan = Orphan(dirpath, sha256orphan);
        this->orphans.push_back(orphan);
    }
    status_callback(ISOCHOP_ISO_IMPORT_STATUS_DONE);
    return ISOCHOP_ISO_IMPORT_RET_SUCCESS;
}

bool isochop::ISO::isExportValid(const std::string &dirpath)
{
    // Does the directory path exist?
    if (!std::filesystem::exists(dirpath))
    {
        return false;
    }
    // Is the directory path pointing to a directory?
    if (!std::filesystem::is_directory(dirpath))
    {
        return false;
    }
    // Does the directory contain a STRUCTURE_ROOT file?
    if (!std::filesystem::exists(dirpath + "/STRUCTURE_ROOT"))
    {
        return false;
    }
    // Is STRUCTURE_ROOT a file (or other non-directory entity)?
    if (std::filesystem::is_directory(dirpath + "/STRUCTURE_ROOT"))
    {
        return false;
    }
    // Is the manifest tree valid?
    std::fstream structureFile;
    structureFile.open(dirpath + "/STRUCTURE_ROOT");
    std::string sha256iso;
    structureFile >> sha256iso;
    structureFile.close();
    return isochop::ISO::isFileValid(dirpath, sha256iso, true);
}

bool isochop::ISO::isFileValid(const std::string &dirpath, const std::string &hash, bool manifest)
{
    // NOTE: if this is called, dirpath is known to be valid
    // Calculate manifest path
    std::string path = dirpath + "/" + hash;
    // Does the manifest file exist?
    if (!std::filesystem::exists(path))
    {
        return false;
    }
    // Is the manifest a file (or other non-directory entity)?
    if (std::filesystem::is_directory(path))
    {
        return false;
    }
    // Is the SHA256 hash correct
    bool readError = false;
    std::vector<char> data = utils::load_vector(path, &readError);
    if (readError)
    {
        return false;
    }
    if (hash != utils::sha256sum(&data))
    {
        return false;
    }
    // Is there an error when loading?
    nlohmann::json manifestJSON;
    try
    {
        std::fstream file;
        file.open(path, std::fstream::in);
        if (manifest)
        {
            file >> manifestJSON;
        }
        file.close();
    }
    catch (...)
    {
        // NOTE: The exception doesn't matter. The file didn't load and it won't if importing.
        return false;
    }
    // Stop checking if this is not a manifest
    if (!manifest)
    {
        // NOTE: If this line is executed, the file was fine
        return true;
    }
    // Read the class from the manifest and test from there
    std::string manifest_class = manifestJSON["manifest_class"];
    if (manifest_class == "ISO")
    {
        // Read fields from disc
        std::string sha256sys = manifestJSON["system_hash"];
        std::string sha256ext = manifestJSON["extenttree_hash"];
        std::vector<std::string> sha256volumedescriptors = manifestJSON["volumedescriptor_hashes"];
        std::vector<std::string> sha256orphans = manifestJSON["orphan_hashes"];
        // Is the system reserved area valid?
        if (!isochop::ISO::isFileValid(dirpath, sha256sys, false))
        {
            return false;
        }
        // Are the volume descriptors valid?
        for (const std::string &sha256str : sha256volumedescriptors)
        {
            if (!isochop::ISO::isFileValid(dirpath, sha256str, true))
            {
                return false;
            }
        }
        // Is the extent tree valid?
        if (!isochop::ISO::isFileValid(dirpath, sha256ext, true))
        {
            return false;
        }
        // Are the orphans valid?
        for (const std::string &sha256str : sha256orphans)
        {
            if (!isochop::ISO::isFileValid(dirpath, sha256str, true))
            {
                return false;
            }
        }
    }
    else if (manifest_class == "ExtentTree")
    {
        // Read field from disc
        std::string sha256ext = manifestJSON["extentnode_hash"];
        // Check the Extent Node
        if (!isochop::ISO::isFileValid(dirpath, sha256ext, true))
        {
            return false;
        }
    }
    else if (manifest_class == "ExtentNode")
    {
        // Read fields from disc
        std::string sha256dir = manifestJSON["directory_hash"];
        std::string sha256ext = manifestJSON["extent_hash"];
        std::vector<std::string> sha256children = manifestJSON["children_hashes"];
        // Is the directory valid?
        if (!isochop::ISO::isFileValid(dirpath, sha256dir, false))
        {
            return false;
        }
        // Is the extent data valid?
        if (!isochop::ISO::isFileValid(dirpath, sha256ext, false))
        {
            return false;
        }
        // Are the child ExtentNodes valid?
        for (const std::string &sha256str : sha256children)
        {
            if (!isochop::ISO::isFileValid(dirpath, sha256str, true))
            {
                return false;
            }
        }
    }
    else if (manifest_class == "Orphan")
    {
        // Read field from disc
        std::string sha256data = manifestJSON["data_hash"];
        // Check the data
        if (!isochop::ISO::isFileValid(dirpath, sha256data, false))
        {
            return false;
        }
    }
    else if (manifest_class == "VolumeDescriptor")
    {
        // Read field from disc
        std::string sha256data = manifestJSON["data_hash"];
        // Check the data
        if (!isochop::ISO::isFileValid(dirpath, sha256data, false))
        {
            return false;
        }
    }
    // If this line executes, nothing failed
    return true;
}

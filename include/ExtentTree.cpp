/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ExtentTree.hpp"
#include "ExtentNode.hpp"
#include "PrimaryVolumeDescriptor.hpp"
#include "utils.hpp"
#include "json/single_include/nlohmann/json.hpp"
#include <string>
#include <vector>
#include <fstream>

isochop::ExtentTree::ExtentTree()
{
    // set an empty root directory
    this->setRoot(ExtentNode());
}

isochop::ExtentTree::ExtentTree(const ExtentNode &root)
{
    // use the given root directory
    this->setRoot(root);
}

isochop::ExtentTree::ExtentTree(PrimaryVolumeDescriptor pmvd)
{
    // get root directory from primary volume descriptor
    Directory rootDir = pmvd.getRootDirectoryEntry();
    // get the starting address
    unsigned long rootAddr = utils::convertSectorToByteAddress(rootDir.getLocationOfExtent());
    // generate ExtentNode
    ExtentNode rootNode = ExtentNode(rootDir, rootAddr);
    // set the root directory
    this->setRoot(rootNode);
}

isochop::ExtentTree::ExtentTree(const std::string &dirpath, const std::string &manifesthash)
{
    // load Extent Tree from a directory path
    this->import_data(dirpath, manifesthash);
}

isochop::ExtentTree::~ExtentTree()
{
    // Valgrind does not report any errors at this time
}

std::string isochop::ExtentTree::toString()
{
    // print the root file node, it should recursively travel from there
    return this->root.toString();
}

isochop::ExtentTree isochop::ExtentTree::clone()
{
    // clone the root node
    ExtentTree et = ExtentTree(this->getRoot());
    // return the new tree
    return et;
}

isochop::ExtentNode isochop::ExtentTree::getRoot()
{
    // return a copy
    return this->root.clone();
}

void isochop::ExtentTree::setRoot(ExtentNode root)
{
    // use a clone
    this->root = root.clone();
}

void isochop::ExtentTree::parseData(std::fstream *file)
{
    // error if file is bad
    if (file == nullptr)
    {
        throw std::invalid_argument("file == nullptr");
    }
    // tell root to parse the tree
    std::vector<uint64_t> parsedAddresses;
    this->root.parseData(file, &parsedAddresses);
}

void isochop::ExtentTree::writeData(std::fstream *file)
{
    // error if file is bad
    if (file == nullptr)
    {
        throw std::invalid_argument("file == nullptr");
    }
    // tell root to write the tree
    this->root.writeData(file);
}

std::string isochop::ExtentTree::tree(size_t indentSpaces)
{
    // call tree command on ExtentNodes
    return this->root.tree(0, indentSpaces);
}

std::string isochop::ExtentTree::export_data(const std::string &dirpath)
{
    // get the SHA256 hash of the root ExtentNode
    std::string sha256extent = this->root.export_data(dirpath);
    // generate manifest JSON
    nlohmann::json manifestJSON;
    manifestJSON["manifest_class"] = "ExtentTree";
    manifestJSON["extentnode_hash"] = sha256extent;
    // generate manifest string
    std::string manifestString = manifestJSON.dump(4, ' ', true);
    // calculate manifest hash
    std::vector<char> tmp;
    for (char c : manifestString)
    {
        tmp.push_back(c);
    }
    std::string manifestSHA256 = utils::sha256sum(&tmp);
    tmp.clear();
    // write JSON to file
    std::fstream file;
    file.open(dirpath + "/" + manifestSHA256, std::fstream::out);
    file << manifestString;
    file.close();
    // return this ExtentTree's SHA256 hash
    return manifestSHA256;
}

void isochop::ExtentTree::import_data(const std::string &dirpath, const std::string &manifesthash)
{
    // parse manifest file
    std::fstream manifestFile;
    manifestFile.open(dirpath + "/" + manifesthash, std::fstream::in);
    nlohmann::json manifestJSON;
    manifestFile >> manifestJSON;
    manifestFile.close();
    // load data from file
    std::string sha256extent = manifestJSON["extentnode_hash"];
    // make the ExtentNode (this will initialize the rest of the tree)
    this->root = ExtentNode(dirpath, sha256extent);
}

void isochop::ExtentTree::markAddresses(std::vector<bool> *bits)
{
    this->root.markAddresses(bits);
}

# credit for parallel code goes to https://stackoverflow.com/a/22921879
# credit for the ar command codes to https://stackoverflow.com/a/5947126

LANG= en_US
CC= g++
INC= -Wall -lcrypto
CFLAGS= -Os -c -std=c++17 $(INC)
JSON_PATH= json/single_include/nlohmann/json.hpp
OBJ_FILES=                        \
	isochop.o                 \
	utils.o                   \
	Orphan.o                  \
	VolumeDescriptor.o        \
	ISO.o                     \
	PrimaryVolumeDescriptor.o \
	DecDatetime.o             \
	DirDatetime.o             \
	FileFlags.o               \
	Directory.o               \
	ExtentData.o              \
	ExtentNode.o              \
	ExtentTree.o

#
#   ____          _
#  / ___|___   __| | ___
# | |   / _ \ / _` |/ _ \
# | |__| (_) | (_| |  __/
#  \____\___/ \__,_|\___|
#

isolib:
	$(MAKE) -j isochop.a PARALLEL=true

isochop.a: $(OBJ_FILES)
	ar rvs $@ $^

isochop.o:                   \
	isochop.hpp          \
	definitions.hpp      \
	ISO.hpp              \
	VolumeDescriptor.hpp \
	ExtentTree.hpp       \
	Orphan.hpp

utils.o:          \
	utils.hpp \
	definitions.hpp

Orphan.o:          \
	Orphan.hpp \
	$(JSON_PATH)

DecDatetime.o:          \
	DecDatetime.hpp \
	definitions.hpp \
	utils.hpp

DirDatetime.o:          \
	DirDatetime.hpp \
	definitions.hpp \
	utils.hpp

FileFlags.o:            \
	FileFlags.hpp   \
	definitions.hpp \
	utils.hpp

Directory.o:            \
	Directory.hpp   \
	FileFlags.hpp   \
	definitions.hpp \
	utils.hpp

ExtentData.o:          \
	ExtentData.hpp \
	$(JSON_PATH)

ExtentNode.o:          \
	ExtentNode.hpp \
	ExtentData.hpp \
	Directory.hpp  \
	utils.cpp      \
	$(JSON_PATH)

ExtentTree.o:                       \
	ExtentTree.hpp              \
	ExtentNode.hpp              \
	PrimaryVolumeDescriptor.hpp \
	utils.hpp

VolumeDescriptor.o:          \
	VolumeDescriptor.hpp \
	definitions.hpp      \
	utils.hpp            \
	DecDatetime.hpp      \
	$(JSON_PATH)

PrimaryVolumeDescriptor.o:          \
	PrimaryVolumeDescriptor.hpp \
	definitions.hpp             \
	VolumeDescriptor.hpp        \
	DecDatetime.hpp             \
	utils.hpp                   \
	Directory.hpp

ISO.o:                              \
	ISO.hpp                     \
	utils.hpp                   \
	definitions.hpp             \
	VolumeDescriptor.hpp        \
	PrimaryVolumeDescriptor.hpp \
	ExtentTree.hpp              \
	Orphan.hpp                  \
	$(JSON_PATH)

#  __  __ _
# |  \/  (_)___  ___
# | |\/| | / __|/ __|
# | |  | | \__ \ (__ _
# |_|  |_|_|___/\___(_)
#

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f $(OBJ_FILES)
	rm -f isochop.a

.PHONY: isolib clean

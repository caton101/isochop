/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// only include once
#pragma once

// includes
#include <string>
#include <vector>
#include <cstdint>

// put code behind library namespace
namespace isochop
{
    /**
     * @brief  This class implements a dec-datetime entry
     * @note   all fields and methods follow the ISO 9660 specification
     */
    class DecDatetime
    {
    protected:
        // this stores the dec-datetime information
        std::vector<char> data;

    public:
        /**
         * @brief  The no-args constructor for a dec-datetime object
         * @note   default values may not be valid
         * @retval a new dec-datetime object
         */
        DecDatetime();

        /**
         * @brief  The normal constructor for a dec-datetime object
         * @note   data vector must be of size DEC_DATETIME_SIZE_TOTAL
         * @param  data: data vector to parse
         * @retval a new dec-datetime object
         */
        DecDatetime(const std::vector<char> &data);

        /**
         * @brief  The deconstructor for a dec-datetime object
         */
        ~DecDatetime();

        /**
         * @brief  Checks if all values are valid
         * @retval true if all values are valid
         */
        bool isValid();

        /**
         * @brief  Generates a string summarizing the dec-datetime
         * @note   string output may change format in future versions
         * @retval string representing the dec-datetime
         */
        std::string toString();

        /**
         * @brief  Fetch the data vector
         * @retval data vector
         */
        std::vector<char> getData();

        /**
         * @brief  Set the data vector
         * @note   must be of size DEC_DATETIME_SIZE_TOTAL
         * @param  data: data vector
         * @retval None
         */
        void setData(const std::vector<char> &data);

        /**
         * @brief  Fetch the year field
         * @retval the year
         */
        std::string getYear();

        /**
         * @brief  Set the year field
         * @note   must be of size DEC_DATETIME_SIZE_YEAR
         * @param  year: year string
         * @retval None
         */
        void setYear(const std::string &year);

        /**
         * @brief  Fetch the month field
         * @retval the month
         */
        std::string getMonth();

        /**
         * @brief  Set the month field
         * @note   must be of size DEC_DATETIME_SIZE_MONTH
         * @param  month: month string
         * @retval None
         */
        void setMonth(const std::string &month);

        /**
         * @brief  Fetch the day field
         * @retval the day
         */
        std::string getDay();

        /**
         * @brief  Set the day field
         * @note   must be of size DEC_DATETIME_SIZE_DAY
         * @param  day: day string
         * @retval None
         */
        void setDay(const std::string &day);

        /**
         * @brief  Fetch the hour field
         * @retval the hour
         */
        std::string getHour();

        /**
         * @brief  Set the hour field
         * @note   must be of size DEC_DATETIME_SIZE_HOUR
         * @param  hour: hour string
         * @retval None
         */
        void setHour(const std::string &hour);

        /**
         * @brief  Fetch the minute field
         * @retval the minute
         */
        std::string getMinute();

        /**
         * @brief  Set the minute field
         * @note   must be of size DEC_DATETIME_SIZE_MINUTE
         * @param  minute: minute string
         * @retval None
         */
        void setMinute(const std::string &minute);

        /**
         * @brief  Fetch the second field
         * @retval the second
         */
        std::string getSecond();

        /**
         * @brief  Set the second field
         * @note   must be of size DEC_DATETIME_SIZE_SECOND
         * @param  second: second string
         * @retval None
         */
        void setSecond(const std::string &second);

        /**
         * @brief  Fetch the hundredths field
         * @retval the hundredths
         */
        std::string getHundredths();

        /**
         * @brief  Set the hundredths field
         * @note   must be of size DEC_DATETIME_SIZE_HUNDREDTHS
         * @param  hundredths: hundredths string
         * @retval None
         */
        void setHundredths(const std::string &hundredths);

        /**
         * @brief  Fetch the timezone field
         * @retval the timezone
         */
        uint8_t getTimezone();

        /**
         * @brief  Set the timezone field
         * @note   must be of size DEC_DATETIME_SIZE_TIMEZONE
         * @param  timezone: timezone string
         * @retval None
         */
        void setTimezone(uint8_t timezone);
    };
}

/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <vector>
#include <filesystem>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "testCommon.hpp"
#include "testStructure.hpp"
#include "../include/isochop.hpp"
#include "../include/utils.hpp"

bool testStructure::test_structure_export()
{
    // settings
    std::string isopath = "../iso/test.iso";
    std::string exportpath = "exported_iso";
    std::string correctpath = "../iso/exported";
    // ensure directory is gone
    if (std::filesystem::exists(exportpath))
    {
        std::filesystem::remove_all(exportpath);
    }
    // Attempt to export. If anything goes wrong, fail the test.
    try
    {
        isochop::ISOChop iso = isochop::ISOChop(isopath, nullptr, nullptr);
        iso.makeDirectory(exportpath, nullptr);
    }
    catch (...)
    {
        return false;
    }
    // if export directory does not exist, fail the test
    if (!std::filesystem::exists(exportpath))
    {
        return false;
    }
    if (!std::filesystem::is_directory(exportpath))
    {
        return false;
    }
    // get directory listings
    std::vector<std::string> exportedFiles;
    for (std::filesystem::directory_entry dir : std::filesystem::recursive_directory_iterator(exportpath))
    {
        exportedFiles.push_back(dir.path().filename());
    }
    std::vector<std::string> correctFiles;
    for (std::filesystem::directory_entry dir : std::filesystem::recursive_directory_iterator(correctpath))
    {
        correctFiles.push_back(dir.path().filename());
    }
    // compare files to each other
    for (std::string correctPath : correctFiles)
    {
        std::vector<std::string>::iterator cursor = std::find(exportedFiles.begin(), exportedFiles.end(), correctPath);
        if (cursor != exportedFiles.end())
        {
            // file exists, remove it from the vector
            exportedFiles.erase(cursor);
        }
        else
        {
            // fail the test
            return false;
        }
    }
    // if there is anything left over, fail the test
    if (exportedFiles.size() > 0)
    {
        return false;
    }
    return true;
}

bool testStructure::test_structure_import()
{
    // settings
    std::string isopath = "../iso/test.iso";
    std::string exportpath = "exported_iso";
    std::string outputpath = "out.iso";
    std::string correctpath = "../iso/exported";
    // ensure directory is gone
    if (std::filesystem::exists(exportpath))
    {
        std::filesystem::remove_all(exportpath);
    }
    // Attempt to export. If anything goes wrong, fail the test.
    try
    {
        isochop::ISOChop iso = isochop::ISOChop(isopath, nullptr, nullptr);
        iso.makeDirectory(exportpath, nullptr);
    }
    catch (...)
    {
        return false;
    }
    // if export directory does not exist, fail the test
    if (!std::filesystem::exists(exportpath))
    {
        return false;
    }
    if (!std::filesystem::is_directory(exportpath))
    {
        return false;
    }
    // attempt to make ISO file
    try
    {
        isochop::ISOChop iso = isochop::ISOChop(exportpath, nullptr, nullptr);
        iso.makeFile(outputpath, nullptr);
    }
    catch (...)
    {
        return false;
    }
    // if iso file does not exist, fail the test
    if (!std::filesystem::exists(outputpath))
    {
        return false;
    }
    if (std::filesystem::is_directory(outputpath))
    {
        return false;
    }
    // compare the hashes
    std::vector<char> correctData = isochop::utils::load_vector(isopath, nullptr);
    std::string correctHash = isochop::utils::sha256sum(&correctData);
    std::vector<char> outData = isochop::utils::load_vector(outputpath, nullptr);
    std::string outHash = isochop::utils::sha256sum(&outData);
    return outHash == correctHash;
}

bool testStructure::test_structure_hashes()
{
    // settings
    std::string isopath = "../iso/test.iso";
    std::string exportpath = "exported_iso";
    std::string outputpath = "out.iso";
    std::string correctpath = "../iso/exported";
    // ensure directory is gone
    if (std::filesystem::exists(exportpath))
    {
        std::filesystem::remove_all(exportpath);
    }
    // Attempt to export. If anything goes wrong, fail the test.
    try
    {
        isochop::ISOChop iso = isochop::ISOChop(isopath, nullptr, nullptr);
        iso.makeDirectory(exportpath, nullptr);
    }
    catch (...)
    {
        return false;
    }
    // if export directory does not exist, fail the test
    if (!std::filesystem::exists(exportpath))
    {
        return false;
    }
    if (!std::filesystem::is_directory(exportpath))
    {
        return false;
    }
    // compare file names to their hashes
    for (std::filesystem::directory_entry dir : std::filesystem::recursive_directory_iterator(exportpath))
    {
        std::vector<char> testData = isochop::utils::load_vector(dir.path(), nullptr);
        std::string testHash = isochop::utils::sha256sum(&testData);
        std::vector<char> correctData = isochop::utils::load_vector(correctpath + "/" + dir.path().filename().c_str(), nullptr);
        std::string correctHash = isochop::utils::sha256sum(&correctData);
        if (correctHash != testHash)
        {
            return false;
        }
    }
    return true;
}

TEST_VECTOR_T testStructure::getTests()
{
    TEST_VECTOR_T tests;
    pushTest(&tests, test_structure_export, "Structure: export");
    pushTest(&tests, test_structure_import, "Structure: import");
    pushTest(&tests, test_structure_hashes, "Structure: hashes");
    return tests;
}
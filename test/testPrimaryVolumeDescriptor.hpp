/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"

/**
 * @brief  This tests all methods inside the PrimaryVolumeDescriptor class
 */
namespace testPrimaryVolumeDescriptor
{
    /**
     * @brief  A test for PrimaryVolumeDescriptor's setAddress and getAddress methods
     * @note   this checks if the address returned is the same as it was set to
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setaddress_getaddress();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setData and getData methods
     * @note   this checks if the data array is altered upon setting or returning
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setdata_getdata();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getType method
     * @note   Checks if a type is fetched correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_gettype();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setType method
     * @note   Checks if a type is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_settype();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getIdentifier method
     * @note   Checks if an identifier is fetched correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getidentifier();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setIdentifier method
     * @note   Checks if an identifier is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setidentifier();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getversion method
     * @note   Checks if a version is fetched correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getversion();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setVersion method
     * @note   Checks if a version is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setversion();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a blank image is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_1();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a sole type is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_2();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a sole identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_3();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a sole version is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_4();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a sole system identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_5();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a sole volume identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_6();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a sole volume set identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_7();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a sole publisher identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_8();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a sole data preparer identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_9();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a sole application identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_10();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a sole copyright file identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_11();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a sole abstract file identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_12();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a sole biblographic file identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_13();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a sole file structure version is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_14();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a correct header is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_15();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a wrong type is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_16();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a wrong identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_17();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a wrong version is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_18();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a wrong system identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_19();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a wrong volume identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_20();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a wrong volume set identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_21();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a wrong publisher identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_22();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a wrong data preparer identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_23();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a wrong application identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_24();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a wrong copyright file identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_25();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a wrong abstract file identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_26();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a wrong biblographic file identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_27();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's isValid method
     * @note   Checks if a wrong file structure version is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_isvalid_28();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's clone method
     * @note   Checks if the raw array in both is the same
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_clone();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's no-args constructor
     * @note   Checks if the no-args constructor makes a legal volume descriptor
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_1();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if the address is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_2();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a blank image is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_3();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a sole identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_4();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a sole version is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_5();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a sole system identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_6();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a sole volume identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_7();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a sole volume set identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_8();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a sole publisher identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_9();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a sole data preparer identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_10();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a sole application identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_11();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a sole copyright file identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_12();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a sole abstract file identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_13();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a sole biblographic file identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_14();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a sole file structure version is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_15();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a correct header is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_16();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a wrong type is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_17();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a wrong identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_18();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a wrong version is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_19();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a wrong system identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_20();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a wrong volume identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_21();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a wrong volume set identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_22();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a wrong publisher identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_23();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a wrong data preparer identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_24();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a wrong application identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_25();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a wrong copyright file identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_26();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a wrong abstract file identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_27();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a wrong biblographic file identifier is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_28();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's constructor
     * @note   Checks if a wrong file structure version is valid
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_constructor_29();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's markAddresses method
     * @note   Checks for an exception when bits is null
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_markaddresses_1();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's markAddresses method
     * @note   Checks if a PrimaryVolumeDescriptor object marks its addresses correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_markaddresses_2();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getSystemIdentifier method
     * @note   Checks if volume identifier is fetched correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getsystemidentifier();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setSystemIdentifier method
     * @note   Checks if an exception is thrown when system identifier is a wrong size
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setsystemidentifier_1();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setSystemIdentifier method
     * @note   Checks if the system identifier is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setsystemidentifier_2();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getVolumeIdentifier method
     * @note   Checks if volume identifier is fetched correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getvolumeidentifier();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setVolumeIdentifier method
     * @note   Checks if an exception is thrown when volume identifier is a wrong size
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setvolumeidentifier_1();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setVolumeIdentifier method
     * @note   Checks if the volume identifier is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setvolumeidentifier_2();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getVolumeSpaceSize method
     * @note   Checks if the volume space size is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getvolumespacesize();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setVolumeSpaceSize method
     * @note   Checks if the volume space size is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setvolumespacesize();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getVolumeSetSize method
     * @note   Checks if the volume set size is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getvolumesetsize();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setVolumeSetSize method
     * @note   Checks if the volume set size is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setvolumesetsize();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getVolumeSequenceNumber method
     * @note   Checks if the volume sequence number is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getvolumesequencenumber();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setVolumeSequenceNumber method
     * @note   Checks if the volume sequence number is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setvolumesequencenumber();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getLogicalBlockSize method
     * @note   Checks if the logical block size is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getlogicalblocksize();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setLogicalBlockSize method
     * @note   Checks if the logical block size is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setlogicalblocksize();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getPathTableSize method
     * @note   Checks if the path table size is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getpathtablesize();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setPathTableSize method
     * @note   Checks if the path table size is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setpathtablesize();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getLocationOfTypeLPathTable method
     * @note   Checks if the location of the type L path table is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getlocationoftypelpathtable();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setLocationOfTypeLPathTable method
     * @note   Checks if the location of the type L path table is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setlocationoftypelpathtable();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getLocationOfOptionalTypeLPathTable method
     * @note   Checks if the location of the optional type L path table is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getlocationofoptionaltypelpathtable();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setLocationOfOptionalTypeLPathTable method
     * @note   Checks if the location of the optional type L path table is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setlocationofoptionaltypelpathtable();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getLocationOfTypeMPathTable method
     * @note   Checks if the location of the type M path table is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getlocationoftypempathtable();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setLocationOfTypeMPathTable method
     * @note   Checks if the location of the type M path table is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setlocationoftypempathtable();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getLocationOfOptionalTypeMPathTable method
     * @note   Checks if the location of the optional type M path table is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getlocationofoptionaltypempathtable();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setLocationOfOptionalTypeMPathTable method
     * @note   Checks if the location of the optional type M path table is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setlocationofoptionaltypempathtable();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getRootDirectoryEntry method
     * @note   Checks if the root directory entry is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getrootdirectoryentry();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setRootDirectoryEntry method
     * @note   Checks if the root directory entry is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setrootdirectoryentry();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getVolumeSetIdentifier method
     * @note   Checks if the volume set identifier is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getvolumesetidentifier();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setVolumeSetIdentifier method
     * @note   Checks if an exception is thrown when a volume set identifier is an incorrect size
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setvolumesetidentifier_1();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setVolumeSetIdentifier method
     * @note   Checks if the volume set identifier is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setvolumesetidentifier_2();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getPublisherIdentifier method
     * @note   Checks if the publisher identifier is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getpublisheridentifier();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setPublisherIdentifier method
     * @note   Checks if an exception is thrown when a publisher identifier is an incorrect size
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setpublisheridentifier_1();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setPublisherIdentifier method
     * @note   Checks if the publisher identifier is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setpublisheridentifier_2();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getDataPreparerIdentifier method
     * @note   Checks if the data preparer identifier is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getdataprepareridentifier();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setDataPreparerIdentifier method
     * @note   Checks if an exception is thrown when a data preparer identifier is an incorrect size
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setdataprepareridentifier_1();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setDataPreparerIdentifier method
     * @note   Checks if the data preparer identifier is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setdataprepareridentifier_2();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getApplicationIdentifier method
     * @note   Checks if the application identifier is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getapplicationidentifier();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setApplicationIdentifier method
     * @note   Checks if an exception is thrown when an application identifier is an incorrect size
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setapplicationidentifier_1();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setApplicationIdentifier method
     * @note   Checks if an application identifier is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setapplicationidentifier_2();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getCopyrightFileIdentifier method
     * @note   Checks if the copyright file identifier is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getcopyrightfileidentifier();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setCopyrightFileIdentifier method
     * @note   Checks if an exception is thrown when a copyright file identifier is an incorrect size
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setcopyrightfileidentifier_1();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setCopyrightFileIdentifier method
     * @note   Checks if a copyright file identifier is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setcopyrightfileidentifier_2();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getAbstractFileIdentifier method
     * @note   Checks if the abstract file identifier is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getabstractfileidentifier();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getAbstractFileIdentifier method
     * @note   Checks if an exception is thrown when an abstract file identifier is an incorrect size
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setabstractfileidentifier_1();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getAbstractFileIdentifier method
     * @note   Checks if an abstract file identifier is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setabstractfileidentifier_2();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getBiblographicFileIdentifier method
     * @note   Checks if the biblographic file identifier is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getbiblographicfileidentifier();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setBiblographicFileIdentifier method
     * @note   Checks if an exception is thrown when a biblographic file identifier is an incorrect size
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setbiblographicfileidentifier_1();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setBiblographicFileIdentifier method
     * @note   Checks if a biblographic file identifier is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setbiblographicfileidentifier_2();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getVolumeCreationDateAndTime method
     * @note   Checks if the volume creation date and time is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getvolumecreationdateandtime();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setVolumeCreationDateAndTime method
     * @note   Checks if the volume creation date and time is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setvolumecreationdateandtime();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getVolumeModificationDateAndTime method
     * @note   Checks if the volume modification date and time is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getvolumemodificationdateandtime();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setVolumeModificationDateAndTime method
     * @note   Checks if the volume modification date and time is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setvolumemodificationdateandtime();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getVolumeExpirationDateAndTime method
     * @note   Checks if the volume expiration date and time is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getvolumeexpirationdateandtime();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setVolumeExpirationDateAndTime method
     * @note   Checks if the volume expiration date and time is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setvolumeexpirationdateandtime();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getVolumeEffectiveDateAndTime method
     * @note   Checks if the volume effective date and time is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getvolumeeffectivedateandtime();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setVolumeEffectiveDateAndTime method
     * @note   Checks if the volume effective date and time is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setvolumeeffectivedateandtime();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getFileStructureVersion method
     * @note   Checks if the file structure version is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getfilestructureversion();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setFileStructureVersion method
     * @note   Checks if the file structure version is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setfilestructureversion();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getApplicationUsed method
     * @note   Checks if the application used area is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getapplicationused();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setApplicationUsed method
     * @note   Checks if the application used area is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setapplicationused();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's getReserved method
     * @note   Checks if the reserved area is retrieved correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_getreserved();

    /**
     * @brief  A test for PrimaryVolumeDescriptor's setReserved method
     * @note   Checks if the reserved area is set correctly
     * @retval true if the test passed
     */
    bool test_primaryvolumedescriptor_setreserved();

    /**
     * @brief  This returns a vector of tests to run
     * @retval a vector of tests to run
     */
    TEST_VECTOR_T getTests();
}

/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"
#include "testVolumeDescriptor.hpp"
#include "../include/VolumeDescriptor.hpp"
#include "../include/definitions.hpp"
#include "../include/DecDatetime.hpp"

bool testVolumeDescriptor::test_volumedescriptor_setaddress_getaddress()
{
    isochop::VolumeDescriptor vd;
    unsigned long addr = 123;
    vd.setAddress(addr);
    return addr == vd.getAddress();
}

bool testVolumeDescriptor::test_volumedescriptor_setdata_getdata()
{
    std::vector<char> data;
    data.reserve(ISOCHOP_VD_SIZE_TOTAL);
    for (int i = 0; i < ISOCHOP_VD_SIZE_TOTAL; i++)
    {
        data.push_back((i % 255) + 1);
    }
    isochop::VolumeDescriptor vd;
    vd.setData(data);
    std::vector<char> copy = vd.getData();
    for (int i = 0; i < ISOCHOP_VD_SIZE_TOTAL; i++)
    {
        if (data[i] != copy[i])
        {
            return false;
        }
    }
    return true;
}

bool testVolumeDescriptor::test_volumedescriptor_gettype()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    char type = 76;
    data[0] = type;
    isochop::VolumeDescriptor vd;
    vd.setData(data);
    char checkMe = vd.getType();
    return checkMe == type;
}

bool testVolumeDescriptor::test_volumedescriptor_settype()
{
    char type = 76;
    isochop::VolumeDescriptor vd;
    vd.setType(type);
    std::vector<char> data = vd.getData();
    return data[0] == type;
}

bool testVolumeDescriptor::test_volumedescriptor_getidentifier()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[1] = 'C';
    data[2] = 'A';
    data[3] = 'M';
    data[4] = 'E';
    data[5] = 'R';
    isochop::VolumeDescriptor vd;
    vd.setData(data);
    std::string checkMe = vd.getIdentifier();
    return checkMe == "CAMER";
}

bool testVolumeDescriptor::test_volumedescriptor_setidentifier()
{
    std::string id = "CAMER";
    isochop::VolumeDescriptor vd;
    vd.setIdentifier(id);
    std::vector<char> checkMe = vd.getData();
    bool isCorrect = true;
    isCorrect &= checkMe[1] == 'C';
    isCorrect &= checkMe[2] == 'A';
    isCorrect &= checkMe[3] == 'M';
    isCorrect &= checkMe[4] == 'E';
    isCorrect &= checkMe[5] == 'R';
    return isCorrect;
}

bool testVolumeDescriptor::test_volumedescriptor_getversion()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    char version = 87;
    data[6] = version;
    isochop::VolumeDescriptor vd;
    vd.setData(data);
    char checkMe = vd.getVersion();
    return checkMe == version;
}

bool testVolumeDescriptor::test_volumedescriptor_setversion()
{
    char version = 87;
    isochop::VolumeDescriptor vd;
    vd.setVersion(version);
    std::vector<char> raw = vd.getData();
    return raw[6] == version;
}

bool testVolumeDescriptor::test_volumedescriptor_isvalid_1()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    isochop::VolumeDescriptor vd;
    vd.setData(data);
    return !vd.isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_isvalid_2()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[0] = (unsigned char)255;
    isochop::VolumeDescriptor vd;
    vd.setData(data);
    return !vd.isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_isvalid_3()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    isochop::VolumeDescriptor vd;
    vd.setData(data);
    return !vd.isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_isvalid_4()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[6] = 1;
    isochop::VolumeDescriptor vd;
    vd.setData(data);
    return !vd.isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_isvalid_5()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[0] = (unsigned char)255;
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    data[6] = 1;
    isochop::VolumeDescriptor vd;
    vd.setData(data);
    return vd.isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_isvalid_6()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[0] = (unsigned char)255;
    data[1] = 'C';
    data[2] = 'A';
    data[3] = 'M';
    data[4] = 'E';
    data[5] = 'R';
    data[6] = 1;
    isochop::VolumeDescriptor vd;
    vd.setData(data);
    return !vd.isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_isvalid_7()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[0] = (unsigned char)255;
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    data[6] = 69;
    isochop::VolumeDescriptor vd;
    vd.setData(data);
    return !vd.isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_clone()
{
    std::vector<char> raw(ISOCHOP_VD_SIZE_TOTAL, 0);
    for (int i = 0; i < ISOCHOP_VD_SIZE_TOTAL; i++)
    {
        raw[i] = (i % 255) + 1;
    }
    isochop::VolumeDescriptor *vd1 = new isochop::VolumeDescriptor();
    vd1->setData(raw);
    isochop::VolumeDescriptor *vd2 = vd1->clone();
    std::vector<char> checkData = vd2->getData();
    for (int i = 0; i < ISOCHOP_VD_SIZE_TOTAL; i++)
    {
        if (checkData[i] != raw[i])
        {
            return false;
        }
    }
    bool fieldsMatch = true;
    fieldsMatch &= vd1->getType() == vd2->getType();
    fieldsMatch &= vd1->getIdentifier() == vd2->getIdentifier();
    fieldsMatch &= vd1->getVersion() == vd2->getVersion();
    return fieldsMatch;
}

bool testVolumeDescriptor::test_volumedescriptor_constructor_1()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    return vd->isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_constructor_2()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor(data, 123);
    return vd->getAddress() == 123;
}

bool testVolumeDescriptor::test_volumedescriptor_constructor_3()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor(data, 0);
    return !vd->isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_constructor_4()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[0] = (unsigned char)255;
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor(data, 0);
    return !vd->isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_constructor_5()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor(data, 0);
    return !vd->isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_constructor_6()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[6] = 1;
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor(data, 0);
    return !vd->isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_constructor_7()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[0] = (unsigned char)255;
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    data[6] = 1;
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor(data, 0);
    return vd->isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_constructor_8()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[0] = (unsigned char)255;
    data[1] = 'C';
    data[2] = 'A';
    data[3] = 'M';
    data[4] = 'E';
    data[5] = 'R';
    data[6] = 1;
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor(data, 0);
    return !vd->isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_constructor_9()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[0] = (unsigned char)255;
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    data[6] = 69;
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor(data, 0);
    return !vd->isValid();
}

bool testVolumeDescriptor::test_volumedescriptor_markaddresses_1()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 1);
    isochop::VolumeDescriptor vd = isochop::VolumeDescriptor(data, 5);
    try
    {
        vd.markAddresses(nullptr);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_markaddresses_2()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 1);
    std::vector<bool> bits(ISOCHOP_VD_SIZE_TOTAL + 100, 0);
    isochop::VolumeDescriptor vd = isochop::VolumeDescriptor(data, 5);
    vd.markAddresses(&bits);
    for (int i = 0; i < 5; i++)
    {
        if (bits.at(i))
        {
            return false;
        }
    }
    for (int i = 5; i < ISOCHOP_VD_SIZE_TOTAL; i++)
    {
        if (!bits.at(i))
        {
            return false;
        }
    }
    for (int i = 5 + ISOCHOP_VD_SIZE_TOTAL; i < ISOCHOP_VD_SIZE_TOTAL + 100; i++)
    {
        if (bits.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testVolumeDescriptor::test_volumedescriptor_getsystemidentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getSystemIdentifier();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setsystemidentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setSystemIdentifier("hi");
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getvolumeidentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getVolumeIdentifier();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setvolumeidentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setVolumeIdentifier("hi");
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getvolumespacesize()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getVolumeSpaceSize();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setvolumespacesize()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setVolumeSpaceSize(24);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getvolumesetsize()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getVolumeSetSize();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setvolumesetsize()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setVolumeSetSize(24);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getvolumesequencenumber()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getVolumeSequenceNumber();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setvolumesequencenumber()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setVolumeSequenceNumber(24);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getlogicalblocksize()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getLogicalBlockSize();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setlogicalblocksize()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setLogicalBlockSize(24);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getpathtablesize()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getPathTableSize();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setpathtablesize()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setPathTableSize(24);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getlocationoftypelpathtable()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getLocationOfTypeLPathTable();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setlocationoftypelpathtable()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setLocationOfTypeLPathTable(24);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getlocationofoptionaltypelpathtable()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getLocationOfOptionalTypeLPathTable();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setlocationofoptionaltypelpathtable()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setLocationOfOptionalTypeLPathTable(24);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getlocationoftypempathtable()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getLocationOfTypeMPathTable();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setlocationoftypempathtable()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setLocationOfTypeMPathTable(24);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getlocationofoptionaltypempathtable()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getLocationOfOptionalTypeMPathTable();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getrootdirectoryentry()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getRootDirectoryEntry();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setrootdirectoryentry()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    isochop::Directory d = isochop::Directory();
    try
    {
        vd->setRootDirectoryEntry(d);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setlocationofoptionaltypempathtable()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setLocationOfOptionalTypeMPathTable(24);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getvolumesetidentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getVolumeSetIdentifier();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setvolumesetidentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setVolumeSetIdentifier("hi");
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getpublisheridentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getPublisherIdentifier();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setpublisheridentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setPublisherIdentifier("hi");
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getdataprepareridentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getDataPreparerIdentifier();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setdataprepareridentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setDataPreparerIdentifier("hi");
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getapplicationidentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getApplicationIdentifier();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setapplicationidentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setApplicationIdentifier("hi");
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getcopyrightfileidentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getCopyrightFileIdentifier();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setcopyrightfileidentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setCopyrightFileIdentifier("hi");
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getabstractfileidentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getAbstractFileIdentifier();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setabstractfileidentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setAbstractFileIdentifier("hi");
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getbiblographicfileidentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getBiblographicFileIdentifier();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setbiblographicfileidentifier()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setBiblographicFileIdentifier("hi");
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getvolumecreationdateandtime()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getVolumeCreationDateAndTime();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setvolumecreationdateandtime()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    isochop::DecDatetime dd = isochop::DecDatetime();
    try
    {
        vd->setVolumeCreationDateAndTime(dd);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getvolumemodificationdateandtime()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getVolumeModificationDateAndTime();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setvolumemodificationdateandtime()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    isochop::DecDatetime dd = isochop::DecDatetime();
    try
    {
        vd->setVolumeModificationDateAndTime(dd);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getvolumeexpirationdateandtime()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getVolumeExpirationDateAndTime();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setvolumeexpirationdateandtime()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    isochop::DecDatetime dd = isochop::DecDatetime();
    try
    {
        vd->setVolumeExpirationDateAndTime(dd);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getvolumeeffectivedateandtime()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getVolumeEffectiveDateAndTime();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setvolumeeffectivedateandtime()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    isochop::DecDatetime dd = isochop::DecDatetime();
    try
    {
        vd->setVolumeEffectiveDateAndTime(dd);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getfilestructureversion()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getFileStructureVersion();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setfilestructureversion()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->setFileStructureVersion(4);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getapplicationused()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getApplicationUsed();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setapplicationused()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    std::vector<char> foo;
    try
    {
        vd->setApplicationUsed(foo);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_getreserved()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    try
    {
        vd->getReserved();
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

bool testVolumeDescriptor::test_volumedescriptor_setreserved()
{
    isochop::VolumeDescriptor *vd = new isochop::VolumeDescriptor();
    std::vector<char> foo;
    try
    {
        vd->setReserved(foo);
    }
    catch (const std::runtime_error &ia)
    {
        return true;
    }
    return false;
}

TEST_VECTOR_T testVolumeDescriptor::getTests()
{
    TEST_VECTOR_T tests;
    pushTest(&tests, test_volumedescriptor_setaddress_getaddress, "VolumeDescriptor.cpp: setAddress/getAddress (symmetry)");
    pushTest(&tests, test_volumedescriptor_setdata_getdata, "VolumeDescriptor.cpp: setData/getData (symmetry)");
    pushTest(&tests, test_volumedescriptor_gettype, "VolumeDescriptor.cpp: getType");
    pushTest(&tests, test_volumedescriptor_settype, "VolumeDescriptor.cpp: setType");
    pushTest(&tests, test_volumedescriptor_getidentifier, "VolumeDescriptor.cpp: getIdentifier");
    pushTest(&tests, test_volumedescriptor_setidentifier, "VolumeDescriptor.cpp: setIdentifier");
    pushTest(&tests, test_volumedescriptor_getversion, "VolumeDescriptor.cpp: getVersion");
    pushTest(&tests, test_volumedescriptor_setversion, "VolumeDescriptor.cpp: setVersion");
    pushTest(&tests, test_volumedescriptor_isvalid_1, "VolumeDescriptor.cpp: isValid (empty)");
    pushTest(&tests, test_volumedescriptor_isvalid_2, "VolumeDescriptor.cpp: isValid (only type)");
    pushTest(&tests, test_volumedescriptor_isvalid_3, "VolumeDescriptor.cpp: isValid (only identifier)");
    pushTest(&tests, test_volumedescriptor_isvalid_4, "VolumeDescriptor.cpp: isValid (only version)");
    pushTest(&tests, test_volumedescriptor_isvalid_5, "VolumeDescriptor.cpp: isValid (legal header)");
    pushTest(&tests, test_volumedescriptor_isvalid_6, "VolumeDescriptor.cpp: isValid (illegal identifier)");
    pushTest(&tests, test_volumedescriptor_isvalid_7, "VolumeDescriptor.cpp: isValid (illegal version)");
    pushTest(&tests, test_volumedescriptor_clone, "VolumeDescriptor.cpp: clone");
    pushTest(&tests, test_volumedescriptor_constructor_1, "VolumeDescriptor.cpp: VolumeDescriptor (no-args)");
    pushTest(&tests, test_volumedescriptor_constructor_2, "VolumeDescriptor.cpp: VolumeDescriptor (address)");
    pushTest(&tests, test_volumedescriptor_constructor_3, "VolumeDescriptor.cpp: VolumeDescriptor (empty)");
    pushTest(&tests, test_volumedescriptor_constructor_4, "VolumeDescriptor.cpp: VolumeDescriptor (only type)");
    pushTest(&tests, test_volumedescriptor_constructor_5, "VolumeDescriptor.cpp: VolumeDescriptor (only identifier)");
    pushTest(&tests, test_volumedescriptor_constructor_6, "VolumeDescriptor.cpp: VolumeDescriptor (only version)");
    pushTest(&tests, test_volumedescriptor_constructor_7, "VolumeDescriptor.cpp: VolumeDescriptor (legal header)");
    pushTest(&tests, test_volumedescriptor_constructor_8, "VolumeDescriptor.cpp: VolumeDescriptor (illegal identifier)");
    pushTest(&tests, test_volumedescriptor_constructor_9, "VolumeDescriptor.cpp: VolumeDescriptor (illegal version)");
    pushTest(&tests, test_volumedescriptor_markaddresses_1, "VolumeDescriptor.cpp: markAddresses (nullptr)");
    pushTest(&tests, test_volumedescriptor_markaddresses_2, "VolumeDescriptor.cpp: markAddresses (data)");
    pushTest(&tests, test_volumedescriptor_getsystemidentifier, "VolumeDescriptor.cpp: getSystemIdentifier");
    pushTest(&tests, test_volumedescriptor_setsystemidentifier, "VolumeDescriptor.cpp: setSystemIdentifier");
    pushTest(&tests, test_volumedescriptor_getvolumeidentifier, "VolumeDescriptor.cpp: getVolumeIdentifier");
    pushTest(&tests, test_volumedescriptor_setvolumeidentifier, "VolumeDescriptor.cpp: setVolumeIdentifier");
    pushTest(&tests, test_volumedescriptor_getvolumespacesize, "VolumeDescriptor.cpp: getVolumeSpaceSize");
    pushTest(&tests, test_volumedescriptor_setvolumespacesize, "VolumeDescriptor.cpp: setVolumeSpaceSize");
    pushTest(&tests, test_volumedescriptor_getvolumesetsize, "VolumeDescriptor.cpp: getVolumeSetSize");
    pushTest(&tests, test_volumedescriptor_setvolumesetsize, "VolumeDescriptor.cpp: setVolumeSetSize");
    pushTest(&tests, test_volumedescriptor_getvolumesequencenumber, "VolumeDescriptor.cpp: getVolumeSequenceNumber");
    pushTest(&tests, test_volumedescriptor_setvolumesequencenumber, "VolumeDescriptor.cpp: setVolumeSequenceNumber");
    pushTest(&tests, test_volumedescriptor_getlogicalblocksize, "VolumeDescriptor.cpp: getLogicalBlockSize");
    pushTest(&tests, test_volumedescriptor_setlogicalblocksize, "VolumeDescriptor.cpp: setLogicalBlockSize");
    pushTest(&tests, test_volumedescriptor_getpathtablesize, "VolumeDescriptor.cpp: getPathTableSize");
    pushTest(&tests, test_volumedescriptor_setpathtablesize, "VolumeDescriptor.cpp: setPathTableSize");
    pushTest(&tests, test_volumedescriptor_getlocationoftypelpathtable, "VolumeDescriptor.cpp: getLocationOfTypeLPathTable");
    pushTest(&tests, test_volumedescriptor_setlocationoftypelpathtable, "VolumeDescriptor.cpp: setLocationOfTypeLPathTable");
    pushTest(&tests, test_volumedescriptor_getlocationofoptionaltypelpathtable, "VolumeDescriptor.cpp: getLocationOfOptionalTypeLPathTable");
    pushTest(&tests, test_volumedescriptor_setlocationofoptionaltypelpathtable, "VolumeDescriptor.cpp: setLocationOfOptionalTypeLPathTable");
    pushTest(&tests, test_volumedescriptor_getlocationoftypempathtable, "VolumeDescriptor.cpp: getLocationOfTypeMPathTable");
    pushTest(&tests, test_volumedescriptor_setlocationoftypempathtable, "VolumeDescriptor.cpp: setLocationOfTypeMPathTable");
    pushTest(&tests, test_volumedescriptor_getlocationofoptionaltypempathtable, "VolumeDescriptor.cpp: getLocationOfOptionalTypeMPathTable");
    pushTest(&tests, test_volumedescriptor_setlocationofoptionaltypempathtable, "VolumeDescriptor.cpp: setLocationOfOptionalTypeMPathTable");
    pushTest(&tests, test_volumedescriptor_getrootdirectoryentry, "VolumeDescriptor.cpp: getRootDirectoryEntry");
    pushTest(&tests, test_volumedescriptor_setrootdirectoryentry, "VolumeDescriptor.cpp: setRootDirectoryEntry");
    pushTest(&tests, test_volumedescriptor_getvolumesetidentifier, "VolumeDescriptor.cpp: getVolumeSetIdentifier");
    pushTest(&tests, test_volumedescriptor_setvolumesetidentifier, "VolumeDescriptor.cpp: setVolumeSetIdentifier");
    pushTest(&tests, test_volumedescriptor_getpublisheridentifier, "VolumeDescriptor.cpp: getPublisherIdentifier");
    pushTest(&tests, test_volumedescriptor_setpublisheridentifier, "VolumeDescriptor.cpp: setPublisherIdentifier");
    pushTest(&tests, test_volumedescriptor_getdataprepareridentifier, "VolumeDescriptor.cpp: getDataPreparerIdentifier");
    pushTest(&tests, test_volumedescriptor_setdataprepareridentifier, "VolumeDescriptor.cpp: setDataPreparerIdentifier");
    pushTest(&tests, test_volumedescriptor_getapplicationidentifier, "VolumeDescriptor.cpp: getApplicationIdentifier");
    pushTest(&tests, test_volumedescriptor_setapplicationidentifier, "VolumeDescriptor.cpp: setApplicationIdentifier");
    pushTest(&tests, test_volumedescriptor_getcopyrightfileidentifier, "VolumeDescriptor.cpp: getCopyrightFileIdentifier");
    pushTest(&tests, test_volumedescriptor_setcopyrightfileidentifier, "VolumeDescriptor.cpp: setCopyrightFileIdentifier");
    pushTest(&tests, test_volumedescriptor_getabstractfileidentifier, "VolumeDescriptor.cpp: getAbstractFileIdentifier");
    pushTest(&tests, test_volumedescriptor_setabstractfileidentifier, "VolumeDescriptor.cpp: setAbstractFileIdentifier");
    pushTest(&tests, test_volumedescriptor_getbiblographicfileidentifier, "VolumeDescriptor.cpp: getBiblographicFileIdentifier");
    pushTest(&tests, test_volumedescriptor_setbiblographicfileidentifier, "VolumeDescriptor.cpp: setBiblographicFileIdentifier");
    pushTest(&tests, test_volumedescriptor_getvolumecreationdateandtime, "VolumeDescriptor.cpp: getVolumeCreationDateAndTime");
    pushTest(&tests, test_volumedescriptor_setvolumecreationdateandtime, "VolumeDescriptor.cpp: setVolumeCreationDateAndTime");
    pushTest(&tests, test_volumedescriptor_getvolumemodificationdateandtime, "VolumeDescriptor.cpp: getVolumeModificationDateAndTime");
    pushTest(&tests, test_volumedescriptor_setvolumemodificationdateandtime, "VolumeDescriptor.cpp: setVolumeModificationDateAndTime");
    pushTest(&tests, test_volumedescriptor_getvolumeexpirationdateandtime, "VolumeDescriptor.cpp: getVolumeExpirationDateAndTime");
    pushTest(&tests, test_volumedescriptor_setvolumeexpirationdateandtime, "VolumeDescriptor.cpp: setVolumeExpirationDateAndTime");
    pushTest(&tests, test_volumedescriptor_getvolumeeffectivedateandtime, "VolumeDescriptor.cpp: getVolumeEffectiveDateAndTime");
    pushTest(&tests, test_volumedescriptor_setvolumeeffectivedateandtime, "VolumeDescriptor.cpp: setVolumeEffectiveDateAndTime");
    pushTest(&tests, test_volumedescriptor_getfilestructureversion, "VolumeDescriptor.cpp: getFileStructureVersion");
    pushTest(&tests, test_volumedescriptor_setfilestructureversion, "VolumeDescriptor.cpp: setFileStructureVersion");
    pushTest(&tests, test_volumedescriptor_getapplicationused, "VolumeDescriptor.cpp: getApplicationUsed");
    pushTest(&tests, test_volumedescriptor_setapplicationused, "VolumeDescriptor.cpp: setApplicationUsed");
    pushTest(&tests, test_volumedescriptor_getreserved, "VolumeDescriptor.cpp: getReserved");
    pushTest(&tests, test_volumedescriptor_setreserved, "VolumeDescriptor.cpp: setReserved");
    return tests;
}

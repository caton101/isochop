/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"
#include "testDirectory.hpp"
#include "../include/Directory.hpp"
#include "../include/FileFlags.hpp"
#include "../include/DirDatetime.hpp"
#include "../include/definitions.hpp"
#include "../include/utils.hpp"

bool testDirectory::test_directory_setdata_getdata()
{
    std::vector<char> ans;
    for (int i = 0; i < 255; i++)
    {
        ans.push_back(i);
    }
    ans.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH) = (unsigned char)255;
    isochop::Directory d;
    d.setData(ans);
    std::vector out = d.getData();
    for (int i = 0; i < 255; i++)
    {
        if (ans.at(i) != out.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testDirectory::test_directory_getdirectoryrecordlength()
{
    std::vector<char> data(255, 0);
    char canary = 48;
    data.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH) = canary;
    isochop::Directory d;
    d.setData(data);
    return d.getDirectoryRecordLength() == canary;
}

bool testDirectory::test_directory_setdirectoryrecordlength()
{
    char canary = 48;
    isochop::Directory d;
    d.setDirectoryRecordLength(canary);
    std::vector<char> out = d.getData();
    return out.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH) == canary;
}

bool testDirectory::test_directory_getextendedattributerecordlength()
{
    std::vector<char> data(255, 0);
    char canary = 48;
    data.at(ISOCHOP_DIRECTORY_OFFSET_EXTENDED_ATTRIBUTE_RECORD_LENGTH) = canary;
    isochop::Directory d;
    d.setData(data);
    return d.getExtendedAttributeRecordLength() == canary;
}

bool testDirectory::test_directory_setextendedattributerecordlength()
{
    char canary = 48;
    isochop::Directory d;
    d.setExtendedAttributeRecordLength(canary);
    std::vector<char> out = d.getData();
    return out.at(ISOCHOP_DIRECTORY_OFFSET_EXTENDED_ATTRIBUTE_RECORD_LENGTH) == canary;
}

bool testDirectory::test_directory_getlocationofextent()
{
    std::vector<char> data(255, 0);
    data.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION) = 0x21;
    data.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION + 1) = 0x43;
    data.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION + 2) = 0x65;
    data.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION + 3) = 0x87;
    data.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION + 4) = 0x87;
    data.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION + 5) = 0x65;
    data.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION + 6) = 0x43;
    data.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION + 7) = 0x21;
    isochop::Directory d;
    d.setData(data);
    return d.getLocationOfExtent() == 0x87654321;
}

bool testDirectory::test_directory_setlocationofextent()
{
    isochop::Directory d;
    d.setLocationOfExtent(0x87654321);
    std::vector<char> out = d.getData();
    bool valid = true;
    valid &= out.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION) == 0x21;
    valid &= out.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION + 1) == 0x43;
    valid &= out.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION + 2) == 0x65;
    valid &= (unsigned char)out.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION + 3) == 0x87;
    valid &= (unsigned char)out.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION + 4) == 0x87;
    valid &= out.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION + 5) == 0x65;
    valid &= out.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION + 6) == 0x43;
    valid &= out.at(ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION + 7) == 0x21;
    return valid;
}

bool testDirectory::test_directory_getdatalength()
{
    std::vector<char> data(255, 0);
    data.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH) = 0x21;
    data.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH + 1) = 0x43;
    data.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH + 2) = 0x65;
    data.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH + 3) = 0x87;
    data.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH + 4) = 0x87;
    data.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH + 5) = 0x65;
    data.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH + 6) = 0x43;
    data.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH + 7) = 0x21;
    isochop::Directory d;
    d.setData(data);
    return d.getDataLength() == 0x87654321;
}

bool testDirectory::test_directory_setdatalength()
{
    isochop::Directory d;
    d.setDataLength(0x87654321);
    std::vector<char> out = d.getData();
    bool valid = true;
    valid &= out.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH) == 0x21;
    valid &= out.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH + 1) == 0x43;
    valid &= out.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH + 2) == 0x65;
    valid &= (unsigned char)out.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH + 3) == 0x87;
    valid &= (unsigned char)out.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH + 4) == 0x87;
    valid &= out.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH + 5) == 0x65;
    valid &= out.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH + 6) == 0x43;
    valid &= out.at(ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH + 7) == 0x21;
    return valid;
}

bool testDirectory::test_directory_getrecordingdateandtime()
{
    std::vector<char> data(255, 0);
    std::vector<char> ans = {1, 2, 3, 4, 5, 6, 7};
    isochop::utils::copyVector(&ans, &data, 0, ISOCHOP_DIRECTORY_OFFSET_RECORDING_DATE_AND_TIME, ISOCHOP_DIRECTORY_SIZE_RECORDING_DATE_AND_TIME);
    isochop::Directory d;
    d.setData(data);
    std::vector<char> out = d.getRecordingDateAndTime().getData();
    for (int i = 0; i < ISOCHOP_DIRECTORY_SIZE_RECORDING_DATE_AND_TIME; i++)
    {
        if (ans.at(i) != out.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testDirectory::test_directory_setrecordingdateandtime()
{
    std::vector<char> ans = {1, 2, 3, 4, 5, 6, 7};
    isochop::DirDatetime dd = isochop::DirDatetime(ans);
    isochop::Directory d;
    d.setRecordingDateAndTime(dd);
    std::vector<char> out = d.getData();
    for (int i = 0; i < ISOCHOP_DIRECTORY_SIZE_RECORDING_DATE_AND_TIME; i++)
    {
        if (ans.at(i) != out.at(ISOCHOP_DIRECTORY_OFFSET_RECORDING_DATE_AND_TIME + i))
        {
            return false;
        }
    }
    return true;
}

bool testDirectory::test_directory_getfileflags()
{
    std::vector<char> data(255, 0);
    uint8_t canary = 48;
    data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_FLAGS) = canary;
    isochop::Directory d;
    d.setData(data);
    return d.getFileFlags().getData() == canary;
}

bool testDirectory::test_directory_setfileflags()
{
    uint8_t canary = 48;
    isochop::Directory d;
    isochop::FileFlags ff = isochop::FileFlags(canary);
    d.setFileFlags(ff);
    std::vector<char> out = d.getData();
    return out.at(ISOCHOP_DIRECTORY_OFFSET_FILE_FLAGS) == canary;
}

bool testDirectory::test_directory_getfileunitsize()
{
    std::vector<char> data(255, 0);
    char canary = 48;
    data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_UNIT_SIZE) = canary;
    isochop::Directory d;
    d.setData(data);
    return d.getFileUnitSize() == canary;
}

bool testDirectory::test_directory_setfileunitsize()
{
    char canary = 48;
    isochop::Directory d;
    d.setFileUnitSize(canary);
    std::vector<char> out = d.getData();
    return out.at(ISOCHOP_DIRECTORY_OFFSET_FILE_UNIT_SIZE) == canary;
}

bool testDirectory::test_directory_getinterleavegapsize()
{
    std::vector<char> data(255, 0);
    char canary = 48;
    data.at(ISOCHOP_DIRECTORY_OFFSET_INTERLEAVE_GAP_SIZE) = canary;
    isochop::Directory d;
    d.setData(data);
    return d.getInterleaveGapSize() == canary;
}

bool testDirectory::test_directory_setinterleavegapsize()
{
    char canary = 48;
    isochop::Directory d;
    d.setInterleaveGapSize(canary);
    std::vector<char> out = d.getData();
    return out.at(ISOCHOP_DIRECTORY_OFFSET_INTERLEAVE_GAP_SIZE) == canary;
}

bool testDirectory::test_directory_getvolumesequencenumber()
{
    std::vector<char> data(255, 0);
    data.at(ISOCHOP_DIRECTORY_OFFSET_VOLUME_SEQUENCE_NUMBER) = 0x65;
    data.at(ISOCHOP_DIRECTORY_OFFSET_VOLUME_SEQUENCE_NUMBER + 1) = 0x87;
    data.at(ISOCHOP_DIRECTORY_OFFSET_VOLUME_SEQUENCE_NUMBER + 2) = 0x87;
    data.at(ISOCHOP_DIRECTORY_OFFSET_VOLUME_SEQUENCE_NUMBER + 3) = 0x65;
    isochop::Directory d;
    d.setData(data);
    return d.getVolumeSequenceNumber() == 0x8765;
}

bool testDirectory::test_directory_setvolumesequencenumber()
{
    isochop::Directory d;
    d.setVolumeSequenceNumber(0x8765);
    std::vector<char> out = d.getData();
    bool valid = true;
    valid &= out.at(ISOCHOP_DIRECTORY_OFFSET_VOLUME_SEQUENCE_NUMBER) == 0x65;
    valid &= (unsigned char)out.at(ISOCHOP_DIRECTORY_OFFSET_VOLUME_SEQUENCE_NUMBER + 1) == 0x87;
    valid &= (unsigned char)out.at(ISOCHOP_DIRECTORY_OFFSET_VOLUME_SEQUENCE_NUMBER + 2) == 0x87;
    valid &= out.at(ISOCHOP_DIRECTORY_OFFSET_VOLUME_SEQUENCE_NUMBER + 3) == 0x65;
    return valid;
}

bool testDirectory::test_directory_getfileidentifierlength()
{
    std::vector<char> data(255, 0);
    char canary = 48;
    data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER_LENGTH) = canary;
    isochop::Directory d;
    d.setData(data);
    return d.getFileIdentifierLength() == canary;
}

bool testDirectory::test_directory_setfileidentifierlength()
{
    char canary = 48;
    isochop::Directory d;
    d.setFileIdentifierLength(canary);
    std::vector<char> out = d.getData();
    return out.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER_LENGTH) == canary;
}

bool testDirectory::test_directory_getfileidentifier()
{
    std::vector<char> data(255, 0);
    std::string canary = "hello there";
    uint8_t len = canary.size();
    data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER_LENGTH) = len;
    for (int i = 0; i < len; i++)
    {
        data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER + i) = canary.at(i);
    }
    isochop::Directory d;
    d.setData(data);
    return d.getFileIdentifier() == canary;
}

bool testDirectory::test_directory_setfileidentifier()
{
    std::string canary = "hello there";
    isochop::Directory d;
    d.setFileIdentifier(canary);
    std::vector<char> out = d.getData();
    uint8_t len = canary.size();
    if (out.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER_LENGTH) != len)
    {
        return false;
    }
    for (int i = 0; i < len; i++)
    {
        if (out.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER + i) != canary.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testDirectory::test_directory_getsystemuse()
{
    std::vector<char> ans;
    for (int i = 0; i < 255; i++)
    {
        ans.push_back(i);
    }
    ans.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH) = (unsigned char)255;
    ans.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER_LENGTH) = 1;
    // with these settings, addr is 35 and len is 220
    int addr = 35;
    int len = 220;
    isochop::Directory d;
    d.setData(ans);
    std::vector<char> out = d.getSystemUse();
    if (out.size() != (std::size_t)len)
    {
        return false;
    }
    for (int i = 0; i < len; i++)
    {
        if (ans.at(addr + i) != out.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testDirectory::test_directory_setsystemuse()
{
    // ensure addr is 35 and len is 220
    int addr = 35;
    int len = 220;
    std::vector<char> use;
    for (int i = 0; i < len; i++)
    {
        use.push_back(i);
    }
    std::vector<char> data(255, 0);
    data.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH) = (unsigned char)255;
    data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER_LENGTH) = 1;
    isochop::Directory d;
    d.setData(data);
    d.setSystemUse(use);
    std::vector<char> out = d.getData();
    if (out.size() != 255)
    {
        return false;
    }
    isochop::utils::copyVector(&use, &data, 0, addr, len);
    for (int i = 0; i < 255; i++)
    {
        if (out.at(i) != data.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testDirectory::test_directory_isvalid_1()
{
    std::vector<char> data(255, 0);
    isochop::Directory d;
    d.setData(data);
    return !d.isValid();
}

bool testDirectory::test_directory_isvalid_2()
{
    std::vector<char> data(255, 0);
    data.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH) = (unsigned char)255;
    isochop::Directory d;
    d.setData(data);
    return d.isValid();
}

bool testDirectory::test_directory_isvalid_3()
{
    std::vector<char> data(255, 0);
    data.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH) = 42;
    data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER) = 'c';
    data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER) = 'a';
    data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER) = 'm';
    data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER) = 'e';
    data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER) = 'r';
    data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER) = 'o';
    data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER) = 'n';
    data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER_LENGTH) = 7;
    isochop::Directory d;
    d.setData(data);
    return !d.isValid();
}

bool testDirectory::test_directory_contructor_1()
{
    isochop::Directory *d = nullptr;
    d = new isochop::Directory();
    return d != nullptr;
}

bool testDirectory::test_directory_constructor_2()
{
    std::vector<char> data(255, 0);
    data.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH) = (unsigned char)255;
    isochop::Directory *d = new isochop::Directory(data);
    return d->isValid();
}

bool testDirectory::test_directory_clone()
{
    std::vector<char> ans;
    for (int i = 0; i < 255; i++)
    {
        ans.push_back(i);
    }
    ans.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH) = (unsigned char)255;
    isochop::Directory *d = new isochop::Directory(ans);
    isochop::Directory dup = d->clone();
    std::vector out = dup.getData();
    for (int i = 0; i < 255; i++)
    {
        if (ans.at(i) != out.at(i))
        {
            return false;
        }
    }
    return true;
}

TEST_VECTOR_T testDirectory::getTests()
{
    TEST_VECTOR_T tests;
    pushTest(&tests, test_directory_setdata_getdata, "Directory.cpp: setData/getData");
    pushTest(&tests, test_directory_getdirectoryrecordlength, "Directory.cpp: getDirectoryRecordLength");
    pushTest(&tests, test_directory_setdirectoryrecordlength, "Directory.cpp: setDirectoryRecordLength");
    pushTest(&tests, test_directory_getextendedattributerecordlength, "Directory.cpp: getExtendedAttributeRecordLength");
    pushTest(&tests, test_directory_setextendedattributerecordlength, "Directory.cpp: setExtendedAttributeRecordLength");
    pushTest(&tests, test_directory_getlocationofextent, "Directory.cpp: getLocationOfExtent");
    pushTest(&tests, test_directory_setlocationofextent, "Directory.cpp: setLocationOfExtent");
    pushTest(&tests, test_directory_getdatalength, "Directory.cpp: getDataLength");
    pushTest(&tests, test_directory_setdatalength, "Directory.cpp: setDataLength");
    pushTest(&tests, test_directory_getrecordingdateandtime, "Directory.cpp: getRecordingDateAndTime");
    pushTest(&tests, test_directory_setrecordingdateandtime, "Directory.cpp: setRecordingDateAndTime");
    pushTest(&tests, test_directory_getfileflags, "Directory.cpp: getFileFlags");
    pushTest(&tests, test_directory_setfileflags, "Directory.cpp: setFileFlags");
    pushTest(&tests, test_directory_getfileunitsize, "Directory.cpp: getFileUnitSize");
    pushTest(&tests, test_directory_setfileunitsize, "Directory.cpp: setFileUnitSize");
    pushTest(&tests, test_directory_getinterleavegapsize, "Directory.cpp: getInterleaveGapSize");
    pushTest(&tests, test_directory_setinterleavegapsize, "Directory.cpp: setInterleaveGapSize");
    pushTest(&tests, test_directory_getvolumesequencenumber, "Directory.cpp: getVolumeSequenceNumber");
    pushTest(&tests, test_directory_setvolumesequencenumber, "Directory.cpp: setVolumeSequenceNumber");
    pushTest(&tests, test_directory_getfileidentifierlength, "Directory.cpp: getFileIdentifierLength");
    pushTest(&tests, test_directory_setfileidentifierlength, "Directory.cpp: setFileIdentifierLength");
    pushTest(&tests, test_directory_getfileidentifier, "Directory.cpp: getFileIdentifier");
    pushTest(&tests, test_directory_setfileidentifier, "Directory.cpp: setFileIdentifier");
    pushTest(&tests, test_directory_getsystemuse, "Directory.cpp: getSystemUse");
    pushTest(&tests, test_directory_setsystemuse, "Directory.cpp: setSystemUse");
    pushTest(&tests, test_directory_isvalid_1, "Directory.cpp: isValid (empty)");
    pushTest(&tests, test_directory_isvalid_2, "Directory.cpp: isValid (legal)");
    pushTest(&tests, test_directory_isvalid_3, "Directory.cpp: isValid (bad DRL)");
    pushTest(&tests, test_directory_contructor_1, "Directory.cpp: Directory (no-args)");
    pushTest(&tests, test_directory_constructor_2, "Directory.cpp: Directory (args)");
    pushTest(&tests, test_directory_clone, "Directory.cpp: clone");
    return tests;
}
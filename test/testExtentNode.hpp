/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"

/**
 * @brief  This tests all methods inside the ExtentNode class
 */
namespace testExtentNode
{
    /**
     * @brief  A test for ExtentNode's getAddress and setAddress methods
     * @note   Checks if the address can be fetched and set correctly
     * @retval true if the test passed
     */
    bool test_extentnode_getaddress_setaddress();

    /**
     * @brief  A test for ExtentNode's getExtent and setExtent methods
     * @note   Checks if the extent can be fetched and set correctly
     * @retval true if the test passed
     */
    bool test_extentnode_getextent_setextent();

    /**
     * @brief  A test for ExtentNode's getDirectoryEntries and setDirectoryEntries methods
     * @note   Checks if the directory entries can be fetched and set correctly
     * @retval true if the test passed
     */
    bool test_extentnode_getdirectoryentries_setdirectoryentries();

    /**
     * @brief  A test for ExtentNode's getChildren and setChildren methods
     * @note   Checks if the children can be fetched and set correctly
     * @retval true if the test passed
     */
    bool test_extentnode_getchildren_setchildren();

    /**
     * @brief  A test for ExtentNode's no-args constructor
     * @note   Checks if a ExtentNode object can be created correctly
     * @retval true if the test passed
     */
    bool test_extentnode_constructor_1();

    /**
     * @brief  A test for ExtentNode's normal constructor
     * @note   Checks if a ExtentNode object is initialized correctly
     * @retval true if the test passed
     */
    bool test_extentnode_constructor_2();

    /**
     * @brief  A test for ExtentNode's clone method
     * @note   Checks if a ExtentNode object is cloned correctly
     * @retval true if the test passed
     */
    bool test_extentnode_clone();

    /**
     * @brief  A test for ExtentNode's markAddresses method
     * @note   Checks for an exception when bits is null
     * @retval true if the test passed
     */
    bool test_extentnode_markaddresses_1();

    /**
     * @brief  A test for ExtentNode's markAddresses method
     * @note   Checks if a ExtentNode object marks its addresses correctly
     * @retval true if the test passed
     */
    bool test_extentnode_markaddresses_2();

    /**
     * @brief  This returns a vector of tests to run
     * @retval a vector of tests to run
     */
    TEST_VECTOR_T getTests();
}
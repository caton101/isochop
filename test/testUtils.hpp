/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"

/**
 * @brief  This tests all functions inside the util.cpp file
 */
namespace testUtils
{
    /**
     * @brief  A test for util's copyBytes function
     * @retval true if the test passed
     */
    bool test_util_copyBytes();

    /**
     * @brief  A test for util's copyVector function
     * @note   checks a normal copy
     * @retval true if the test passed
     */
    bool test_util_copyVector_1();

    /**
     * @brief  A test for util's copyVector function
     * @note   checks crash on nullptr origin
     * @retval true if the test passed
     */
    bool test_util_copyVector_2();

    /**
     * @brief  A test for util's copyVector function
     * @note   checks crash on nullptr destination
     * @retval true if the test passed
     */
    bool test_util_copyVector_3();

    /**
     * @brief  A test for util's copyVector function
     * @note   checks crash on origin offset + size too big
     * @retval true if the test passed
     */
    bool test_util_copyVector_4();

    /**
     * @brief  A test for util's copyVector function
     * @note   checks crash on destination offset + size too big
     * @retval true if the test passed
     */
    bool test_util_copyVector_5();

    /**
     * @brief  A test for util's copyVector function
     * @note   checks crash on size too large for origin
     * @retval true if the test passed
     */
    bool test_util_copyVector_6();

    /**
     * @brief  A test for util's copyVector function
     * @note   checks crash on size too large for destination
     * @retval true if the test passed
     */
    bool test_util_copyVector_7();

    /**
     * @brief  A test for util's extractStringFromVector function
     * @note   checks normal extraction
     * @retval true if the test passed
     */
    bool test_util_extractStringFromVector_1();

    /**
     * @brief  A test for util's extractStringFromVector function
     * @note   checks cropped extraction
     * @retval true if the test passed
     */
    bool test_util_extractStringFromVector_2();

    /**
     * @brief  A test for util's extractStringFromVector function
     * @note   checks extraction of empty vector
     * @retval true if the test passed
     */
    bool test_util_extractStringFromVector_3();

    /**
     * @brief  A test for util's extractStringFromVector function
     * @note   checks crash on massive offset
     * @retval true if the test passed
     */
    bool test_util_extractStringFromVector_4();

    /**
     * @brief  A test for util's extractStringFromVector function
     * @note   checks crash on massive size
     * @retval true if the test passed
     */
    bool test_util_extractStringFromVector_5();

    /**
     * @brief  A test for util's extractVectorFromString function
     * @note   checks normal extraction
     * @retval true if the test passed
     */
    bool test_util_extractVectorFromString_1();

    /**
     * @brief  A test for util's extractVectorFromString function
     * @note   checks cropped extraction
     * @retval true if the test passed
     */
    bool test_util_extractVectorFromString_2();

    /**
     * @brief  A test for util's extractVectorFromString function
     * @note   checks extraction from empty string
     * @retval true if the test passed
     */
    bool test_util_extractVectorFromString_3();

    /**
     * @brief  A test for util's extractVectorFromString function
     * @note   checks crash on massive offset
     * @retval true if the test passed
     */
    bool test_util_extractVectorFromString_4();

    /**
     * @brief  A test for util's extractVectorFromString function
     * @note   checks crash on massive size
     * @retval true if the test passed
     */
    bool test_util_extractVectorFromString_5();

    /**
     * @brief  A test for util's isStrA function
     * @note   this checks the empty string
     * @retval true if the test passed
     */
    bool test_util_isstra_1();

    /**
     * @brief  A test for util's isStrA function
     * @note   this checks a string of all possible legal characters
     * @retval true if the test passed
     */
    bool test_util_isstra_2();

    /**
     * @brief  A test for util's isStrA function
     * @note   this checks a string with an invalid character
     * @retval true if the test passed
     */
    bool test_util_isstra_3();

    /**
     * @brief  A test for util's isStrA function
     * @note   this checks some lowercase letters
     * @retval true if the test passed
     */
    bool test_util_isstra_4();

    /**
     * @brief  A test for util's isStrA function
     * @note   this checks if \0 works when allowZero is false
     * @retval true if the test passed
     */
    bool test_util_isstra_5();

    /**
     * @brief  A test for util's isStrA function
     * @note   this checks if \0 works when allowZero is true
     * @retval true if the test passed
     */
    bool test_util_isstra_6();

    /**
     * @brief  A test for util's isStrD function
     * @note   this checks the empty string
     * @retval true if the test passed
     */
    bool test_util_isstrd_1();

    /**
     * @brief  A test for util's isStrD function
     * @note   this checks a string of all possible legal characters
     * @retval true if the test passed
     */
    bool test_util_isstrd_2();

    /**
     * @brief  A test for util's isStrD function
     * @note   this checks a string with an invalid character
     * @retval true if the test passed
     */
    bool test_util_isstrd_3();

    /**
     * @brief  A test for util's isStrD function
     * @note   this checks some lowercase letters
     * @retval true if the test passed
     */
    bool test_util_isstrd_4();

    /**
     * @brief  A test for util's isStrD function
     * @note   this checks if \0 works when allowZero is false
     * @retval true if the test passed
     */
    bool test_util_isstrd_5();

    /**
     * @brief  A test for util's isStrD function
     * @note   this checks if \0 works when allowZero is true
     * @retval true if the test passed
     */
    bool test_util_isstrd_6();

    /**
     * @brief  A test for util's parseInt16LSB function
     * @note   this checks for an exception if the vector is too short
     * @retval true if the test passed
     */
    bool test_util_decodeint16lsb_1();

    /**
     * @brief  A test for util's parseInt16LSB function
     * @note   this checks for an exception if the vector is too long
     * @retval true if the test passed
     */
    bool test_util_decodeint16lsb_2();

    /**
     * @brief  A test for util's parseInt16LSB function
     * @note   this checks if a number is parsed correctly
     * @retval true if the test passed
     */
    bool test_util_decodeint16lsb_3();

    /**
     * @brief  A test for util's parseInt16MSB function
     * @note   this checks for an exception if the vector is too short
     * @retval true if the test passed
     */
    bool test_util_decodeint16msb_1();

    /**
     * @brief  A test for util's parseInt16MSB function
     * @note   this checks for an exception if the vector is too long
     * @retval true if the test passed
     */
    bool test_util_decodeint16msb_2();

    /**
     * @brief  A test for util's parseInt16MSB function
     * @note   this checks if a number is parsed correctly
     * @retval true if the test passed
     */
    bool test_util_decodeint16msb_3();

    /**
     * @brief  A test for util's parseInt16LSB_MSB function
     * @note   this checks for an exception if the vector is too short
     * @retval true if the test passed
     */
    bool test_util_decodeint16lsbmsb_1();

    /**
     * @brief  A test for util's parseInt16LSB_MSB function
     * @note   this checks for an exception if the vector is too long
     * @retval true if the test passed
     */
    bool test_util_decodeint16lsbmsb_2();

    /**
     * @brief  A test for util's parseInt16LSB_MSB function
     * @note   this checks for an exception if the vector is encoded wrong
     * @retval true if the test passed
     */
    bool test_util_decodeint16lsbmsb_3();

    /**
     * @brief  A test for util's parseInt16LSB_MSB function
     * @note   this checks if a number is parsed correctly
     * @retval true if the test passed
     */
    bool test_util_decodeint16lsbmsb_4();

    /**
     * @brief  A test for util's parseSint16LSB function
     * @note   this checks for an exception if the vector is too short
     * @retval true if the test passed
     */
    bool test_util_decodesint16lsb_1();

    /**
     * @brief  A test for util's parseSint16LSB function
     * @note   this checks for an exception if the vector is too long
     * @retval true if the test passed
     */
    bool test_util_decodesint16lsb_2();

    /**
     * @brief  A test for util's parseSint16LSB function
     * @note   this checks if a number is parsed correctly
     * @retval true if the test passed
     */
    bool test_util_decodesint16lsb_3();

    /**
     * @brief  A test for util's parseSint16MSB function
     * @note   this checks for an exception if the vector is too short
     * @retval true if the test passed
     */
    bool test_util_decodesint16msb_1();

    /**
     * @brief  A test for util's parseSint16MSB function
     * @note   this checks for an exception if the vector is too long
     * @retval true if the test passed
     */
    bool test_util_decodesint16msb_2();

    /**
     * @brief  A test for util's parseSint16MSB function
     * @note   this checks if a number is parsed correctly
     * @retval true if the test passed
     */
    bool test_util_decodesint16msb_3();

    /**
     * @brief  A test for util's parseSint16LSB_MSB function
     * @note   this checks for an exception if the vector is too short
     * @retval true if the test passed
     */
    bool test_util_decodesint16lsbmsb_1();

    /**
     * @brief  A test for util's parseSint16LSB_MSB function
     * @note   this checks for an exception if the vector is too long
     * @retval true if the test passed
     */
    bool test_util_decodesint16lsbmsb_2();

    /**
     * @brief  A test for util's parseSint16LSB_MSB function
     * @note   this checks for an exception if the vector is encoded wrong
     * @retval true if the test passed
     */
    bool test_util_decodesint16lsbmsb_3();

    /**
     * @brief  A test for util's parseSint16LSB_MSB function
     * @note   this checks if a number is parsed correctly
     * @retval true if the test passed
     */
    bool test_util_decodesint16lsbmsb_4();

    /**
     * @brief  A test for util's parseInt32LSB function
     * @note   this checks for an exception if the vector is too short
     * @retval true if the test passed
     */
    bool test_util_decodeint32lsb_1();

    /**
     * @brief  A test for util's parseInt32LSB function
     * @note   this checks for an exception if the vector is too long
     * @retval true if the test passed
     */
    bool test_util_decodeint32lsb_2();

    /**
     * @brief  A test for util's parseInt32LSB function
     * @note   this checks if a number is parsed correctly
     * @retval true if the test passed
     */
    bool test_util_decodeint32lsb_3();

    /**
     * @brief  A test for util's parseInt32MSB function
     * @note   this checks for an exception if the vector is too short
     * @retval true if the test passed
     */
    bool test_util_decodeint32msb_1();

    /**
     * @brief  A test for util's parseInt32MSB function
     * @note   this checks for an exception if the vector is too long
     * @retval true if the test passed
     */
    bool test_util_decodeint32msb_2();

    /**
     * @brief  A test for util's parseInt32MSB function
     * @note   this checks if a number is parsed correctly
     * @retval true if the test passed
     */
    bool test_util_decodeint32msb_3();

    /**
     * @brief  A test for util's parseInt32LSB_MSB function
     * @note   this checks for an exception if the vector is too short
     * @retval true if the test passed
     */
    bool test_util_decodeint32lsbmsb_1();

    /**
     * @brief  A test for util's parseInt32LSB_MSB function
     * @note   this checks for an exception if the vector is too long
     * @retval true if the test passed
     */
    bool test_util_decodeint32lsbmsb_2();

    /**
     * @brief  A test for util's parseInt32LSB_MSB function
     * @note   this checks for an exception if the vector is encoded wrong
     * @retval true if the test passed
     */
    bool test_util_decodeint32lsbmsb_3();

    /**
     * @brief  A test for util's parseInt32LSB_MSB function
     * @note   this checks if a number is parsed correctly
     * @retval true if the test passed
     */
    bool test_util_decodeint32lsbmsb_4();

    /**
     * @brief  A test for util's parseSint32LSB function
     * @note   this checks for an exception if the vector is too short
     * @retval true if the test passed
     */
    bool test_util_decodesint32lsb_1();

    /**
     * @brief  A test for util's parseSint32LSB function
     * @note   this checks for an exception if the vector is too long
     * @retval true if the test passed
     */
    bool test_util_decodesint32lsb_2();

    /**
     * @brief  A test for util's parseSint32LSB function
     * @note   this checks if a number is parsed correctly
     * @retval true if the test passed
     */
    bool test_util_decodesint32lsb_3();

    /**
     * @brief  A test for util's parseSint32MSB function
     * @note   this checks for an exception if the vector is too short
     * @retval true if the test passed
     */
    bool test_util_decodesint32msb_1();

    /**
     * @brief  A test for util's parseSint32MSB function
     * @note   this checks for an exception if the vector is too long
     * @retval true if the test passed
     */
    bool test_util_decodesint32msb_2();

    /**
     * @brief  A test for util's parseSint32MSB function
     * @note   this checks if a number is parsed correctly
     * @retval true if the test passed
     */
    bool test_util_decodesint32msb_3();

    /**
     * @brief  A test for util's parseSint32LSB_MSB function
     * @note   this checks for an exception if the vector is too short
     * @retval true if the test passed
     */
    bool test_util_decodesint32lsbmsb_1();

    /**
     * @brief  A test for util's parseSint32LSB_MSB function
     * @note   this checks for an exception if the vector is too long
     * @retval true if the test passed
     */
    bool test_util_decodesint32lsbmsb_2();

    /**
     * @brief  A test for util's parseSint32LSB_MSB function
     * @note   this checks for an exception if the vector is encoded wrong
     * @retval true if the test passed
     */
    bool test_util_decodesint32lsbmsb_3();

    /**
     * @brief  A test for util's parseSint32LSB_MSB function
     * @note   this checks if a number is parsed correctly
     * @retval true if the test passed
     */
    bool test_util_decodesint32lsbmsb_4();

    /**
     * @brief  A test for util's encodeInt16LSB function
     * @note   this checks if the vector is the correct size
     * @retval true if the test passed
     */
    bool test_util_encodeint16lsb_1();

    /**
     * @brief  A test for util's encodeInt16LSB function
     * @note   this checks if the vector contains the correct bytes
     * @retval true if the test passed
     */
    bool test_util_encodeint16lsb_2();

    /**
     * @brief  A test for util's encodeInt16MSB function
     * @note   this checks if the vector is the correct size
     * @retval true if the test passed
     */
    bool test_util_encodeint16msb_1();

    /**
     * @brief  A test for util's encodeInt16MSB function
     * @note   this checks if the vector contains the correct bytes
     * @retval true if the test passed
     */
    bool test_util_encodeint16msb_2();

    /**
     * @brief  A test for util's encodeInt16LSB_MSB function
     * @note   this checks if the vector is the correct size
     * @retval true if the test passed
     */
    bool test_util_encodeint16lsbmsb_1();

    /**
     * @brief  A test for util's encodeInt16LSB_MSB function
     * @note   this checks if the vector contains the correct bytes
     * @retval true if the test passed
     */
    bool test_util_encodeint16lsbmsb_2();

    /**
     * @brief  A test for util's encodeSint16LSB function
     * @note   this checks if the vector is the correct size
     * @retval true if the test passed
     */
    bool test_util_encodesint16lsb_1();

    /**
     * @brief  A test for util's encodeSint16LSB function
     * @note   this checks if the vector contains the correct bytes
     * @retval true if the test passed
     */
    bool test_util_encodesint16lsb_2();

    /**
     * @brief  A test for util's encodeSint16MSB function
     * @note   this checks if the vector is the correct size
     * @retval true if the test passed
     */
    bool test_util_encodesint16msb_1();

    /**
     * @brief  A test for util's encodeSint16MSB function
     * @note   this checks if the vector contains the correct bytes
     * @retval true if the test passed
     */
    bool test_util_encodesint16msb_2();

    /**
     * @brief  A test for util's encodeSint16LSB_MSB function
     * @note   this checks if the vector is the correct size
     * @retval true if the test passed
     */
    bool test_util_encodesint16lsbmsb_1();

    /**
     * @brief  A test for util's encodeSint16LSB_MSB function
     * @note   this checks if the vector contains the correct bytes
     * @retval true if the test passed
     */
    bool test_util_encodesint16lsbmsb_2();

    /**
     * @brief  A test for util's encodeInt32LSB function
     * @note   this checks if the vector is the correct size
     * @retval true if the test passed
     */
    bool test_util_encodeint32lsb_1();

    /**
     * @brief  A test for util's encodeInt32LSB function
     * @note   this checks if the vector contains the correct bytes
     * @retval true if the test passed
     */
    bool test_util_encodeint32lsb_2();

    /**
     * @brief  A test for util's encodeInt32MSB function
     * @note   this checks if the vector is the correct size
     * @retval true if the test passed
     */
    bool test_util_encodeint32msb_1();

    /**
     * @brief  A test for util's encodeInt32MSB function
     * @note   this checks if the vector contains the correct bytes
     * @retval true if the test passed
     */
    bool test_util_encodeint32msb_2();

    /**
     * @brief  A test for util's encodeInt32LSB_MSB function
     * @note   this checks if the vector is the correct size
     * @retval true if the test passed
     */
    bool test_util_encodeint32lsbmsb_1();

    /**
     * @brief  A test for util's encodeInt32LSB_MSB function
     * @note   this checks if the vector contains the correct bytes
     * @retval true if the test passed
     */
    bool test_util_encodeint32lsbmsb_2();

    /**
     * @brief  A test for util's encodeSint32LSB function
     * @note   this checks if the vector is the correct size
     * @retval true if the test passed
     */
    bool test_util_encodesint32lsb_1();

    /**
     * @brief  A test for util's encodeSint32LSB function
     * @note   this checks if the vector contains the correct bytes
     * @retval true if the test passed
     */
    bool test_util_encodesint32lsb_2();

    /**
     * @brief  A test for util's encodeSint32MSB function
     * @note   this checks if the vector is the correct size
     * @retval true if the test passed
     */
    bool test_util_encodesint32msb_1();

    /**
     * @brief  A test for util's encodeSint32MSB function
     * @note   this checks if the vector contains the correct bytes
     * @retval true if the test passed
     */
    bool test_util_encodesint32msb_2();

    /**
     * @brief  A test for util's encodeSint32LSB_MSB function
     * @note   this checks if the vector is the correct size
     * @retval true if the test passed
     */
    bool test_util_encodesint32lsbmsb_1();

    /**
     * @brief  A test for util's encodeSint32LSB_MSB function
     * @note   this checks if the vector contains the correct bytes
     * @retval true if the test passed
     */
    bool test_util_encodesint32lsbmsb_2();

    /**
     * @brief  A test for util's getBit function
     * @note   this checks for an exception if an invalid bit index is used
     * @retval true if the test passed
     */
    bool test_util_getbit_1();

    /**
     * @brief  A test for util's getBit function
     * @note   this checks if bits are fetched correctly
     * @retval true if the test passed
     */
    bool test_util_getbit_2();

    /**
     * @brief  A test for util's setBit function
     * @note   this checks for an exception if an invalid bit index is used
     * @retval true of the test passed
     */
    bool test_util_setbit_1();

    /**
     * @brief  A test for util's setBit function
     * @note   this checks if bits are set correctly
     * @retval true if the test passed
     */
    bool test_util_setbit_2();

    /**
     * @brief  A test for util's convertSectorToByteAddress function
     * @note   this checks if sector 0 converts to 0
     * @retval true if the test passed
     */
    bool test_util_convertsectortobyteaddress_1();

    /**
     * @brief  A test for util's convertSectorToByteAddress function
     * @note   this checks if sector 1 converts to 255
     * @retval true if the test passed
     */
    bool test_util_convertsectortobyteaddress_2();

    /**
     * @brief  A test for util's convertSectorToByteAddress function
     * @note   this checks if sector 2 converts to 510
     * @retval true if the test passed
     */
    bool test_util_convertsectortobyteaddress_3();

    /**
     * @brief  A test for util's convertSectorToByteAddress function
     * @note   this checks if sector 3 converts to 765
     * @retval true if the test passed
     */
    bool test_util_convertsectortobyteaddress_4();

    /**
     * @brief  A test for util's convertSectorToByteAddress function
     * @note   this checks if sector 4 converts to 1020
     * @retval true if the test passed
     */
    bool test_util_convertsectortobyteaddress_5();

    /**
     * @brief  A test for util's convertByteAddressToSector function
     * @note   this checks if address 0 converts to 0
     * @retval true if the test passed
     */
    bool test_util_convertbyteaddresstosector_1();

    /**
     * @brief  A test for util's convertByteAddressToSector function
     * @note   this checks if address 254 converts to 0
     * @retval true if the test passed
     */
    bool test_util_convertbyteaddresstosector_2();

    /**
     * @brief  A test for util's convertByteAddressToSector function
     * @note   this checks if address 255 converts to 1
     * @retval true if the test passed
     */
    bool test_util_convertbyteaddresstosector_3();

    /**
     * @brief  A test for util's convertByteAddressToSector function
     * @note   this checks if address 509 converts to 1
     * @retval true if the test passed
     */
    bool test_util_convertbyteaddresstosector_4();

    /**
     * @brief  A test for util's convertByteAddressToSector function
     * @note   this checks if address 510 converts to 2
     * @retval true if the test passed
     */
    bool test_util_convertbyteaddresstosector_5();

    /**
     * @brief  A test for util's sha256sum function
     * @note   this checks for an exception of vec is null
     * @retval true if the test passed
     */
    bool test_util_sha256sum_1();

    /**
     * @brief  A test for util's sha256sum function
     * @note   this checks the SHA256 hash of an empty vector
     * @retval true if the test passed
     */
    bool test_util_sha256sum_2();

    /**
     * @brief  A test for util's sha256sum function
     * @note   this checks the SHA256 hash of the letter "A"
     * @retval true if the test passed
     */
    bool test_util_sha256sum_3();

    /**
     * @brief  A test for util's sha256sum function
     * @note   this checks the SHA256 hash of amy name
     * @retval true if the test passed
     */
    bool test_util_sha256sum_4();

    /**
     * @brief  A test for util's dump_vector function
     * @note   this checks for an exception of vec is null
     * @retval true if the test passed
     */
    bool test_util_dump_vector_1();

    /**
     * @brief  A test for util's dump_vector function
     * @note   this checks if an empty file is stored
     * @retval true if the test passed
     */
    bool test_util_dump_vector_2();

    /**
     * @brief  A test for util's dump_vector function
     * @note   this checks if my name is stored correctly
     * @retval true if the test passed
     */
    bool test_util_dump_vector_3();

    /**
     * @brief  A test for util's load_vector function
     * @note   this checks if a bad path raises an error
     * @retval true if the test passed
     */
    bool test_util_load_vector_1();

    /**
     * @brief  A test for util's load_vector function
     * @note   this checks if an empty file is read correctly
     * @retval true if the test passed
     */
    bool test_util_load_vector_2();

    /**
     * @brief  A test for util's load_vector function
     * @note   this checks if amy name is read correctly
     * @retval true if the test passed
     */
    bool test_util_load_vector_3();

    /**
     * @brief  This returns a vector of tests to run
     * @retval a vector of tests to run
     */
    TEST_VECTOR_T getTests();
}
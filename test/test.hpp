/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// includes
#include <string>
#include "testCommon.hpp"

/**
 * @brief  Runs a test, display the test status, and update the counters
 * @param  test a pointer to the test function to run
 * @param  name the name of the test being run (human version)
 * @param  passedCount: the number of tests passed so far
 * @param  totalCount: the number of tests ran so far
 * @retval None
 */
void exec_test(test_ptr test, std::string name, int *passedCount, int *totalCount);

/**
 * @brief  Merges a test vector into the main test vector
 * @param  all: vector of all tests
 * @param  tests: test vector to merge
 * @retval None
 */
void add_tests(TEST_VECTOR_T *all, TEST_VECTOR_T tests);

/**
 * @brief  This is the main driver for the testing suite
 * @param  argc number of arguments in the argv array
 * @param  argv: an array of arguments
 * @retval exit code
 */
int main(int argc, char **argv);
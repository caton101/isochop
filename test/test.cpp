/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "test.hpp"
#include "testCommon.hpp"
#include "testDummy.hpp"
#include "testUtils.hpp"
#include "testOrphan.hpp"
#include "testDecDatetime.hpp"
#include "testDirDatetime.hpp"
#include "testFileFlags.hpp"
#include "testDirectory.hpp"
#include "testExtentData.hpp"
#include "testExtentNode.hpp"
#include "testVolumeDescriptor.hpp"
#include "testPrimaryVolumeDescriptor.hpp"
#include "testStructure.hpp"

void exec_test(test_ptr test, std::string name, int *passedCount, int *totalCount)
{
    // show test name
    std::cout << "Test " << (*totalCount) + 1 << ": " << name << "... ";
    std::flush(std::cout);
    // run the test
    bool stat = test();
    // show results
    std::cout << (stat ? "PASSED" : "FAILED") << "." << std::endl;
    // update the counters
    (*passedCount) += stat ? 1 : 0;
    (*totalCount) += 1;
}

void add_tests(TEST_VECTOR_T *all, TEST_VECTOR_T tests)
{
    if (!all)
    {
        throw new std::invalid_argument("all == nullptr");
    }
    all->insert(all->end(), tests.begin(), tests.end());
}

int main(int argc, char **argv)
{
    // make counters
    int passedTests = 0;
    int totalTests = 0;

    // load tests
    TEST_VECTOR_T tests;
    add_tests(&tests, testDummy::getTests());
    add_tests(&tests, testUtils::getTests());
    add_tests(&tests, testOrphan::getTests());
    add_tests(&tests, testDecDatetime::getTests());
    add_tests(&tests, testDirDatetime::getTests());
    add_tests(&tests, testVolumeDescriptor::getTests());
    add_tests(&tests, testFileFlags::getTests());
    add_tests(&tests, testDirectory::getTests());
    add_tests(&tests, testExtentData::getTests());
    add_tests(&tests, testExtentNode::getTests());
    add_tests(&tests, testPrimaryVolumeDescriptor::getTests());
    add_tests(&tests, testStructure::getTests());

    // execute all tests
    for (test_data test : tests)
    {
        exec_test(test.function, test.description, &passedTests, &totalTests);
    }

    // print the results
    std::cout << "Testing complete." << std::endl;
    std::cout << "Passed " << passedTests << "/" << totalTests << " tests." << std::endl;
    std::cout << "Solution coverage: " << (((double)passedTests / totalTests) * 100) << "%" << std::endl;

    // stop program
    return passedTests != totalTests;
}

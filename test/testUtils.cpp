/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <filesystem>
#include "testCommon.hpp"
#include "testUtils.hpp"
#include "../include/utils.hpp"

bool testUtils::test_util_copyBytes()
{
    char canary = 42;
    char source[5] = {1, 2, 3, 4, 5};
    char dest[6] = {0, 0, 0, 0, 0, canary};
    bool status = true;
    isochop::utils::copyBytes(source, dest, 5);
    status &= source[0] == dest[0];
    status &= source[1] == dest[1];
    status &= source[2] == dest[2];
    status &= source[3] == dest[3];
    status &= source[4] == dest[4];
    status &= dest[5] == canary;
    return status;
}

bool testUtils::test_util_copyVector_1()
{
    char canary = 6;
    std::vector<char> source = {1, 2, 3, 4, 5};
    std::vector<char> dest = {0, 0, 0, 0, 0, canary};
    isochop::utils::copyVector(&source, &dest, 0, 0, 5);
    bool status = true;
    status &= source[0] == dest[0];
    status &= source[1] == dest[1];
    status &= source[2] == dest[2];
    status &= source[3] == dest[3];
    status &= source[4] == dest[4];
    status &= dest[5] == canary;
    return status;
}

bool testUtils::test_util_copyVector_2()
{
    std::vector<char> source = {1, 2, 3, 4, 5};
    std::vector<char> dest = {0, 0, 0, 0, 0, 0};
    try
    {
        // origin offset is negative
        isochop::utils::copyVector(nullptr, &dest, -1, 0, 5);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_copyVector_3()
{
    std::vector<char> source = {1, 2, 3, 4, 5};
    std::vector<char> dest = {0, 0, 0, 0, 0, 0};
    try
    {
        // dest offset is negative
        isochop::utils::copyVector(&source, nullptr, 0, -1, 5);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_copyVector_4()
{
    std::vector<char> source = {1, 2, 3, 4, 5};
    std::vector<char> dest = {0, 0, 0, 0, 0, 0};
    try
    {
        // origin offset + size is too big
        isochop::utils::copyVector(&source, &dest, 1, 0, 5);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_copyVector_5()
{
    std::vector<char> source = {1, 2, 3, 4, 5};
    std::vector<char> dest = {0, 0, 0, 0, 0, 0};
    try
    {
        // destination size + offset is too big
        isochop::utils::copyVector(&source, &dest, 0, 2, 5);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_copyVector_6()
{
    std::vector<char> source = {1, 2, 3, 4, 5};
    std::vector<char> dest = {0, 0, 0, 0, 0, 0};
    try
    {
        // size is too large for origin
        isochop::utils::copyVector(&source, &dest, 0, 0, 6);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_copyVector_7()
{
    std::vector<char> source = {1, 2, 3, 4, 5, 6, 7, 8};
    std::vector<char> dest = {0, 0, 0, 0, 0, 0};
    try
    {
        // size is too big for destination
        isochop::utils::copyVector(&source, &dest, 0, 0, 7);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_extractStringFromVector_1()
{
    std::vector<char> vec = {'C', 'A', 'M', 'E', 'R', 'O', 'N'};
    std::string test = isochop::utils::extractStringFromVector(vec, 0, vec.size());
    return test == "CAMERON";
}

bool testUtils::test_util_extractStringFromVector_2()
{
    std::vector<char> vec = {'C', 'A', 'M', 'E', 'R', 'O', 'N'};
    std::string test = isochop::utils::extractStringFromVector(vec, 1, vec.size() - 2);
    return test == "AMERO";
}

bool testUtils::test_util_extractStringFromVector_3()
{
    std::vector<char> vec = {};
    std::string test = isochop::utils::extractStringFromVector(vec, 0, vec.size());
    return test == "";
}

bool testUtils::test_util_extractStringFromVector_4()
{
    std::vector<char> vec = {'C', 'A', 'M', 'E', 'R', 'O', 'N'};
    try
    {
        // size+offset is too large (inflated offset)
        std::string test = isochop::utils::extractStringFromVector(vec, vec.size() + 1, 0);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_extractStringFromVector_5()
{
    std::vector<char> vec = {'C', 'A', 'M', 'E', 'R', 'O', 'N'};
    try
    {
        // size+offset is too large (inflated size)
        std::string test = isochop::utils::extractStringFromVector(vec, 0, vec.size() + 1);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_extractVectorFromString_1()
{
    std::string str = "CAMERON";
    std::vector<char> ans = {'C', 'A', 'M', 'E', 'R', 'O', 'N'};
    std::vector<char> out = isochop::utils::extractVectorFromString(str, 0, str.size());
    if (ans.size() != out.size())
    {
        return false;
    }
    for (std::size_t i = 0; i < ans.size(); i++)
    {
        if (ans[i] != out[i])
        {
            return false;
        }
    }
    return true;
}

bool testUtils::test_util_extractVectorFromString_2()
{
    std::string str = "CAMERON";
    std::vector<char> ans = {'A', 'M', 'E', 'R', 'O'};
    std::vector<char> out = isochop::utils::extractVectorFromString(str, 1, str.size() - 2);
    if (ans.size() != out.size())
    {
        return false;
    }
    for (std::size_t i = 0; i < ans.size(); i++)
    {
        if (ans[i] != out[i])
        {
            return false;
        }
    }
    return true;
}

bool testUtils::test_util_extractVectorFromString_3()
{
    std::string str = "";
    std::vector<char> out = isochop::utils::extractVectorFromString(str, 0, str.size());
    if (0 != out.size())
    {
        return false;
    }
    return true;
}

bool testUtils::test_util_extractVectorFromString_4()
{
    std::string str = "CAMERON";
    try
    {
        // offset + size is too large (inflated offset)
        std::vector<char> test = isochop::utils::extractVectorFromString(str, str.size() + 1, 0);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_extractVectorFromString_5()
{
    std::string str = "CAMERON";
    try
    {
        // offset + size is too large (inflated size)
        std::vector<char> test = isochop::utils::extractVectorFromString(str, 0, str.size() + 1);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_isstra_1()
{
    // this string should be valid
    std::string str = "";
    return isochop::utils::isStrA(str, false);
}

bool testUtils::test_util_isstra_2()
{
    // this string should be valid
    std::string str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_!\"%&'()*+,-./:;<=>? ";
    return isochop::utils::isStrA(str, false);
}

bool testUtils::test_util_isstra_3()
{
    // this string should be invalid ('\t' is illegal)
    std::string str = "\tABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_!\"%&'()*+,-./:;<=>? ";
    return !isochop::utils::isStrA(str, false);
}

bool testUtils::test_util_isstra_4()
{
    // this string should be invalid (lowercase is illegal)
    std::string str = "deadbeef";
    return !isochop::utils::isStrA(str, false);
}

bool testUtils::test_util_isstra_5()
{
    // this should be invalid (allowZero is false)
    std::string str = "HELLO";
    str += '\0';
    str += '\0';
    str += '\0';
    return !isochop::utils::isStrA(str, false);
}

bool testUtils::test_util_isstra_6()
{
    // this should be valid (allowZero is true)
    std::string str = "HELLO";
    str += '\0';
    str += '\0';
    str += '\0';
    return isochop::utils::isStrA(str, true);
}

bool testUtils::test_util_isstrd_1()
{
    // this string should be valid
    std::string str = "";
    return isochop::utils::isStrD(str, false);
}

bool testUtils::test_util_isstrd_2()
{
    // this string should be valid
    std::string str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_ ";
    return isochop::utils::isStrD(str, false);
}

bool testUtils::test_util_isstrd_3()
{
    // this string should be invalid ('\t' is illegal)
    std::string str = "\tABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_ ";
    return !isochop::utils::isStrD(str, false);
}

bool testUtils::test_util_isstrd_4()
{
    // this string should be invalid (lowercase is illegal)
    std::string str = "deadbeef";
    return !isochop::utils::isStrD(str, false);
}

bool testUtils::test_util_isstrd_5()
{
    // this should be invalid (allowZero is false)
    std::string str = "HELLO";
    str += '\0';
    str += '\0';
    str += '\0';
    return !isochop::utils::isStrD(str, false);
}

bool testUtils::test_util_isstrd_6()
{
    // this should be valid (allowZero is true)
    std::string str = "HELLO";
    str += '\0';
    str += '\0';
    str += '\0';
    return isochop::utils::isStrD(str, true);
}

bool testUtils::test_util_decodeint16lsb_1()
{
    std::vector<char> test(1, 0);
    try
    {
        isochop::utils::decodeInt16LSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodeint16lsb_2()
{
    std::vector<char> test(3, 0);
    try
    {
        isochop::utils::decodeInt16LSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodeint16lsb_3()
{
    std::vector<char> test = {0x65, (char)0x87};
    uint16_t out = isochop::utils::decodeInt16LSB(test);
    return out == 34661;
}

bool testUtils::test_util_decodeint16msb_1()
{
    std::vector<char> test(1, 0);
    try
    {
        isochop::utils::decodeInt16MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodeint16msb_2()
{
    std::vector<char> test(3, 0);
    try
    {
        isochop::utils::decodeInt16MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodeint16msb_3()
{
    std::vector<char> test = {(char)0x87, 0x65};
    uint16_t out = isochop::utils::decodeInt16MSB(test);
    return out == 34661;
}

bool testUtils::test_util_decodeint16lsbmsb_1()
{
    std::vector<char> test(3, 0);
    try
    {
        isochop::utils::decodeInt16LSB_MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodeint16lsbmsb_2()
{
    std::vector<char> test(5, 0);
    try
    {
        isochop::utils::decodeInt16LSB_MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodeint16lsbmsb_3()
{
    std::vector<char> test = {1, 2, 3, 4};
    try
    {
        isochop::utils::decodeInt16LSB_MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodeint16lsbmsb_4()
{
    std::vector<char> test = {0x65, (char)0x87, (char)0x87, 0x65};
    uint16_t out = isochop::utils::decodeInt16LSB_MSB(test);
    return out == 34661;
}

bool testUtils::test_util_decodesint16lsb_1()
{
    std::vector<char> test(1, 0);
    try
    {
        isochop::utils::decodeSint16LSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodesint16lsb_2()
{
    std::vector<char> test(3, 0);
    try
    {
        isochop::utils::decodeSint16LSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodesint16lsb_3()
{
    std::vector<char> test = {0x65, (char)0x87};
    int16_t out = isochop::utils::decodeSint16LSB(test);
    return out == -30875;
}

bool testUtils::test_util_decodesint16msb_1()
{
    std::vector<char> test(1, 0);
    try
    {
        isochop::utils::decodeSint16MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodesint16msb_2()
{
    std::vector<char> test(3, 0);
    try
    {
        isochop::utils::decodeSint16MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodesint16msb_3()
{
    std::vector<char> test = {(char)0x87, 0x65};
    int16_t out = isochop::utils::decodeSint16MSB(test);
    return out == -30875;
}

bool testUtils::test_util_decodesint16lsbmsb_1()
{
    std::vector<char> test(3, 0);
    try
    {
        isochop::utils::decodeSint16LSB_MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodesint16lsbmsb_2()
{
    std::vector<char> test(5, 0);
    try
    {
        isochop::utils::decodeSint16LSB_MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodesint16lsbmsb_3()
{
    std::vector<char> test = {1, 2, 3, 4};
    try
    {
        isochop::utils::decodeSint16LSB_MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodesint16lsbmsb_4()
{
    std::vector<char> test = {0x65, (char)0x87, (char)0x87, 0x65};
    int16_t out = isochop::utils::decodeSint16LSB_MSB(test);
    return out == -30875;
}

bool testUtils::test_util_decodeint32lsb_1()
{
    std::vector<char> test(3, 0);
    try
    {
        isochop::utils::decodeInt32LSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodeint32lsb_2()
{
    std::vector<char> test(5, 0);
    try
    {
        isochop::utils::decodeInt32LSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodeint32lsb_3()
{
    std::vector<char> test = {0x21, 0x43, 0x65, (char)0x87};
    uint32_t out = isochop::utils::decodeInt32LSB(test);
    return out == 2271560481;
}

bool testUtils::test_util_decodeint32msb_1()
{
    std::vector<char> test(3, 0);
    try
    {
        isochop::utils::decodeInt32MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodeint32msb_2()
{
    std::vector<char> test(5, 0);
    try
    {
        isochop::utils::decodeInt32MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodeint32msb_3()
{
    std::vector<char> test = {(char)0x87, 0x65, 0x43, 0x21};
    uint32_t out = isochop::utils::decodeInt32MSB(test);
    return out == 2271560481;
}

bool testUtils::test_util_decodeint32lsbmsb_1()
{
    std::vector<char> test(7, 0);
    try
    {
        isochop::utils::decodeInt32LSB_MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodeint32lsbmsb_2()
{
    std::vector<char> test(9, 0);
    try
    {
        isochop::utils::decodeInt32LSB_MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodeint32lsbmsb_3()
{
    std::vector<char> test = {1, 2, 3, 4, 5, 6, 7, 8};
    try
    {
        isochop::utils::decodeInt32LSB_MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodeint32lsbmsb_4()
{
    std::vector<char> test = {0x21, 0x43, 0x65, (char)0x87, (char)0x87, 0x65, 0x43, 0x21};
    uint32_t out = isochop::utils::decodeInt32LSB_MSB(test);
    return out == 2271560481;
}

bool testUtils::test_util_decodesint32lsb_1()
{
    std::vector<char> test(3, 0);
    try
    {
        isochop::utils::decodeSint32LSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodesint32lsb_2()
{
    std::vector<char> test(5, 0);
    try
    {
        isochop::utils::decodeSint32LSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodesint32lsb_3()
{
    std::vector<char> test = {0x21, 0x43, 0x65, (char)0x87};
    int32_t out = isochop::utils::decodeSint32LSB(test);
    return out == -2023406815;
}

bool testUtils::test_util_decodesint32msb_1()
{
    std::vector<char> test(3, 0);
    try
    {
        isochop::utils::decodeSint32MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodesint32msb_2()
{
    std::vector<char> test(5, 0);
    try
    {
        isochop::utils::decodeSint32MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodesint32msb_3()
{
    std::vector<char> test = {(char)0x87, 0x65, 0x43, 0x21};
    int32_t out = isochop::utils::decodeSint32MSB(test);
    return out == -2023406815;
}

bool testUtils::test_util_decodesint32lsbmsb_1()
{
    std::vector<char> test(7, 0);
    try
    {
        isochop::utils::decodeSint32LSB_MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodesint32lsbmsb_2()
{
    std::vector<char> test(9, 0);
    try
    {
        isochop::utils::decodeSint32LSB_MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodesint32lsbmsb_3()
{
    std::vector<char> test = {1, 2, 3, 4, 5, 6, 7, 8};
    try
    {
        isochop::utils::decodeSint32LSB_MSB(test);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_decodesint32lsbmsb_4()
{
    std::vector<char> test = {0x21, 0x43, 0x65, (char)0x87, (char)0x87, 0x65, 0x43, 0x21};
    int32_t out = isochop::utils::decodeSint32LSB_MSB(test);
    return out == -2023406815;
}

bool testUtils::test_util_encodeint16lsb_1()
{
    std::vector<char> test = isochop::utils::encodeInt16LSB(0);
    return test.size() == 2;
}

bool testUtils::test_util_encodeint16lsb_2()
{
    std::vector<char> test = isochop::utils::encodeInt16LSB(34661);
    bool valid = true;
    valid &= test.at(0) == 0x65;
    valid &= test.at(1) == (char)0x87;
    return valid;
}

bool testUtils::test_util_encodeint16msb_1()
{
    std::vector<char> test = isochop::utils::encodeInt16MSB(0);
    return test.size() == 2;
}

bool testUtils::test_util_encodeint16msb_2()
{
    std::vector<char> test = isochop::utils::encodeInt16MSB(34661);
    bool valid = true;
    valid &= test.at(0) == (char)0x87;
    valid &= test.at(1) == 0x65;
    return valid;
}

bool testUtils::test_util_encodeint16lsbmsb_1()
{
    std::vector<char> test = isochop::utils::encodeInt16LSB_MSB(0);
    return test.size() == 4;
}

bool testUtils::test_util_encodeint16lsbmsb_2()
{
    std::vector<char> test = isochop::utils::encodeInt16LSB_MSB(34661);
    bool valid = true;
    valid &= test.at(0) == 0x65;
    valid &= test.at(1) == (char)0x87;
    valid &= test.at(2) == (char)0x87;
    valid &= test.at(3) == 0x65;
    return valid;
}

bool testUtils::test_util_encodesint16lsb_1()
{
    std::vector<char> test = isochop::utils::encodeSint16LSB(0);
    return test.size() == 2;
}

bool testUtils::test_util_encodesint16lsb_2()
{
    std::vector<char> test = isochop::utils::encodeSint16LSB(-30875);
    bool valid = true;
    valid &= test.at(0) == 0x65;
    valid &= test.at(1) == (char)0x87;
    return valid;
}

bool testUtils::test_util_encodesint16msb_1()
{
    std::vector<char> test = isochop::utils::encodeSint16MSB(0);
    return test.size() == 2;
}

bool testUtils::test_util_encodesint16msb_2()
{
    std::vector<char> test = isochop::utils::encodeSint16MSB(-30875);
    bool valid = true;
    valid &= test.at(0) == (char)0x87;
    valid &= test.at(1) == 0x65;
    return valid;
}

bool testUtils::test_util_encodesint16lsbmsb_1()
{
    std::vector<char> test = isochop::utils::encodeSint16LSB_MSB(0);
    return test.size() == 4;
}

bool testUtils::test_util_encodesint16lsbmsb_2()
{
    std::vector<char> test = isochop::utils::encodeSint16LSB_MSB(-30875);
    bool valid = true;
    valid &= test.at(0) == 0x65;
    valid &= test.at(1) == (char)0x87;
    valid &= test.at(2) == (char)0x87;
    valid &= test.at(3) == 0x65;
    return valid;
}

bool testUtils::test_util_encodeint32lsb_1()
{
    std::vector<char> test = isochop::utils::encodeInt32LSB(0);
    return test.size() == 4;
}

bool testUtils::test_util_encodeint32lsb_2()
{
    std::vector<char> test = isochop::utils::encodeInt32LSB(2271560481);
    bool valid = true;
    valid &= test.at(0) == 0x21;
    valid &= test.at(1) == 0x43;
    valid &= test.at(2) == 0x65;
    valid &= test.at(3) == (char)0x87;
    return valid;
}

bool testUtils::test_util_encodeint32msb_1()
{
    std::vector<char> test = isochop::utils::encodeInt32MSB(0);
    return test.size() == 4;
}

bool testUtils::test_util_encodeint32msb_2()
{
    std::vector<char> test = isochop::utils::encodeInt32MSB(2271560481);
    bool valid = true;
    valid &= test.at(0) == (char)0x87;
    valid &= test.at(1) == 0x65;
    valid &= test.at(2) == 0x43;
    valid &= test.at(3) == 0x21;
    return valid;
}

bool testUtils::test_util_encodeint32lsbmsb_1()
{
    std::vector<char> test = isochop::utils::encodeInt32LSB_MSB(0);
    return test.size() == 8;
}

bool testUtils::test_util_encodeint32lsbmsb_2()
{
    std::vector<char> test = isochop::utils::encodeInt32LSB_MSB(2271560481);
    bool valid = true;
    valid &= test.at(0) == 0x21;
    valid &= test.at(1) == 0x43;
    valid &= test.at(2) == 0x65;
    valid &= test.at(3) == (char)0x87;
    valid &= test.at(4) == (char)0x87;
    valid &= test.at(5) == 0x65;
    valid &= test.at(6) == 0x43;
    valid &= test.at(7) == 0x21;
    return valid;
}

bool testUtils::test_util_encodesint32lsb_1()
{
    std::vector<char> test = isochop::utils::encodeSint32LSB(0);
    return test.size() == 4;
}

bool testUtils::test_util_encodesint32lsb_2()
{
    std::vector<char> test = isochop::utils::encodeSint32LSB(-2023406815);
    bool valid = true;
    valid &= test.at(0) == 0x21;
    valid &= test.at(1) == 0x43;
    valid &= test.at(2) == 0x65;
    valid &= test.at(3) == (char)0x87;
    return valid;
}

bool testUtils::test_util_encodesint32msb_1()
{
    std::vector<char> test = isochop::utils::encodeSint32MSB(0);
    return test.size() == 4;
}

bool testUtils::test_util_encodesint32msb_2()
{
    std::vector<char> test = isochop::utils::encodeSint32MSB(-2023406815);
    bool valid = true;
    valid &= test.at(0) == (char)0x87;
    valid &= test.at(1) == (char)0x65;
    valid &= test.at(2) == (char)0x43;
    valid &= test.at(3) == (char)0x21;
    return valid;
}

bool testUtils::test_util_encodesint32lsbmsb_1()
{
    std::vector<char> test = isochop::utils::encodeSint32LSB_MSB(0);
    return test.size() == 8;
}

bool testUtils::test_util_encodesint32lsbmsb_2()
{
    std::vector<char> test = isochop::utils::encodeSint32LSB_MSB(-2023406815);
    bool valid = true;
    valid &= test.at(0) == 0x21;
    valid &= test.at(1) == 0x43;
    valid &= test.at(2) == 0x65;
    valid &= test.at(3) == (char)0x87;
    valid &= test.at(4) == (char)0x87;
    valid &= test.at(5) == 0x65;
    valid &= test.at(6) == 0x43;
    valid &= test.at(7) == 0x21;
    return valid;
}

bool testUtils::test_util_getbit_1()
{
    try
    {
        isochop::utils::getBit(5, 8);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_getbit_2()
{
    bool passed = true;
    passed &= isochop::utils::getBit(0b00000001, 0) == 1;
    passed &= isochop::utils::getBit(0b00000010, 1) == 1;
    passed &= isochop::utils::getBit(0b00000100, 2) == 1;
    passed &= isochop::utils::getBit(0b00001000, 3) == 1;
    passed &= isochop::utils::getBit(0b00010000, 4) == 1;
    passed &= isochop::utils::getBit(0b00100000, 5) == 1;
    passed &= isochop::utils::getBit(0b01000000, 6) == 1;
    passed &= isochop::utils::getBit(0b10000000, 7) == 1;
    return passed;
}

bool testUtils::test_util_setbit_1()
{
    try
    {
        isochop::utils::setBit(5, 8, 1);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_setbit_2()
{
    bool passed = true;
    passed &= isochop::utils::setBit(0b11111111, 0, 0) == 0b11111110;
    passed &= isochop::utils::setBit(0b11111111, 1, 0) == 0b11111101;
    passed &= isochop::utils::setBit(0b11111111, 2, 0) == 0b11111011;
    passed &= isochop::utils::setBit(0b11111111, 3, 0) == 0b11110111;
    passed &= isochop::utils::setBit(0b11111111, 4, 0) == 0b11101111;
    passed &= isochop::utils::setBit(0b11111111, 5, 0) == 0b11011111;
    passed &= isochop::utils::setBit(0b11111111, 6, 0) == 0b10111111;
    passed &= isochop::utils::setBit(0b11111111, 7, 0) == 0b01111111;
    passed &= isochop::utils::setBit(0b00000000, 0, 1) == 0b00000001;
    passed &= isochop::utils::setBit(0b00000000, 1, 1) == 0b00000010;
    passed &= isochop::utils::setBit(0b00000000, 2, 1) == 0b00000100;
    passed &= isochop::utils::setBit(0b00000000, 3, 1) == 0b00001000;
    passed &= isochop::utils::setBit(0b00000000, 4, 1) == 0b00010000;
    passed &= isochop::utils::setBit(0b00000000, 5, 1) == 0b00100000;
    passed &= isochop::utils::setBit(0b00000000, 6, 1) == 0b01000000;
    passed &= isochop::utils::setBit(0b00000000, 7, 1) == 0b10000000;
    return passed;
}

bool testUtils::test_util_convertsectortobyteaddress_1()
{
    return isochop::utils::convertSectorToByteAddress(0) == 0;
}

bool testUtils::test_util_convertsectortobyteaddress_2()
{
    return isochop::utils::convertSectorToByteAddress(1) == 2048;
}

bool testUtils::test_util_convertsectortobyteaddress_3()
{
    return isochop::utils::convertSectorToByteAddress(2) == 4096;
}

bool testUtils::test_util_convertsectortobyteaddress_4()
{
    return isochop::utils::convertSectorToByteAddress(3) == 6144;
}

bool testUtils::test_util_convertsectortobyteaddress_5()
{
    return isochop::utils::convertSectorToByteAddress(4) == 8192;
}

bool testUtils::test_util_convertbyteaddresstosector_1()
{
    return isochop::utils::convertByteAddressToSector(0) == 0;
}

bool testUtils::test_util_convertbyteaddresstosector_2()
{
    return isochop::utils::convertByteAddressToSector(2047) == 0;
}

bool testUtils::test_util_convertbyteaddresstosector_3()
{
    return isochop::utils::convertByteAddressToSector(2048) == 1;
}

bool testUtils::test_util_convertbyteaddresstosector_4()
{
    return isochop::utils::convertByteAddressToSector(4095) == 1;
}

bool testUtils::test_util_convertbyteaddresstosector_5()
{
    return isochop::utils::convertByteAddressToSector(4096) == 2;
}

bool testUtils::test_util_sha256sum_1()
{
    try
    {
        isochop::utils::sha256sum(nullptr);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testUtils::test_util_sha256sum_2()
{
    std::vector<char> in;
    std::string ans = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";
    std::string out = isochop::utils::sha256sum(&in);
    return ans == out;
}

bool testUtils::test_util_sha256sum_3()
{
    std::vector<char> in = {'A'};
    std::string ans = "559aead08264d5795d3909718cdd05abd49572e84fe55590eef31a88a08fdffd";
    std::string out = isochop::utils::sha256sum(&in);
    return ans == out;
}

bool testUtils::test_util_sha256sum_4()
{
    std::vector<char> in = {'C', 'a', 'm', 'e', 'r', 'o', 'n'};
    std::string ans = "3fae11bddd374e4966d61e5d9e154552e6772bdfaf6f87349e53403cd22f4fed";
    std::string out = isochop::utils::sha256sum(&in);
    return ans == out;
}

/**
 * @brief  A test for util's dump_vector function
 * @note   this checks for an exception of vec is null
 * @retval true if the test passed
 */
bool testUtils::test_util_dump_vector_1()
{
    bool error;
    isochop::utils::dump_vector(nullptr, "temp.dat", &error);
    return error;
}

/**
 * @brief  A test for util's dump_vector function
 * @note   this checks if an empty file is stored
 * @retval true if the test passed
 */
bool testUtils::test_util_dump_vector_2()
{
    // set up useful fields
    std::vector<char> ans;
    bool error;
    // dump test vector
    if (std::filesystem::exists("test.dat"))
    {
        std::filesystem::remove("test.dat");
    }
    isochop::utils::dump_vector(&ans, "test.dat", &error);
    bool passed = !error;
    // ensure file was written
    if (!std::filesystem::exists("test.dat"))
    {
        return false;
    }
    // read file contents
    std::fstream file;
    file.open("test.dat", std::fstream::in | std::fstream::binary);
    if (!file.good())
    {
        return false;
    }
    std::vector<char> buffer;
    while (file.peek() != std::fstream::traits_type::eof())
    {
        char c;
        file.read(&c, 1);
        buffer.push_back(c);
    }
    file.close();
    // compare file contents
    passed &= ans.size() == buffer.size();
    if (passed)
    {
        for (size_t i = 0; i < ans.size(); i++)
        {
            if (ans.at(i) != buffer.at(i))
            {
                return false;
            }
        }
    }
    // return test status
    return passed;
}

/**
 * @brief  A test for util's dump_vector function
 * @note   this checks if my name is stored correctly
 * @retval true if the test passed
 */
bool testUtils::test_util_dump_vector_3()
{
    // set up useful fields
    std::vector<char> ans = {'C', 'a', 'm', 'e', 'r', 'o', 'n'};
    bool error;
    // dump test vector
    if (std::filesystem::exists("test.dat"))
    {
        std::filesystem::remove("test.dat");
    }
    isochop::utils::dump_vector(&ans, "test.dat", &error);
    bool passed = !error;
    // ensure file was written
    if (!std::filesystem::exists("test.dat"))
    {
        return false;
    }
    // read file contents
    std::fstream file;
    file.open("test.dat", std::fstream::in | std::fstream::binary);
    if (!file.good())
    {
        return false;
    }
    std::vector<char> buffer;
    while (file.peek() != std::fstream::traits_type::eof())
    {
        char c;
        file.read(&c, 1);
        buffer.push_back(c);
    }
    file.close();
    // compare file contents
    passed &= ans.size() == buffer.size();
    if (passed)
    {
        for (size_t i = 0; i < ans.size(); i++)
        {
            if (ans.at(i) != buffer.at(i))
            {
                return false;
            }
        }
    }
    // return test status
    return passed;
}

/**
 * @brief  A test for util's load_vector function
 * @note   this checks if a bad path raises an error
 * @retval true if the test passed
 */
bool testUtils::test_util_load_vector_1()
{
    // ensure test.dat does not exist
    if (std::filesystem::exists("test.dat"))
    {
        std::filesystem::remove("test.dat");
    }
    // run test
    bool error;
    isochop::utils::load_vector("test.dat", &error);
    return error;
}

/**
 * @brief  A test for util's load_vector function
 * @note   this checks if an empty file is read correctly
 * @retval true if the test passed
 */
bool testUtils::test_util_load_vector_2()
{
    // set up useful fields
    std::vector<char> ans;
    bool error;
    // ensure test.dat does not exist
    if (std::filesystem::exists("test.dat"))
    {
        std::filesystem::remove("test.dat");
    }
    // generate new test.dat with answer key
    std::fstream file;
    file.open("test.dat", std::fstream::out | std::fstream::binary);
    if (!file.good())
    {
        return false;
    }
    for (char c : ans)
    {
        file.write(&c, 1);
    }
    file.close();
    // attempt to load the file
    std::vector<char> buffer = isochop::utils::load_vector("test.dat", &error);
    // check file for errors
    bool passed = !error;
    passed &= buffer.size() == ans.size();
    if (passed)
    {
        for (size_t i = 0; i < ans.size(); i++)
        {
            if (ans.at(i) != buffer.at(i))
            {
                return false;
            }
        }
    }
    // return test status
    return passed;
}

/**
 * @brief  A test for util's load_vector function
 * @note   this checks if amy name is read correctly
 * @retval true if the test passed
 */
bool testUtils::test_util_load_vector_3()
{
    // set up useful fields
    std::vector<char> ans = {'C', 'a', 'm', 'e', 'r', 'o', 'n'};
    bool error;
    // ensure test.dat does not exist
    if (std::filesystem::exists("test.dat"))
    {
        std::filesystem::remove("test.dat");
    }
    // generate new test.dat with answer key
    std::fstream file;
    file.open("test.dat", std::fstream::out | std::fstream::binary);
    if (!file.good())
    {
        return false;
    }
    for (char c : ans)
    {
        file.write(&c, 1);
    }
    file.close();
    // attempt to load the file
    std::vector<char> buffer = isochop::utils::load_vector("test.dat", &error);
    // check file for errors
    bool passed = !error;
    passed &= buffer.size() == ans.size();
    if (passed)
    {
        for (size_t i = 0; i < ans.size(); i++)
        {
            if (ans.at(i) != buffer.at(i))
            {
                return false;
            }
        }
    }
    // return test status
    return passed;
}

TEST_VECTOR_T testUtils::getTests()
{
    TEST_VECTOR_T tests;
    pushTest(&tests, test_util_copyBytes, "Utils.cpp: copyBytes");
    pushTest(&tests, test_util_copyVector_1, "Utils.cpp: copyVector (normal)");
    pushTest(&tests, test_util_copyVector_2, "Utils.cpp: copyVector (null origin)");
    pushTest(&tests, test_util_copyVector_3, "Utils.cpp: copyVector (null destination)");
    pushTest(&tests, test_util_copyVector_4, "Utils.cpp: copyVector (origin offset + size)");
    pushTest(&tests, test_util_copyVector_5, "Utils.cpp: copyVector (destination offset + size)");
    pushTest(&tests, test_util_copyVector_6, "Utils.cpp: copyVector (origin size)");
    pushTest(&tests, test_util_copyVector_7, "Utils.cpp: copyVector (destination size)");
    pushTest(&tests, test_util_extractStringFromVector_1, "Utils.cpp: extractStringFromVector (normal)");
    pushTest(&tests, test_util_extractStringFromVector_2, "Utils.cpp: extractStringFromVector (cropped)");
    pushTest(&tests, test_util_extractStringFromVector_3, "Utils.cpp: extractStringFromVector (empty)");
    pushTest(&tests, test_util_extractStringFromVector_4, "Utils.cpp: extractStringFromVector (massive offset)");
    pushTest(&tests, test_util_extractStringFromVector_5, "Utils.cpp: extractStringFromVector (massive size)");
    pushTest(&tests, test_util_extractVectorFromString_1, "Utils.cpp: extractVectorFromString (normal)");
    pushTest(&tests, test_util_extractVectorFromString_2, "Utils.cpp: extractVectorFromString (cropped)");
    pushTest(&tests, test_util_extractVectorFromString_3, "Utils.cpp: extractVectorFromString (empty)");
    pushTest(&tests, test_util_extractVectorFromString_4, "Utils.cpp: extractVectorFromString (massive offset)");
    pushTest(&tests, test_util_extractVectorFromString_5, "Utils.cpp: extractVectorFromString (massive size)");
    pushTest(&tests, test_util_isstra_1, "Utils.cpp: isStrA (empty string)");
    pushTest(&tests, test_util_isstra_2, "Utils.cpp: isStrA (all legals)");
    pushTest(&tests, test_util_isstra_3, "Utils.cpp: isStrA (all legals and one illegal)");
    pushTest(&tests, test_util_isstra_4, "Utils.cpp: isStrA (all lowercase)");
    pushTest(&tests, test_util_isstra_5, "Utils.cpp: isStrA (\\0 disabled)");
    pushTest(&tests, test_util_isstra_6, "Utils.cpp: isStrA (\\0 enabled)");
    pushTest(&tests, test_util_isstrd_1, "Utils.cpp: isStrD (empty string)");
    pushTest(&tests, test_util_isstrd_2, "Utils.cpp: isStrD (all legals)");
    pushTest(&tests, test_util_isstrd_3, "Utils.cpp: isStrD (all legals and one illegal)");
    pushTest(&tests, test_util_isstrd_4, "Utils.cpp: isStrD (all lowercase)");
    pushTest(&tests, test_util_isstrd_5, "Utils.cpp: isStrD (\\0 disabled)");
    pushTest(&tests, test_util_isstrd_6, "Utils.cpp: isStrD (\\0 enabled)");
    pushTest(&tests, test_util_decodeint16lsb_1, "Utils.cpp: decodeInt16LSB (too short)");
    pushTest(&tests, test_util_decodeint16lsb_2, "Utils.cpp: decodeInt16LSB (too long)");
    pushTest(&tests, test_util_decodeint16lsb_3, "Utils.cpp: decodeInt16LSB (value)");
    pushTest(&tests, test_util_decodeint16msb_1, "Utils.cpp: decodeInt16MSB (too short)");
    pushTest(&tests, test_util_decodeint16msb_2, "Utils.cpp: decodeInt16MSB (too long)");
    pushTest(&tests, test_util_decodeint16msb_3, "Utils.cpp: decodeInt16MSB (value)");
    pushTest(&tests, test_util_decodeint16lsbmsb_1, "Utils.cpp: decodeInt16LSB_MSB (too short)");
    pushTest(&tests, test_util_decodeint16lsbmsb_2, "Utils.cpp: decodeInt16LSB_MSB (too long)");
    pushTest(&tests, test_util_decodeint16lsbmsb_3, "Utils.cpp: decodeInt16LSB_MSB (bad encoding)");
    pushTest(&tests, test_util_decodeint16lsbmsb_4, "Utils.cpp: decodeInt16LSB_MSB (value)");
    pushTest(&tests, test_util_decodesint16lsb_1, "Utils.cpp: decodeSint16LSB (too short)");
    pushTest(&tests, test_util_decodesint16lsb_2, "Utils.cpp: decodeSint16LSB (too long)");
    pushTest(&tests, test_util_decodesint16lsb_3, "Utils.cpp: decodeSint16LSB (value)");
    pushTest(&tests, test_util_decodesint16msb_1, "Utils.cpp: decodeSint16MSB (too short)");
    pushTest(&tests, test_util_decodesint16msb_2, "Utils.cpp: decodeSint16MSB (too long)");
    pushTest(&tests, test_util_decodesint16msb_3, "Utils.cpp: decodeSint16MSB (value)");
    pushTest(&tests, test_util_decodesint16lsbmsb_1, "Utils.cpp: decodeSint16LSB_MSB (too short)");
    pushTest(&tests, test_util_decodesint16lsbmsb_2, "Utils.cpp: decodeSint16LSB_MSB (too long)");
    pushTest(&tests, test_util_decodesint16lsbmsb_3, "Utils.cpp: decodeSint16LSB_MSB (bad encoding)");
    pushTest(&tests, test_util_decodesint16lsbmsb_4, "Utils.cpp: decodeSint16LSB_MSB (value)");
    pushTest(&tests, test_util_decodeint32lsb_1, "Utils.cpp: decodeInt32LSB (too short)");
    pushTest(&tests, test_util_decodeint32lsb_2, "Utils.cpp: decodeInt32LSB (too long)");
    pushTest(&tests, test_util_decodeint32lsb_3, "Utils.cpp: decodeInt32LSB (value)");
    pushTest(&tests, test_util_decodeint32msb_1, "Utils.cpp: decodeInt32MSB (too short)");
    pushTest(&tests, test_util_decodeint32msb_2, "Utils.cpp: decodeInt32MSB (too long)");
    pushTest(&tests, test_util_decodeint32msb_3, "Utils.cpp: decodeInt32MSB (value)");
    pushTest(&tests, test_util_decodeint32lsbmsb_1, "Utils.cpp: decodeInt32LSB_MSB (too short)");
    pushTest(&tests, test_util_decodeint32lsbmsb_2, "Utils.cpp: decodeInt32LSB_MSB (too long)");
    pushTest(&tests, test_util_decodeint32lsbmsb_3, "Utils.cpp: decodeInt32LSB_MSB (bad encoding)");
    pushTest(&tests, test_util_decodeint32lsbmsb_4, "Utils.cpp: decodeInt32LSB_MSB (value)");
    pushTest(&tests, test_util_decodesint32lsb_1, "Utils.cpp: decodeSint32LSB (too short)");
    pushTest(&tests, test_util_decodesint32lsb_2, "Utils.cpp: decodeSint32LSB (too long)");
    pushTest(&tests, test_util_decodesint32lsb_3, "Utils.cpp: decodeSint32LSB (value)");
    pushTest(&tests, test_util_decodesint32msb_1, "Utils.cpp: decodeSint32MSB (too short)");
    pushTest(&tests, test_util_decodesint32msb_2, "Utils.cpp: decodeSint32MSB (too long)");
    pushTest(&tests, test_util_decodesint32msb_3, "Utils.cpp: decodeSint32MSB (value)");
    pushTest(&tests, test_util_decodesint32lsbmsb_1, "Utils.cpp: decodeSint32LSB_MSB (too short)");
    pushTest(&tests, test_util_decodesint32lsbmsb_2, "Utils.cpp: decodeSint32LSB_MSB (too long)");
    pushTest(&tests, test_util_decodesint32lsbmsb_3, "Utils.cpp: decodeSint32LSB_MSB (bad encoding)");
    pushTest(&tests, test_util_decodesint32lsbmsb_4, "Utils.cpp: decodeSint32LSB_MSB (value)");
    pushTest(&tests, test_util_encodeint16lsb_1, "Utils.cpp: encodeInt16LSB (size)");
    pushTest(&tests, test_util_encodeint16lsb_2, "Utils.cpp: encodeInt16LSB (value)");
    pushTest(&tests, test_util_encodeint16msb_1, "Utils.cpp: encodeInt16MSB (size)");
    pushTest(&tests, test_util_encodeint16msb_2, "Utils.cpp: encodeInt16MSB (value)");
    pushTest(&tests, test_util_encodeint16lsbmsb_1, "Utils.cpp: encodeInt16LSB_MSB (size)");
    pushTest(&tests, test_util_encodeint16lsbmsb_2, "Utils.cpp: encodeInt16LSB_MSB (value)");
    pushTest(&tests, test_util_encodesint16lsb_1, "Utils.cpp: encodeSint16LSB (size)");
    pushTest(&tests, test_util_encodesint16lsb_2, "Utils.cpp: encodeSint16LSB (value)");
    pushTest(&tests, test_util_encodesint16msb_1, "Utils.cpp: encodeSint16MSB (size)");
    pushTest(&tests, test_util_encodesint16msb_2, "Utils.cpp: encodeSint16MSB (value)");
    pushTest(&tests, test_util_encodesint16lsbmsb_1, "Utils.cpp: encodeSint16LSB_MSB (size)");
    pushTest(&tests, test_util_encodesint16lsbmsb_2, "Utils.cpp: encodeSint16LSB_MSB (value)");
    pushTest(&tests, test_util_encodeint32lsb_1, "Utils.cpp: encodeInt32LSB (size)");
    pushTest(&tests, test_util_encodeint32lsb_2, "Utils.cpp: encodeInt32LSB (value)");
    pushTest(&tests, test_util_encodeint32msb_1, "Utils.cpp: encodeInt32MSB (size)");
    pushTest(&tests, test_util_encodeint32msb_2, "Utils.cpp: encodeInt32MSB (value)");
    pushTest(&tests, test_util_encodeint32lsbmsb_1, "Utils.cpp: encodeInt32LSB_MSB (size)");
    pushTest(&tests, test_util_encodeint32lsbmsb_2, "Utils.cpp: encodeInt32LSB_MSB (value)");
    pushTest(&tests, test_util_encodesint32lsb_1, "Utils.cpp: encodeSint32LSB (size)");
    pushTest(&tests, test_util_encodesint32lsb_2, "Utils.cpp: encodeSint32LSB (value)");
    pushTest(&tests, test_util_encodesint32msb_1, "Utils.cpp: encodeSint32MSB (size)");
    pushTest(&tests, test_util_encodesint32msb_2, "Utils.cpp: encodeSint32MSB (value)");
    pushTest(&tests, test_util_encodesint32lsbmsb_1, "Utils.cpp: encodeSint32LSB_MSB (size)");
    pushTest(&tests, test_util_encodesint32lsbmsb_2, "Utils.cpp: encodeSint32LSB_MSB (value)");
    pushTest(&tests, test_util_getbit_1, "Utils.cpp: getBit (illegal index)");
    pushTest(&tests, test_util_getbit_2, "Utils.cpp: getBit (all legal indexes)");
    pushTest(&tests, test_util_setbit_1, "Utils.cpp: setBit (illegal index)");
    pushTest(&tests, test_util_setbit_2, "Utils.cpp: setBit (all legal indexes)");
    pushTest(&tests, test_util_convertsectortobyteaddress_1, "Utils.cpp: convertSectorToByteAddress (0 -> 0)");
    pushTest(&tests, test_util_convertsectortobyteaddress_2, "Utils.cpp: convertSectorToByteAddress (1 -> 2048)");
    pushTest(&tests, test_util_convertsectortobyteaddress_3, "Utils.cpp: convertSectorToByteAddress (2 -> 4096)");
    pushTest(&tests, test_util_convertsectortobyteaddress_4, "Utils.cpp: convertSectorToByteAddress (3 -> 6144)");
    pushTest(&tests, test_util_convertsectortobyteaddress_5, "Utils.cpp: convertSectorToByteAddress (4 -> 8192)");
    pushTest(&tests, test_util_convertbyteaddresstosector_1, "Utils.cpp: convertByteAddressToSector (0 -> 0)");
    pushTest(&tests, test_util_convertbyteaddresstosector_2, "Utils.cpp: convertByteAddressToSector (2047 -> 0)");
    pushTest(&tests, test_util_convertbyteaddresstosector_3, "Utils.cpp: convertByteAddressToSector (2048 -> 1)");
    pushTest(&tests, test_util_convertbyteaddresstosector_4, "Utils.cpp: convertByteAddressToSector (4095 -> 1)");
    pushTest(&tests, test_util_convertbyteaddresstosector_5, "Utils.cpp: convertByteAddressToSector (4096 -> 2)");
    pushTest(&tests, test_util_sha256sum_1, "Utils.cpp: test_util_sha256sum_1 (nullptr)");
    pushTest(&tests, test_util_sha256sum_2, "Utils.cpp: test_util_sha256sum_2 (empty)");
    pushTest(&tests, test_util_sha256sum_3, "Utils.cpp: test_util_sha256sum_3 (A)");
    pushTest(&tests, test_util_sha256sum_4, "Utils.cpp: test_util_sha256sum_4 (Cameron)");
    pushTest(&tests, test_util_dump_vector_1, "Utils.cpp: test_util_dump_vector_1 (nullptr)");
    pushTest(&tests, test_util_dump_vector_2, "Utils.cpp: test_util_dump_vector_2 (empty)");
    pushTest(&tests, test_util_dump_vector_3, "Utils.cpp: test_util_dump_vector_3 (Cameron)");
    pushTest(&tests, test_util_load_vector_1, "Utils.cpp: test_util_load_vector_1 (bad filename)");
    pushTest(&tests, test_util_load_vector_2, "Utils.cpp: test_util_load_vector_2 (empty)");
    pushTest(&tests, test_util_load_vector_3, "Utils.cpp: test_util_load_vector_3 (Cameron)");
    return tests;
}
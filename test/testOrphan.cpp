/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdexcept>
#include "testCommon.hpp"
#include "testOrphan.hpp"
#include "../include/Orphan.hpp"

bool testOrphan::test_orphan_setdata_getdata()
{
    std::vector<char> ans;
    for (int i = 0; i < 100; i++)
    {
        ans.push_back(i % 256);
    }
    isochop::Orphan ext;
    ext.setData(ans);
    std::vector<char> out = ext.getData();
    if (ans.size() != out.size())
    {
        return false;
    }
    for (size_t i = 0; i < out.size(); i++)
    {
        if (out.at(i) != ans.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testOrphan::test_orphan_setaddress_getaddress()
{
    unsigned long address = 42;
    isochop::Orphan o;
    o.setAddress(address);
    return o.getAddress() == address;
}

bool testOrphan::test_orphan_constructor_1()
{
    isochop::Orphan *ext = new isochop::Orphan();
    return ext != nullptr;
}

bool testOrphan::test_orphan_constructor_2()
{
    std::vector<char> ans;
    unsigned long addr = 42;
    for (int i = 0; i < 100; i++)
    {
        ans.push_back(i % 256);
    }
    isochop::Orphan *ext = new isochop::Orphan(ans, addr);
    std::vector<char> out = ext->getData();
    if (ans.size() != out.size())
    {
        return false;
    }
    for (size_t i = 0; i < out.size(); i++)
    {
        if (out.at(i) != ans.at(i))
        {
            return false;
        }
    }
    return ext->getAddress() == addr;
}

bool testOrphan::test_orphan_clone()
{
    std::vector<char> ans;
    unsigned long addr = 42;
    for (int i = 0; i < 100; i++)
    {
        ans.push_back(i % 256);
    }
    isochop::Orphan *ext = new isochop::Orphan(ans, addr);
    isochop::Orphan dup = ext->clone();
    std::vector<char> out = dup.getData();
    if (ans.size() != out.size())
    {
        return false;
    }
    for (size_t i = 0; i < out.size(); i++)
    {
        if (out.at(i) != ans.at(i))
        {
            return false;
        }
    }
    return ext->getAddress() == addr;
}

bool testOrphan::test_orphan_markaddresses_1()
{
    isochop::Orphan orphan = isochop::Orphan(std::vector<char>(), 20);
    try
    {
        orphan.markAddresses(nullptr);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testOrphan::test_orphan_markaddresses_2()
{
    std::vector<char> data = {1, 2, 3, 4, 5};
    std::vector<bool> bits(20, 0);
    isochop::Orphan orphan = isochop::Orphan(data, 5);
    orphan.markAddresses(&bits);
    for (int i = 0; i < 5; i++)
    {
        if (bits.at(i))
        {
            return false;
        }
    }
    for (int i = 5; i < 10; i++)
    {
        if (!bits.at(i))
        {
            return false;
        }
    }
    for (int i = 10; i < 20; i++)
    {
        if (bits.at(i))
        {
            return false;
        }
    }
    return true;
}

TEST_VECTOR_T testOrphan::getTests()
{
    TEST_VECTOR_T tests;
    pushTest(&tests, test_orphan_setdata_getdata, "Orphan.cpp: getData and setData (symmetry)");
    pushTest(&tests, test_orphan_setaddress_getaddress, "orphan.cpp: getAddress and setAddress (symmetry)");
    pushTest(&tests, test_orphan_constructor_1, "Orphan.cpp: Extent (no-args)");
    pushTest(&tests, test_orphan_constructor_2, "Orphan.cpp: Extent (args)");
    pushTest(&tests, test_orphan_clone, "Orphan.cpp: clone");
    pushTest(&tests, test_orphan_markaddresses_1, "Orphan.cpp: markAddresses (nullptr)");
    pushTest(&tests, test_orphan_markaddresses_2, "Orphan.cpp: markAddresses (data)");
    return tests;
}
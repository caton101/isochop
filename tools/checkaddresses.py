# ISOLib
# Copyright (C) 2022 Cameron Himes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from sys import argv
import json

ADDRESSES = []

def readOrphans(dirpath, root_manifest):
    global ADDRESSES
    for x in root_manifest['orphan_hashes']:
        with open(dirpath + "/" + x, "r") as f:
            vd = json.load(f)
            address = vd['byte_address']
            class_ = vd['manifest_class']
            ADDRESSES.append((address, class_))

def readExtentNodes(dirpath, extentnode_hash):
    global ADDRESSES
    with open(dirpath + "/" + extentnode_hash, "r") as f:
        extentnode_manifest = json.load(f);
    addr = extentnode_manifest['byte_address']
    class_ = extentnode_manifest['manifest_class']
    ADDRESSES.append((addr, class_))
    for x in extentnode_manifest['children_hashes']:
        readExtentNodes(dirpath=dirpath, extentnode_hash=x)

def readExtentTree(dirpath, root_manifest):
    eth = root_manifest['extenttree_hash']
    with open(dirpath + "/" + eth, "r") as f:
        etj = json.load(f)
        enh = etj['extentnode_hash']
        readExtentNodes(dirpath=dirpath, extentnode_hash=enh)

def readVDs(dirpath, root_manifest):
    global ADDRESSES
    for x in root_manifest['volumedescriptor_hashes']:
        with open(dirpath + "/" + x, "r") as f:
            vd = json.load(f)
            address = vd['byte_address']
            class_ = vd['manifest_class']
            ADDRESSES.append((address, class_))

def findDups():
    dups = []
    for i, (ea, ec) in enumerate(ADDRESSES):
        for j, (oa, oc) in enumerate(ADDRESSES):
            if ea == oa and i != j:
                dups.append((ea, ec, oa, oc))
    return dups

def main():
    if len(argv) != 2:
        print("Usage: checkaddresses.py dirpath")
        exit(1)
    DIRPATH = argv[1];
    with open(DIRPATH + "/STRUCTURE_ROOT", "r") as f:
        structure_root = f.read();
    with open(DIRPATH + "/" + structure_root, "r") as f:
        root_manifest = json.load(f)
    readVDs(dirpath=DIRPATH, root_manifest=root_manifest)
    readExtentTree(dirpath=DIRPATH, root_manifest=root_manifest)
    readOrphans(dirpath=DIRPATH, root_manifest=root_manifest)
    dups = findDups();
    print("=====| Known Objects |=====")
    if (len(ADDRESSES) > 0):
        for address, class_ in ADDRESSES:
            print("    {c}@{a}".format(c=class_, a=hex(int(address))))
    else:
        print("There are no objects.")
    print("=====| Duplicate Objects |=====")
    if (len(dups) > 0):
        for ea, ec, oa, oc in dups:
            print("    {ea} ({ec}) == {oa} ({oc})".format(
                ea=hex(int(ea)),
                ec=ec,
                oa=hex(int(oa)),
                oc=oc
            ))
    else:
        print("No duplicates found.")

if __name__ == "__main__":
    main();

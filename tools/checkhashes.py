# ISOLib
# Copyright (C) 2022 Cameron Himes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from sys import argv
import os
import hashlib

def main():
    if len(argv) != 2:
        print("Usage: checkaddresses.py dirpath")
        exit(1)
    failed = []
    files = os.listdir(argv[1])
    for file in files:
        if file == "STRUCTURE_ROOT":
            print("Skipped STRUCTURE_ROOT.")
        else:
            with open(argv[1] + "/" + file, "rb") as f:
                data = f.read()
            if file != hashlib.sha256(data).hexdigest():
                print(file, "does not match its hash")
                failed.append(file)
            else:
                print(file, "matches its hash")
    if len(failed) > 0:
        if len(failed) == 1:
            print(len(failed), "does not match its hash:")
        else:
            print(len(failed), "files do not match their hash:")
        for f in failed:
            print("\t", f)
        exit(1)
    else:
        print("All files match their hash")

if __name__ == "__main__":
    main();

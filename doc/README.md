# Important

These files are included to give more information about the ISO 9660 filesystem.
I did not make these files and do not take credit for making them. The owner for
each is listed below:

* ":doc/ISO 9660 - OSDev Wiki.pdf" [source](https://wiki.osdev.org/ISO_9660)
* ":doc/ISO 9660 - Wikipedia.pdf" [source](https://en.wikipedia.org/wiki/ISO_9660)
* ":doc/ECMA-119_1st_edition_december_1986.pdf" [source](https://www.ecma-international.org/publications-and-standards/standards/ecma-119/)
* ":doc/ECMA-119_2nd_edition_december_1987.pdf" [source](https://www.ecma-international.org/publications-and-standards/standards/ecma-119/)
* ":doc/ECMA-119_3rd_edition_december_2017.pdf" [source](https://www.ecma-international.org/publications-and-standards/standards/ecma-119/)
